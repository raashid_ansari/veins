#! /usr/bin/env python3
import time
import json
from datetime import datetime
# from psutil import cpu_percent

import redis
import numpy as np
from flask_socketio import emit, SocketIO

REDIS_URL = "redis://localhost:6379/"
socket = SocketIO(message_queue=REDIS_URL)


class Data:
    tpl = []
    fpl = []
    tnl = []
    fnl = []
    fprs = []
    tprs = []
    recalls = []
    precisions = []
    f1_scores = []
    accuracies = []


def bootstrap_on_connect():
    emit('bootstrap', {'x': [0], 'y': [0]})
    socket.emit('setup-mps-plot', {})
    socket.emit('setup-attack-plot', {})
    socket.emit('setup-latency-plot', {})
    socket.emit('setup-exec-time-plot', {})


def push_data_task(df):
    # sort the data according to time
    df.sort_values(by=['simTime'], inplace=True)
    push_mps_plots(df)
    push_attack_plots(df)
    push_latency_task(df)
    push_exec_time_task(df)

    hvList = df.hvId.unique().tolist()
    hvList.sort()
    socket.emit('update-checks-dd', {'hvList': hvList})


def push_mps_plots(df):
    mps_df = df[[
        'simTime',
        'ratings.ewma.ART','ratings.ewma.MBF','ratings.ewma.SAW',
        'ratings.ewma.MVP','ratings.ewma.MVN','ratings.ewma.MAP',
        'ratings.ewma.MAN','ratings.ewma.MDM','ratings.ewma.KALMAN',
        'ratings.ewma.FBR','ratings.ewma.suspicion','suspectedAttack',
        'ewma.mlPrediction'
        ]].copy()

    # process data into boolean for easier operations
    mps_df['ratings.ewma.suspicion'] = np.where(
            df['ratings.ewma.suspicion'] > df['groundTruth.threshold'],
            1.0, 0.0
            )

    mps_df = _make_df_good(mps_df)

    d = _calculate(mps_df.iloc[:, 1:], df['groundTruth.attackGt'])
    _emit_socks(d, mps_df, 14, 'mps')


def push_attack_plots(df):
    attack_df = df[[
        'simTime','eebl.mpsOff','eebl.mpsOn'
        ]].copy()

    d = _calculate(attack_df.iloc[:, 1:], df['groundTruth.attackGt'])
    _emit_socks(d, attack_df, 3, 'attack')


def push_vehicle_density_task(df):
  # prepare mps-vehicle-density-update output
  df = _make_df_good(df)
  df.sort_values(by=['simTime'], inplace=True)
  df.fillna(0.0, inplace=True)
  _sockemit(df['simTime'].tolist(), df['vehicleDensity'].tolist(),
      'update-mps-vehicle-density-plot')


def push_latency_task(df):
    latency_df = _make_df_good(
            df[[
                'simTime',
                'latency.simTime','latency.realTime','latency.bsm',
                'latency.distance'
                ]].copy()
            )
    latency_df.fillna(0.0, inplace=True)

    _sockemit(latency_df['simTime'].tolist(), latency_df['latency.realTime'].tolist(),
            'update-sim-real-plot')
    _sockemit(latency_df['simTime'].tolist(), latency_df['latency.bsm'].tolist(),
            'update-sim-bsm-plot')
    _sockemit(latency_df['simTime'].tolist(), latency_df['latency.distance'].tolist(),
            'update-sim-dist-plot')
    _sockemit(latency_df['simTime'].tolist(), latency_df['latency.simTime'].tolist(),
            'update-sim-sim-plot')
    _sockemit(latency_df['latency.realTime'].tolist(), latency_df['latency.bsm'].tolist(),
            'update-real-bsm-plot')
    _sockemit(latency_df['latency.realTime'].tolist(), latency_df['latency.distance'].tolist(),
            'update-real-dist-plot')
    _sockemit(latency_df['latency.bsm'].tolist(), latency_df['latency.distance'].tolist(),
            'update-bsm-dist-plot')


def push_exec_time_task(df):
    exec_time_df = _make_df_good(
            df[[
                'simTime','latency.bsm',
                'execTime.ART','execTime.SAW','execTime.MBF','execTime.MVP',
                'execTime.MVN','execTime.MAP','execTime.MAN','execTime.FBR',
                'execTime.KALMAN','execTime.IML','execTime.EML'
                ]].copy()
            )

    total = exec_time_df.loc[exec_time_df['latency.bsm'].notnull()]

    total['execTime.TOTAL'] = (
            total['execTime.ART'] + total['execTime.SAW'] +\
            total['execTime.MBF'] + total['execTime.MVP'] +\
            total['execTime.MVN'] + total['execTime.MAP'] +\
            total['execTime.MAN'] + total['execTime.KALMAN'] +\
            total['execTime.FBR'])\
            *\
            (((total['latency.bsm'] - 1) * 10 ** 9) / 10)
    _sockemit(total['simTime'].tolist(), total['execTime.TOTAL'].tolist(),
            'update-total-exec-time-plot')

    exec_time_df.fillna(0.0, inplace=True)
    
    for col in exec_time_df.columns[2:]:
        _sockemit(
                exec_time_df['simTime'].tolist(),
                exec_time_df[col].tolist(),
                'update-' + col.split('.')[-1].lower() + '-exec-time-plot'
                )

    cols = [col.split('.')[-1] for col in exec_time_df.columns[2:-3]]
    check = [exec_time_df[col].tolist() for col in exec_time_df.columns[2:-3]]
    _sockemit(cols, check, 'update-check-box-exec-time-plot')

    _sockemit(['KALMAN'], exec_time_df['execTime.KALMAN'].tolist(),
            'update-kalman-box-exec-time-plot')

    _sockemit(['TOTAL'], total['execTime.TOTAL'].tolist(),
            'update-total-box-exec-time-plot')

    _sockemit(['EML'], exec_time_df['execTime.EML'].tolist(),
            'update-ml-box-exec-time-plot')


def _make_df_good(df):
    for col in df.columns:
        df.loc[:, col] = df[col].astype(float)
    return df

def _sockemit(x, y, queue_name):
    response_object = {
            'x': x,
            'y': y
            }
    socket.emit(queue_name, json.dumps(response_object))


def _calculate(df, attackGt):
    d = Data()

    for column in df.columns:
        # correcting data types
        df.loc[:,column] = df[column].astype(float)

        tp_col = "tp." + column
        fp_col = "fp." + column
        tn_col = "tn." + column
        fn_col = "fn." + column
        tpr_col = "tpr." + column
        fpr_col = "fpr." + column
        precision_col = "precision." + column
        recall_col = "recall." + column
        f1_score_col = "f1_score." + column
        accuracy_col = "accuracy." + column

        # base confusion
        df[tp_col] = ((df[column] > 0.0) & (attackGt == 1)).cumsum()
        df[fp_col] = ((df[column] > 0.0) & ~(attackGt == 1)).cumsum()
        df[fn_col] = (~(df[column] > 0.0) & (attackGt == 1)).cumsum()
        df[tn_col] = (~(df[column] > 0.0) & ~(attackGt == 1)).cumsum()

        # get last values of confusion matrix arrays
        d.tpl.append(df[tp_col].tail(1).item())
        d.fpl.append(df[fp_col].tail(1).item())
        d.fnl.append(df[fn_col].tail(1).item())
        d.tnl.append(df[tn_col].tail(1).item())

        # False positive rate
        df[fpr_col] = _compare_arrays(df[fp_col], df[fp_col] + df[tn_col])
        d.fprs.append(df[fpr_col].tolist())

        # True positive rate
        df[tpr_col] = _compare_arrays(df[tp_col], df[tp_col] + df[fn_col])
        d.tprs.append(df[tpr_col].tolist())

        # Precision
        df[precision_col] = _compare_arrays(
            df[tp_col],
            df[tp_col] + df[fp_col]
            )
        d.precisions.append(df[precision_col].tolist())

        # Recall
        df[recall_col] = _compare_arrays(
            df[tp_col],
            df[tp_col] + df[fn_col]
            )
        d.recalls.append(df[recall_col].tolist())

        # F1-score
        df[f1_score_col] = _compare_arrays(
            2 * df[precision_col] * df[recall_col],
            df[precision_col] + df[recall_col]
            )
        d.f1_scores.append(df[f1_score_col].tolist())

        # Accuracy
        df[accuracy_col] = _compare_arrays(
            df[tp_col] + df[tn_col],
            df[tp_col] + df[tn_col] + df[fp_col] + df[fn_col]
            )
        d.accuracies.append(df[accuracy_col].tolist())
    # print(pd.DataFrame({"tp":df.tppred,
        # "fn":df.fnpred, "total":df.tppred +\
        # df.fnpred}))
    # print(df.eeblBefore.sum(), df.eeblAfter.sum())
    df.fillna(0.0)
    return d


def _compare_arrays(numerator, denominator):
    return np.where(denominator == 0, numerator, numerator / denominator)


def _emit_socks(d, df, end_idx, plot_name):
    # prepare base-confusion-update output
    cols = [col.split('.')[-1] for col in df.columns[1:end_idx]]
    _sockemit(cols, [d.tpl,d.fpl,d.tnl,d.fnl], 'update-' + plot_name + '-base-confusion-plot')

    # prepare update-roc-plot output
    _sockemit(d.fprs, d.tprs, 'update-' + plot_name + '-roc-plot')

    # prepare update-pr-plot output
    _sockemit(d.recalls, d.precisions, 'update-' + plot_name + '-pr-plot')

    # prepare update-precision-plot output
    _sockemit(df['simTime'].tolist(), d.precisions, 'update-' + plot_name + '-precision-plot')

    # prepare update-precision-plot output
    _sockemit(df['simTime'].tolist(), d.recalls, 'update-' + plot_name + '-recall-plot')

    # prepare f1score-plot update output
    _sockemit(df['simTime'].tolist(), d.f1_scores, 'update-' + plot_name + '-f1score-plot')

