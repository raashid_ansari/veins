server {
  listen 8880;
    server_name  localhost;
    root         /var/www/html;
    client_max_body_size 16M;
    access_log  /var/log/nginx/mps-data-server.log;

    location / {
        include proxy_params;
        proxy_pass http://127.0.0.1:8000;
    }

    location /socket.io {
        include proxy_params;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:8000/socket.io;
    }

    location /data {
      include proxy_params;
      proxy_pass http://127.0.0.1:8000/data;
    }

    location /vehicleDensity {
      include proxy_params;
      proxy_pass http://127.0.0.1:8000/vehicleDensity;
    }

    location /tasks {
      include proxy_params;
      proxy_pass http://127.0.0.1:8000/tasks;
    }

    location /ml {
      include proxy_params;
      proxy_pass http://127.0.0.1:8000/ml;
    }

    location /hv {
      include proxy_params;
      proxy_pass http://127.0.0.1:8000/hv;
    }

    location /hvPost {
      include proxy_params;
      proxy_pass http://127.0.0.1:8000/hvPost;
    }

    location /simulation {
      include proxy_params;
      proxy_pass http://127.0.0.1:8000/simulation;
    }
}
