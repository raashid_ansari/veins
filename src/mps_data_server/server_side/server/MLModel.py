import os

import numpy as np
# from sklearn import datasets
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestClassifier

import sys
sys.path.insert(0, os.environ['HOME'] + '/src/veins/src/scripts')

from calculationCore import Data

class MLModel:
    def __init__(self):
        # load dataset
        # digits = datasets.load_digits()
        # X, y = digits.data, digits.target

        # setup classifier
        self.base_dir = os.environ['HOME'] + '/src/veins/'
        self.clf_dir = self.base_dir + 'src/veins/src/mps_data_server/classifiers/'
        self.simulation_dir = self.base_dir + 'examples/mps/results/FakeEEBL'

    def train_and_save(self):
        # load training data
        data = Data()
        data.extract_trace_data(self.simulation_dir + "_training/")
        data = data.trace_data
        X = data.loc[:, 'art':'suspicion']
        y = data.attackGt
        # print(X,y)

        # train classifier
        clf = RandomForestClassifier(n_estimators = 25)
        clf.fit(X, y)

        # save classifier into a file
        joblib.dump(clf, self.clf_dir + 'clf.pkl')

    def get_prediction(self, data):
        # prepare data
        ratings = data['ratings']
        test_data = np.array([[ratings['ART'], ratings['MBF'], ratings['SAW'],
                ratings['MA_VEL_POS'], ratings['MA_VEL_NEG'],
                ratings['MA_ACCEL_POS'], ratings['MA_ACCEL_NEG'],
                ratings['MDM'], ratings['KALMAN'], ratings['FBR'],
                ratings['suspicionRating']]])

        # load the classifier from file
        clf = joblib.load(self.simulation_dir + "/" + str(data['reporterId']) + '.pkl')

        # predict using saved classifier
        prediction = clf.predict(test_data)
        # print(prediction)
        return prediction[0]

if __name__ == '__main__':
    mlm = MLModel()
    mlm.train_and_save()

