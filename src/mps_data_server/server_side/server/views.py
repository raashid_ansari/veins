import re
import os
import glob
import json
import shutil
import subprocess
from contextlib import contextmanager
from datetime import datetime as dt

import numpy as np
import pandas as pd
import redis
from redis import Redis
from rq import Queue, Connection
from flask import render_template, Blueprint, jsonify, \
        request, current_app

from server.tasks import store_task
from websocket.streaming import push_data_task, push_vehicle_density_task
from server.MLModel import MLModel

main_blueprint = Blueprint('main', __name__,)


# File Paths
base_dir = os.environ['HOME'] + "/src/veins/"
server_side = base_dir + "src/mps_data_server/server_side/"
mps_dir = base_dir + "examples/mps/"


# DataFrame
count = 0
N = 5000
df = pd.DataFrame(index=np.arange(N), columns=[
    'rv_id','hvId','simTime','kalman.x','kalman.y','pos.x','pos.y','gps.x',
    'gps.y','posSD.x','posSD.y','vel.x','vel.y','distance','distanceSD',
    'ratings.ewma.ART','ratings.ewma.MBF','ratings.ewma.SAW','ratings.ewma.MVP',
    'ratings.ewma.MVN','ratings.ewma.MAP','ratings.ewma.MAN','ratings.ewma.MDM',
    'ratings.ewma.KALMAN','ratings.ewma.FBR','ratings.ewma.suspicion',
    'ratings.instant.ART','ratings.instant.MBF','ratings.instant.SAW',
    'ratings.instant.MVP','ratings.instant.MVN','ratings.instant.MAP',
    'ratings.instant.MAN','ratings.instant.MDM','ratings.instant.KALMAN',
    'ratings.instant.FBR','ratings.instant.suspicion','suspectedAttack',
    'eebl.mpsOff','eebl.mpsOn','rssi','ewma.mlPrediction',
    'instant.mlPrediction','wsmData',
    'latency.simTime','latency.realTime','latency.bsm','latency.distance',
    'execTime.ART','execTime.SAW','execTime.MBF','execTime.MVP','execTime.MVN',
    'execTime.MAP','execTime.MAN','execTime.FBR','execTime.KALMAN',
    'execTime.IML','execTime.EML',
    'groundTruth.attackGt','groundTruth.attackType','groundTruth.threshold',
    'groundTruth.repNum'
    ])

v_count = 0
VN = 5000
vdf = pd.DataFrame(index=np.arange(N), columns=[
    'simTime', 'vehicleDensity', 'repNum'
    ])


@main_blueprint.route('/', methods=['GET'])
def index():
    header_data = {
            'title': 'MPS',
            'x_window': 100
            }
    return render_template('index.html', headerdata = header_data)


@main_blueprint.route('/data', methods=['POST'])
def run_store_task():
    global df
    global count
    global N

    if count == N:
        N = N * 2
        df = df.reindex(index=np.arange(N))

    data_str = request.get_json()["bsm"]

    response_object = {}
    data = data_str.split(',')
    df.iloc[count] = data
    # with Connection(redis.from_url(current_app.config['REDIS_URL'])):
        # q = Queue('store-data')
        # task = q.enqueue(store_task, data_str, count, df.columns.values)
    response_object = {
            'status': 'success'
            }

    slice_size = 200
    if count % slice_size == 0:
        # transmit data to queue
        with Connection(redis.from_url(current_app.config['REDIS_URL'])):
            q = Queue('push-data')
            task = q.enqueue(push_data_task,
                    # df.iloc[count - slice_size : count])
                    df.iloc[ : count])
                    # outs)
            response_object = {
                    'status': 'success',
                    'data': {
                        'task_id': task.get_id()
                        }
                    }
    print(count, end='\r')
    count = count + 1
    return jsonify(response_object), 202


@main_blueprint.route('/vehicleDensity', methods=['POST'])
def run_vehicle_density_task():
    global vdf
    global VN
    global v_count

    if v_count == VN:
        VN = VN * 2
        vdf = vdf.reindex(index=np.arange(VN))

    data_str = request.get_json()['vehicleDensity']

    response_object = {}
    data = data_str.split(',')
    vdf.iloc[v_count] = data
    # with Connection(redis.from_url(current_app.config['REDIS_URL'])):
        # q = Queue('store-data')
        # task = q.enqueue(store_task, data_str, count, df.columns.values)
    response_object = {
            'status': 'success'
            }

    slice_size = 200
    if v_count % slice_size == 0:
        with Connection(redis.from_url(current_app.config['REDIS_URL'])):
            q = Queue('push-vehicle-density-data')
            task = q.enqueue(push_vehicle_density_task,
                    # vdf.iloc[v_count - slice_size : v_count])
                    vdf.iloc[ : v_count])
            response_object = {
                    'status': 'success',
                    'data': {
                        'task_id': task.get_id()
                        }
                    }
    print('         ', v_count, end='\r')
    v_count = v_count + 1
    return jsonify(response_object), 202


@main_blueprint.route('/tasks/<task_id>', methods=['GET'])
def get_status(task_id):
    with Connection(redis.from_url(current_app.config['REDIS_URL'])):
        q = Queue()
        task = q.fetch_job(task_id)
    if task:
        response_object = {
                'status': 'success',
                'data': {
                    'task_id': task.get_id(),
                    'task_status': task.get_status(),
                    'task_result': task.result,
                    }
                }
    else:
        response_object = {'status': 'error'}
    return jsonify(response_object)


@main_blueprint.route('/ml', methods=['POST'])
def get_prediction():
    src = server_side + "classifiers/clf.pkl"
    simulation_dir = mps_dir + "results/FakeEEBL/"

    data = request.get_json()
    host_vehicle_id = str(data['reporterId'])

    # check if classifier available for this host vehicle
    if not os.path.isfile(simulation_dir + host_vehicle_id + ".pkl"):
        dst = simulation_dir + host_vehicle_id + ".pkl"
        shutil.copy(src, dst)

    ml_model = MLModel()
    prediction = ml_model.get_prediction(data)
    response_object = {
            'status': 'success',
            'data': {
                'prediction': str(prediction)
                }
            }
    return jsonify(response_object), 202


# send list of host vehicles to checks plot dropdown
@main_blueprint.route('/hv', methods=['GET'])
def send_hv_data():
    hv_list = df['hvId'].unique().astype(float)
    data = hv_list[~np.isnan(hv_list)].astype(int).tolist()
    data.sort()
    response_object = {
            'status': 200,
            'hvIds': data
            }
    return jsonify(response_object), 200


# Data class for remote vehicle data
class RvResponse:
    def __init__(self, id_):
        self.id = id_
        self.type = None
        self.instants = []
        self.ewmas = []

    def __repr__(self):
        return(json.dumps(
            {
                "id": self.id,
                "type": self.type,
                "instants": self.instants,
                "ewmas": self.ewmas
                }
            ))


# send data to plot host vehicle perception of remote vehicles
@main_blueprint.route('/hvPost/<hvId>', methods=['GET'])
def send_rv_data(hvId):
    # get data from df for hvId
    hv_subset = df.loc[df['hvId'] == hvId].reset_index(drop=True)

    # create RvResponse for all RVs for hvId
    rv_ids = hv_subset.rv_id.unique()
    rv_list = [RvResponse(rv_id) for rv_id in rv_ids]

    # extract instant and ewma ratings for all RVs
    inst_cols = [col for col in hv_subset.columns if "instant" in col]
    ewma_cols = [col for col in hv_subset.columns if "ewma" in col]
    for rv in rv_list:
        rv_subset = hv_subset.loc[hv_subset.rv_id == rv.id].tail(1)

        rv.instants = rv_subset.loc[:, inst_cols].values.astype(int).tolist()[0]
        rv.ewmas = rv_subset.loc[:, ewma_cols].values.astype(float).tolist()[0]

        # extract vehicle type for all RVs from ground truth
        rv.type = rv_subset['groundTruth.attackType'].item()

    response = [rv.__repr__() for rv in rv_list]

    response_object = {
            'status': 200,
            'rvData': response
            }
    return jsonify(response_object), 200


@main_blueprint.route('/simulation/<command>', methods=['GET', 'POST'])
def simulationControl(command):
    global N
    global VN
    global count
    global v_count
    global df
    global vdf

    if request.method == 'POST':
        data = request.get_json()
        if command == "reset":
            now = dt.now().__format__("_%Y_%m_%d_%H_%M_%S")
            N = 5000
            count = 0
            df_filename = server_side + "data/" +\
                    data['attack'] + now
            df.to_hdf(df_filename + ".h5", key="df", mode="a")
            df.to_csv(df_filename + ".csv", mode="a")
            df = df.iloc[0:0]
            df = df.reindex(np.arange(N))

            VN = 5000
            v_count = 0
            vdf_filename = df_filename + "_veh_density"
            vdf.to_hdf(vdf_filename + ".h5", key="vdf", mode="a")
            vdf.to_csv(vdf_filename + ".csv", mode="a")
            vdf = vdf.iloc[0:0]
            vdf = vdf.reindex(np.arange(N))
        elif command == "save":
            now = dt.now().__format__("_%Y_%m_%d_%H_%M_%S")
            df_filename = server_side + "data/" +\
                    data['attack'] + now
            df_to_save = df.dropna()
            df_to_save.to_hdf(df_filename + ".h5", key="df", mode="a")
            df_to_save.to_csv(df_filename + ".csv", mode="a", index=False)

            vdf_filename = df_filename + "_veh_density"
            vdf_to_save = vdf.dropna()
            vdf_to_save.to_hdf(vdf_filename + ".h5", key="vdf", mode="a")
            vdf_to_save.to_csv(vdf_filename + ".csv", mode="a", index=False)
        elif command == "loadsession":
            df_file_path = server_side + "data/"
            df = pd.read_hdf(df_file_path + data['session'] + ".h5", key="df")
            vdf = pd.read_hdf(df_file_path + data['session'] + "_veh_density.h5", key="vdf")
        elif command == "stop":
            now = dt.now().__format__("_%Y_%m_%d_%H_%M_%S")
            df_filename = server_side + "data/" +\
                    data['attack'] + now
            df_to_save = df.dropna()
            df_to_save.to_hdf(df_filename + ".h5", key="df", mode="a")
            df_to_save.to_csv(df_filename + ".csv", mode="a", index=False)

            vdf_filename = df_filename + "_veh_density"
            vdf_to_save = vdf.dropna()
            vdf_to_save.to_hdf(vdf_filename + ".h5", key="vdf", mode="a")
            vdf_to_save.to_csv(vdf_filename + ".csv", mode="a", index=False)
            response_object = {
                    'status': 200,
                    'msg': "process terminated"
                    }
            return jsonify(response_object), 200

    elif request.method == 'GET':
        if command == "loadsession":
            sessions_path = server_side + "data/*.h5"
            sessions = [session.split('/')[-1].split('.')[0] for session in
                    glob.glob(sessions_path) if 'veh_density' not in session]
            sessions.sort()

            if sessions is not None:
                response_object = {
                        'status': 200,
                        'sessions': sessions
                        }
                return jsonify(response_object), 200
            else:
                response_object = {
                        'status': 500,
                        'msg': "No sessions found"
                        }
                return jsonify(response_object), 500

    # run only if no other code branch is taken; meaning the request was bad
    response_object = {
            'status': 400,
            'msg': "invalid request"
            }
    return jsonify(response_object), 400

