def store_task(data_str, count, cols):
    print('Starting store_task')

    # data = data_str.split(',')
    # df.iloc[count] = data

    import os
    with open(os.environ.get('HOME') + "/src/veins/examples/mps/results/pandas.csv", "a") as fh:
        if count == 0:
            for i, col in enumerate(cols):
                fh.write(col)
                if i != len(cols)-1:
                    fh.write(",")
                elif i == len(cols)-1:
                    fh.write("\n")
        fh.write(data_str + "\n")

    print('Task Complete')
    return True

