function getWindow(lastDate) {
    var window = $('meta[name=x_window]').attr("content");
    var lastDateObj = new Date(lastDate);
    var windowDateObj = lastDateObj.setSeconds(lastDateObj.getSeconds() - window);
    return windowDateObj;
}

function makeLatencyPlot(){
  var additional_params = {
    responsive: true
  };

  var layout = {};

  // Setup plots
  var simRealPlot = {
    name: 'sim-real',
    type: 'scatter'
  };
  var simRealDiv = document.getElementById("latency-sim-real-plot");
  Plotly.react(simRealDiv, simRealPlot, layout, additional_params);

  var simBsmPlot = {
    type: 'scatter'
  };
  var simBsmDiv = document.getElementById("latency-sim-bsm-plot");
  Plotly.react(simBsmDiv, simBsmPlot, layout, additional_params);

  var simDistPlot = {
    type: 'scatter'
  };
  var simDistDiv = document.getElementById("latency-sim-dist-plot");
  Plotly.react(simDistDiv, simDistPlot, layout, additional_params);

  var simSimPlot = {
    type: 'scatter'
  };
  var simSimDiv = document.getElementById("latency-sim-sim-plot");
  Plotly.react(simSimDiv, simSimPlot, layout, additional_params);

  var realBsmPlot = {
    type: 'scatter'
  };
  var realBsmDiv = document.getElementById("latency-real-bsm-plot");
  Plotly.react(realBsmDiv, realBsmPlot, layout, additional_params);

  var realDistPlot = {
    type: 'scatter'
  };
  var realDistDiv = document.getElementById("latency-real-dist-plot");
  Plotly.react(realDistDiv, realDistPlot, layout, additional_params);

  var bsmDistPlot = {
    type: 'scatter'
  };
  var bsmDistDiv = document.getElementById("latency-bsm-dist-plot");
  Plotly.react(bsmDistDiv, bsmDistPlot, layout, additional_params);

    //var windowDateObj = getWindow(x[x.length - 1])
    //var layout = {
      //grid: { rows: 3, columns: 3, pattern: 'independent' },
      //font: { size: 18 },
      //margin: { t: 0 },
      //xaxis: {
        //range: [windowDateObj,  x[x.length - 1]],
        //rangeslider: {range: [x[0], x[x.length - 1]]},
        //type: 'scatter'
      //},
      //yaxis: {
        //range: [0, 110]
      //}
    //};
};


function updatePlot(x, y, title, plot_name) {
  var i = 0;
  var data = new Array();
  for (i = 0; i < x.length; i++) {
    data.push(
        {
          x: x[i],
          y: y[i],
          type: 'scatter'
        }
      );
  }

  var layout = {
    title: title
  }

  var plotDiv = document.getElementById(plot_name);
  Plotly.react(plotDiv, data, layout);
}


var url = 'http://' + document.domain + ':' + location.port
var socket = io.connect(url);

socket.on('connect', function(msg) {
  console.log('connected to websocket on ' + url);
});

socket.on('bootstrap', function (msg) {
  plot_start = msg.x[0];
  console.log('bootstrapping');
});
 
socket.on('setup-latency-plot', function (msg) {
  makeLatencyPlot();
  console.log('creating latency plot')
})

socket.on('update-sim-real-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'SimTime vs RealTime Latency', 'latency-sim-real-plot');
  console.log('updating sim real plot');
});

socket.on('update-sim-bsm-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'SimTime vs BSM Latency', 'latency-sim-bsm-plot');
  console.log('updating sim bsm plot');
});

socket.on('update-sim-dist-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'SimTime vs Distance Traveled', 'latency-sim-dist-plot');
  console.log('updating sim dist plot');
});

socket.on('update-sim-sim-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'SimTime vs SimTime Latency', 'latency-sim-sim-plot');
  console.log('updating sim sim plot');
});

socket.on('update-real-bsm-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'RealTime vs BSM Latency', 'latency-real-bsm-plot');
  console.log('updating real bsm plot');
});

socket.on('update-real-dist-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'RealTime vs Distance Latency', 'latency-real-dist-plot');
  console.log('updating real dist plot');
});

socket.on('update-bsm-dist-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, '#BSM vs Distance Latency', 'latency-bsm-dist-plot');
  console.log('updating bsm dist plot');
});

