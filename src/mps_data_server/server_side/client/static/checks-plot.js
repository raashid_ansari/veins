var url = 'http://' + document.domain + ':' + location.port;
var socket = io.connect(url);

// colors
const red = "#FF0000";
const green = "#008000";
const orange = "#FFA500";
const black = "#000000";

socket.on('connect', function(msg) {
  console.log('connected to websocket on ' + url);
});


$("#hvList").mouseenter(function() {
  //console.log("Hovering over hvList");
  //get updated list
  $.getJSON("/hv", null, function(data, statusTxt, xhr) {
      $("#hvList option").remove(); // Remove all <option> child tags.
      //console.log(data);
      $.each(data.hvIds, function(index, item) { // Iterates through a collection
          $("#hvList").append( // Append an object to the inside of the select box
              $("<option></option>") // Yes you can do this.
                  .text(item)
                  .val(item)
          );
      });
  });
});


$(document).on('click', '#hvList option', function(event){
  //console.log("selected option", $("#hvList option:selected").html());
  event.preventDefault();
  $.getJSON("/hvPost/" + $(this).text(), function(responseTxt, statusTxt, xhr) {
    if (statusTxt == "success") {
      var tmp = responseTxt.rvData;

      var rvData = new Array();
      $.each(tmp, function(i, d) {
        rvData.push(JSON.parse(d));
      });
      rower(rvData);
    }
  });
  poll();
});


// poll every 2 seconds for new data
function poll() {
  setTimeout(function() {
    $.ajax({
      url: "/hvPost/" + $("#hvList option:selected").text(),
      type: "GET",
      success: function(data) {
        var tmp = data.rvData;
        var rvData = new Array();
        $.each(tmp, function(i, d) {
          rvData.push(JSON.parse(d));
        });
        rower(rvData);
      },
      dataType: "json",
      complete: poll,
      timeout: 2000
    })
  }, 1000);
}


//$(window).resize(function() {
    //if(this.resizeTO) clearTimeout(this.resizeTO);
    //this.resizeTO = setTimeout(function() {
        //$(this).trigger('resizeEnd');
    //}, 200);
//});

//$(window).bind('resizeEnd', function() {
    //var width = d3.select("#checks-plot")._groups[0][0].clientWidth;
    //var height = rvData.length * 170;
    //$("#checks-plot svg").css("height", height).css("width", width);
    //draw(width, height);
//});

const checksPlot = d3.select("#checks-plot")

function rower(rvData) {
  checksPlot.selectAll("*").remove();
  // add svg element
  const svgWidth = checksPlot._groups[0][0].clientWidth;
  const svgHeight = 500;
  var svg = checksPlot.append("svg")
    .attr("width", svgWidth)
    .attr("height", svgHeight);

  // add header text
  const headers = ["Host", "ART", "MBF", "SAW", "MVP", "MVN", "MAP", "MAN", "MDM",
    "Kal", "FBR", "Susp", "ML"];
  const headerHeight = 20;
  var header = svg.append("g")
    .attr("transform", "translate(0," + headerHeight + ")");

  header.selectAll("text")
    .data(headers)
  .enter()
    .append("text")
    .attr("text-anchor", "middle")
    .text(function(d, i) {return d})
      .attr("x", function(d, i) {return (svgWidth * i / headers.length) + 30});

  // draw header line
  const headerLineHeight = 10;
  const headerLineWidthConst = 50;
  var headerLine = header.append("line")
    .attr("x1", 0)
    .attr("y1", headerLineHeight)
    .attr("x2", svgWidth - headerLineWidthConst)
    .attr("y2", headerLineHeight)
    .attr("stroke", black);

  // add row as a group
  const row = svg.append("g")
    .attr("transform", "translate(0, 60)")
    .selectAll("g")
    .data(rvData)
  .enter()
    .append("g")
      .attr("transform", function(d, i) {
        var h = i * 120;
        return "translate(0," + h + ")"
      });

  const rvWidth = 50;
  const rvHeight = rvWidth / 2;
  const rvDy = 15;
  const rv = row.append("g")
    .attr("transform", function(d, i) {
      var t = "translate(0, " + rvDy + ")";
      return t;
    });

  rv.append("rect")
    .attr("width", rvWidth)
    .attr("height", rvHeight)
    .style("fill", function(d) {
      switch(d.type) {
        case "Genuine":
          return green;
        default:
          return red;
      }
    });

  rv.append("text")
    .attr("x", 16)
    .attr("y", 19)
    .text(function(d) {return d.id;});

  const ratingRow = row.append("g")
    .attr("transform", "translate(100, 0)");

  function circles(ctx) {

    function valToColor(d, i) {
      if (i == headers.length - 3) {
        d = d / (headers.length - 2);
      }

      if (d >= 0.0 && d < 0.5) {
        return green;
      } else if (d >= 0.5 && d < 0.98) {
        return orange;
      } else {
        return red;
      }
    }

    var w = svgWidth / headers.length;
    var newCtx = ctx.enter()
      .append("g")
        .attr("transform", function(d, i) {
          var t = "translate(" + ((i + 1) * w - 70) + ", 0)";
          return t;
        });


    newCtx.append("circle")
      .attr("r", function(d, i) {
        function rad (d, i) {
          if (i == headers.length - 3) {
            return d / (headers.length - 2);
          } else if (i == headers.length - 2) {
            return 1.0;
          } else {
            return d;
          }
        }
        var radius = rad(d, i) * 20;
        return radius;
      })
      .style("fill", function(d, i) {
        return valToColor(d, i);
      })
      .style("stroke", function(d, i) {
        return valToColor(d, i);
      })
      .style("stroke-width", 1.5)
      .attr("fill-opacity", 0.6);

    newCtx.append("text")
      .attr("text-anchor", "middle")
      .attr("y", 5)
      .attr("font-size", "10px")
      .text(function(d) { return d; });
  }

  const instantGroup = ratingRow.append("g")
    .selectAll("g")
    .data(function(d) { return d.instants; });
  circles(instantGroup);

  const ewmaGroup = ratingRow.append("g")
    .attr("transform", "translate(0, 50)")
    .selectAll("g")
    .data(function(d) { return d.ewmas; });
  circles(ewmaGroup);
}


//function draw(width, height) {
  //svg.attr("width", width)
    //.attr("height", height);

  //header.selectAll("text")
    //.attr("x", function(d, i) {return width * i / headers.length});

  //headerLine.attr("x2", width - headerLineWidthConst);

  //row.select("g")
    //.selectAll("g")
  //.enter()
    //selectAll("g")
  //.enter()
    //k
    //.selectAll("circle")
    //.attr("cx", function(d, i) {
      //return width * i / headers.length;
    //});
//}

