// Load all saved sessions' list
$.getJSON("/simulation/loadsession", null, function(data, statusTxt, xhr) {
    $("#loadsession option").remove(); // Remove all <option> child tags.
    console.log(data);
    $.each(data.sessions, function(index, item) { // Iterates through a collection
        $("#loadsession").append( // Append an object to the inside of the select box
            $("<option></option>") // Yes you can do this.
                .text(item)
                .val(item)
        );
    });
});


// Load the data in selected session into current dataframe
$(document).on('click', '#loadsession option', function() {
  console.log("selected", $("#loadsession option:selected").html());
  var getSessionData = JSON.stringify(
      {
        session: $("#loadsession option:selected").html(),
      });
  console.log("get session Data:", getSessionData);

  $.ajax({
    url: "/simulation/loadsession",
    contentType: "application/json",
    data: getSessionData,
    success: function(data, statusTxt, xhr) {
      console.log(data);
    },
    dataType: "json",
    method: "POST"
  });
});


// Reset Button action
var resetBtn = $("#resetDataBtn");
resetBtn.click(function() {
  var resetSimData = JSON.stringify(
      {
        city: $("#city option:selected").html(),
        attack: $("#attack option:selected").html(),
        gui: $('#guicheckbox').prop('checked')
      });
  console.log("Reset Sim Data:", resetSimData);

  $.ajax({
    url: "/simulation/reset",
    contentType: "application/json",
    data: resetSimData,
    success: function(data, statusTxt, xhr) {
      if (statusTxt == "success") {
        console.log("RESET", data);
      } else {
        console.log("FAILED RESET", data, statusTxt);
      }
    },
    dataType: "json",
    method: "POST"
  });
});


// Save Button action
var saveSessionBtn = $("#saveSessionBtn");
saveSessionBtn.click(function() {
  var saveSimData = JSON.stringify(
      {
        city: $("#city option:selected").html(),
        attack: $("#attack option:selected").html(),
        gui: $('#guicheckbox').prop('checked')
      });
  console.log("Save Sim Data:", saveSimData);

  $.ajax({
    url: "/simulation/save",
    contentType: "application/json",
    data: saveSimData,
    success: function(data, statusTxt, xhr) {
      if (statusTxt == "success") {
        console.log("SAVE", data);
      } else {
        console.log("FAILED SAVE", data, statusTxt);
      }
    },
    dataType: "json",
    method: "POST"
  });
});

