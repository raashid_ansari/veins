function getWindow(lastDate) {
    var window = $('meta[name=x_window]').attr("content");
    var lastDateObj = new Date(lastDate);
    var windowDateObj = lastDateObj.setSeconds(lastDateObj.getSeconds() - window);
    return windowDateObj;
}

function makeMpsPlot(){
  var additional_params = {
    responsive: true
  };

  var layout = {
    grid: {
      rows: 3,
      columns: 3,
      pattern: 'independent'},
  };

  // Setup plots
  var baseConfusionPlot = {
    name: 'Count',
    type: 'bar',
    title: "Confusion",
    xaxis: {
      tickangle: 90,
      tickfont: {
        size: 10
      }
    },
    yaxis: {
      zeroline: false,
      gridwidth: 2
    },
    barmode: 'stack'
  };

  var rocPlot = {
    name: 'ROC',
    type: 'scatter',
    xaxis: 'x2',
    yaxis: 'y2'
  };

  var prPlot = {
    name: 'PR',
    type: 'scatter',
    xaxis: 'x3',
    yaxis: 'y3'
  };

  var accuracyPlot = {
    name: 'Accuracy',
    type: 'scatter',
    xaxis: 'x4',
    yaxis: 'y4'
  };

  var precisionPlot = {
    name: 'Precision',
    type: 'scatter',
    xaxis: 'x5',
    yaxis: 'y5'
  };

  var recallPlot = {
    name: 'Recall',
    type: 'scatter',
    xaxis: 'x6',
    yaxis: 'y6'
  };

  var f1ScorePlot = {
    name: 'F1 Score',
    type: 'scatter',
    xaxis: 'x7',
    yaxis: 'y7'
  };

  var vehicleDensityPlot = {
    name: 'Vehicle Density',
    type: 'scatter',
    xaxis: 'x8',
    yaxis: 'y8'
  };

  var mpsPlot = [baseConfusionPlot, rocPlot, prPlot, accuracyPlot,
    precisionPlot, recallPlot, f1ScorePlot, vehicleDensityPlot];
  var mpsPlotDiv = document.getElementById("mps-performance-plot");
  Plotly.react(mpsPlotDiv, layout, additional_params);

    //var windowDateObj = getWindow(x[x.length - 1])
    //var layout = {
      //grid: { rows: 3, columns: 3, pattern: 'independent' },
      //font: { size: 18 },
      //margin: { t: 0 },
      //xaxis: {
        //range: [windowDateObj,  x[x.length - 1]],
        //rangeslider: {range: [x[0], x[x.length - 1]]},
        //type: 'scatter'
      //},
      //yaxis: {
        //range: [0, 110]
      //}
    //};
};

var plot_start = 0;
var firstTime = true

function updateBaseConfusionPlot(x, y) {
  var i = 0;
  var data = new Array();
  var name_list = ['TP', 'FP', 'FN', 'TN'];
  var color_list = ['red', 'blue', 'green', 'yellow'];

  if (name_list.length != y.length) {
    console.log('length not same for name_list and y');
  }

  if (name_list.length != color_list.length) {
    console.log('length not same for name_list and color_list');
  }

  for (i = 0; i < y.length; i++) {
    data.push(
        {
          x: x,
          y: y[i],
          type: 'bar',
          name: name_list[i],
          marker: {
            color: color_list[i],
            opacity: 0.7
          }
        }
      );
  }

  // plot
  var plotDiv = document.getElementById("mps-base-confusion-plot");
  if (firstTime) {
    //firstTime = false;
    Plotly.react(plotDiv, data, layout_update);//, [0]);
  } else {
    Plotly.extendTraces(plotDiv, data, [0]);
  }
};


function updatePlot(x, y, title, plot_name) {
  var i = 0;
  var data = new Array();
  for (i = 0; i < x.length; i++) {
    data.push(
        {
          x: x[i],
          y: y[i],
          type: 'scatter'
        }
      );
  }

  var layout = {
    title: title
  }

  var plotDiv = document.getElementById(plot_name);
  Plotly.react(plotDiv, data, layout);
}


var url = 'http://' + document.domain + ':' + location.port
var socket = io.connect(url);

socket.on('connect', function(msg) {
  console.log('connected to websocket on ' + url);
});

socket.on('bootstrap', function (msg) {
  plot_start = msg.x[0];
  console.log('bootstrapping');
});

socket.on('setup-mps-plot', function (msg) {
  makeMpsPlot();
  console.log('creating mps plot')
})

socket.on('update-mps-base-confusion-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updateBaseConfusionPlot( parsed.x, parsed.y );
  console.log('updating base confusion plot');
});

socket.on('update-mps-roc-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'ROC', 'mps-roc-plot');
  console.log('updating roc plot');
});

socket.on('update-mps-pr-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'PR', 'mps-pr-plot');
  console.log('updating pr plot');
});

socket.on('update-mps-precision-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'Precision', 'mps-precision-plot');
  console.log('updating precision plot');
});

socket.on('update-mps-recall-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'Recall', 'mps-recall-plot');
  console.log('updating recall plot');
});

socket.on('update-mps-f1score-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'F1-Score', 'mps-f1score-plot');
  console.log('updating f1score plot');
});

socket.on('update-mps-vehicle-density-plot', function (msg) {
  var parsed = JSON.parse(msg);
  updatePlot(parsed.x, parsed.y, 'Vehicle Density', 'mps-vehicle-density-plot');
  console.log('updating vehicle-density plot');
});
