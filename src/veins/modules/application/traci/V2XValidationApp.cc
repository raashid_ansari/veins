//
// Copyright (C) 2016 David Eckhoff <david.eckhoff@fau.de>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include <chrono>
#include <cmath>
#include <ctime>

#include "V2XValidationApp.h"

#include "veins/mps/utility/CSVWriter/include/CSVWriter.h"

#define CSV(X) for (int i = 0; i < X; ++i) csv << "";

Define_Module(V2XValidationApp);

void V2XValidationApp::initialize(int stage) {
    BaseWaveApplLayer::initialize(stage);
    if (stage == 0) {
        //Initializing members and pointers of your application goes here
        EV << "Initializing " << par("appName").stringValue() << std::endl;
        brake_ = par("brake").boolValue();
    }
    else if (stage == 1) {
        //Initializing members that require initialized other modules goes here
    }
}

void V2XValidationApp::finish() {
    BaseWaveApplLayer::finish();
    //statistics recording goes here

}

void V2XValidationApp::onBSM(BasicSafetyMessage* bsm) {
  //Your application has received a beacon message from another car or RSU
  //code for handling the message goes here

  auto chronoNow = std::chrono::system_clock::now();
  auto now = std::chrono::system_clock::to_time_t(chronoNow);
  auto localTime = std::localtime(&now);

  char nowFmt[18];
  std::strftime(nowFmt, sizeof(nowFmt), "%Y/%m/%d-%H:%M:", localTime);

  auto simTimePoint = simTime().dbl();

//  if (firstTime_) {
//    initMs_ = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
//    firstTime_ = false;
//  }
//  auto tmpMs = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
//  auto ms = (tmpMs - initMs_) % 1000;

  auto timeStamp = std::ostringstream{};
  timeStamp << nowFmt << simTimePoint;

  char year[5];
  std::strftime(year, sizeof(year), "%Y", localTime);

  char month[3];
  std::strftime(month, sizeof(month), "%m", localTime);

  char day[3];
  std::strftime(day, sizeof(day), "%d", localTime);

  char hour[3];
  std::strftime(hour, sizeof(hour), "%H", localTime);

  char minute[3];
  std::strftime(minute, sizeof(minute), "%M", localTime);

  auto timeInfo = std::tm();
  timeInfo.tm_year = atoi(year) - 1900;
  timeInfo.tm_mon = atoi(month) - 1;
  timeInfo.tm_mday = atoi(day);
  timeInfo.tm_hour = atoi(hour);
  timeInfo.tm_min = atoi(minute);

  auto tt = std::mktime(&timeInfo);
  auto tp = std::chrono::system_clock::from_time_t(tt);

  int secondsPart = simTimePoint;
  auto milliSecondsPart = simTimePoint - secondsPart;
  int hoursPart = secondsPart/3600;
  int minutesPart = (secondsPart - hoursPart * 3600)/60;
  secondsPart = secondsPart - minutesPart * 60 - hoursPart * 3600;

  auto msPartStream = std::ostringstream{};
  msPartStream << milliSecondsPart;

  auto msPartString = msPartStream.str();
  auto msIdx = msPartString.find('.');
  msPartStream.str("");
  for (int i = msIdx + 1; i < msIdx + 4; ++i)
    if (i != msIdx)
      msPartStream << msPartString[i];

  milliSecondsPart = hoursPart * 3600000 + minutesPart * 60000 + secondsPart * 1000 + atoi(msPartStream.str().c_str());


  long tpMs = std::chrono::duration_cast<std::chrono::milliseconds>(tp.time_since_epoch()).count() + milliSecondsPart;

//  std::cout << "Hours: " << hoursPart << " Minutes: " << minutesPart << " Seconds: " << secondsPart << " MilliSeconds: " << milliSecondsPart <<
//      " tpms: " << tpMs << " msPart: " << milliSecondsPart << " both: " << tpMs << " Lat: " << bsm->getSenderPos().x << " Lon: " << bsm->getSenderPos().y <<
//      " Lat traci: " << connection_->omnet2traci(bsm->getSenderPos()).x << " Lon traci: " << connection_->omnet2traci(bsm->getSenderPos()).y << std::endl;

  auto gps = traci->getLonLat(bsm->getSenderPos());
  auto speed = std::sqrt(std::pow(bsm->getSenderSpeed().x, 2) + std::pow(bsm->getSenderSpeed().y, 2));
  auto heading = std::sqrt(std::pow(bsm->getSenderOrientation().x, 2) + std::pow(bsm->getSenderOrientation().y, 2));

  auto csv = CSVWriter(",");
  csv.newRow() <<
      timeStamp.str() <<
      tpMs <<
      "TX" <<
      "" << "" << "" <<
      172 <<
      "" << "" <<
      12 <<
      "" << "" << "" <<
      1 <<
      "" <<
      bsm->getMsgId() <<
      bsm->getMsgCount() <<
      "" <<
      3 <<
      "" <<
      gps.second << gps.first << 0 <<
      "" << "" << "" <<
      "5" <<
      speed <<
      heading <<
      "" <<
      bsm->getSenderAcceleration().x <<
      bsm->getSenderAcceleration().y <<
      bsm->getSenderAcceleration().z <<
      0 <<
      "" << "" << "" << "" <<
      bsm->getWidth() <<
      bsm->getLength();
  CSV(122);
  csv << 3276.7;
  CSV(14);
  csv << bsm->getHeight() <<
      0.43 << 0.43 << 1700 << 4;
  CSV(10);

  auto fileName = std::ostringstream{};
  fileName << findHost()->getId() << "_log.csv";

  if (!csv.writeToFile(fileName.str(), true))
    throw std::runtime_error("Couldn't write log to file");
}

void V2XValidationApp::onWSM(WaveShortMessage* wsm) {
    //Your application has received a data message from another car or RSU
    //code for handling the message goes here, see TraciDemo11p.cc for examples

}

void V2XValidationApp::onWSA(WaveServiceAdvertisment* wsa) {
    //Your application has received a service advertisement from another car or RSU
    //code for handling the message goes here, see TraciDemo11p.cc for examples

}

void V2XValidationApp::handleSelfMsg(cMessage* msg) {
    BaseWaveApplLayer::handleSelfMsg(msg);
    //this method is for self messages (mostly timers)
    //it is important to call the BaseWaveApplLayer function for BSM and WSM transmission

}

void V2XValidationApp::handlePositionUpdate(cObject* obj) {
    BaseWaveApplLayer::handlePositionUpdate(obj);
    //the vehicle has moved. Code that reacts to new positions goes here.
    //member variables such as currentPosition and currentSpeed are updated in the parent class
    auto simTimePoint = simTime().dbl();

    if (simTimePoint > 30.0 && simTimePoint < 40.0 && brake_) {
      traciVehicle->slowDown(0.0, 2.0);
    } else if (simTimePoint > 70.0 && simTimePoint < 80.0 && brake_) {
      traciVehicle->slowDown(0.0, 0.0);
    } else {
      traciVehicle->slowDown(30.0, 2.0);
    }
}
