//
// Copyright (C) 2016 David Eckhoff <david.eckhoff@fau.de>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef __VEINS_SILENTPERIODAPP_H_
#define __VEINS_SILENTPERIODAPP_H_

#include <omnetpp.h>
#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"

using namespace omnetpp;

/**
 * @brief
 * This is a stub for a typical Veins application layer.
 * Most common functions are overloaded.
 * See SilentPeriodApp.cc for hints
 *
 * @author David Eckhoff
 *
 */

class SilentPeriodApp : public BaseWaveApplLayer {
public:
  virtual void initialize(int stage) override;
  virtual void finish() override;
protected:
  virtual void onBSM(BasicSafetyMessage* bsm) override;
  virtual void onWSM(WaveShortMessage* wsm) override;
  virtual void onWSA(WaveServiceAdvertisment* wsa) override;

  virtual void handleSelfMsg(cMessage* msg) override;
  virtual void handlePositionUpdate(cObject* obj) override;
private:
  int       scheme_;
  int       state_;

  simtime_t lastPseudoChangeTime_;
  simtime_t lastSilentPeriodTime_;
  simtime_t timeAtFirstChange_;

  double    pseudoChangePeriod_;
  double    silentPeriod_;

  Coord     lastPosition_;

  double    distanceFromStartPosition_;
  double    distanceFromFirstChangePosition_;
  double    distanceFromSecondChangePosition_;
  double    distanceFromThirdChangePosition_;
  double    distanceFromLastChangePosition_;

  int       nextChangePosition_;

  int       firstChangePosition_;
  int       secondChangeTime_;
  int       thirdChangePosition_;
  int       fourthChangePosition_;

  enum Scheme {
    NO_PC_SP,
    SIMPLE_PC_SP,
    RANDOM_PC_WITH_SP,
    C2C,
    C2C_WITH_SP
  };

  enum C2CStates {
    START_POSITION,
    FIRST_CHANGE,
    SECOND_CHANGE,
    THIRD_CHANGE,
    FOURTH_CHANGE,
  };

};

#endif
