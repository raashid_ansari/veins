//
// Copyright (C) 2016 David Eckhoff <david.eckhoff@fau.de>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include <veins/mps/utility/TypeDefs.h>
#include "SilentPeriodApp.h"

Define_Module(SilentPeriodApp);

void SilentPeriodApp::initialize(int stage) {
    BaseWaveApplLayer::initialize(stage);
    if (stage == 0) {
      //Initializing members and pointers of your application goes here
      EV << "Initializing " << par("appName").stringValue() << std::endl;

      scheme_ = par("scheme").intValue();

      if (scheme_ != NO_PC_SP) {
        lastPseudoChangeTime_ = simTime();
        lastSilentPeriodTime_ = simTime();
        silentPeriod_         = par("silentPeriod").doubleValue();
        pseudoChangePeriod_   = par("pseudoChangePeriod").doubleValue();

        if (silentPeriod_ > pseudoChangePeriod_)
          error("Silent Period (%d) cannot be greater than Pseudonym Change Period (%d)", silentPeriod_, pseudoChangePeriod_);

        if (scheme_ == RANDOM_PC_WITH_SP) {
          distanceFromLastChangePosition_ = 0.0;
          nextChangePosition_ = intrand(700, 0) + 800;   // output = [800, 1500)
          lastPosition_ = curPosition;
        }

        if (scheme_ == C2C || scheme_ == C2C_WITH_SP) {
          distanceFromStartPosition_ = 0.0;
          distanceFromFirstChangePosition_ = 0.0;
          distanceFromSecondChangePosition_ = 0.0;
          distanceFromThirdChangePosition_ = 0.0;

          firstChangePosition_  = intrand(700, 0)   + 800;   // output = [800, 1500)
          secondChangeTime_     = intrand(240, 0)   + 120;   // output = [240, 360)
          thirdChangePosition_  = intrand(10000, 0) + 10000; // output = [10000, 20000)
          fourthChangePosition_ = intrand(10000, 0) + 25000; // output = [25000, 35000)

//          std::cout <<
//              "\nFC: " << firstChangePosition_ <<
//              " SC: " << secondChangeTime_ <<
//              " TC: " << thirdChangePosition_ <<
//              " FC: " << fourthChangePosition_ <<
//              std::endl;

          state_ = START_POSITION;
        }
      }
    }
    else if (stage == 1) {
        //Initializing members that require initialized other modules goes here
    }
}

void SilentPeriodApp::finish() {
    BaseWaveApplLayer::finish();
    //statistics recording goes here

}

void SilentPeriodApp::onBSM(BasicSafetyMessage* bsm) {
  //Your application has received a beacon message from another car or RSU
  //code for handling the message goes here

}

void SilentPeriodApp::onWSM(WaveShortMessage* wsm) {
    //Your application has received a data message from another car or RSU
    //code for handling the message goes here, see TraciDemo11p.cc for examples

}

void SilentPeriodApp::onWSA(WaveServiceAdvertisment* wsa) {
    //Your application has received a service advertisement from another car or RSU
    //code for handling the message goes here, see TraciDemo11p.cc for examples

}

void SilentPeriodApp::handleSelfMsg(cMessage* msg) {
    BaseWaveApplLayer::handleSelfMsg(msg);
    //this method is for self messages (mostly timers)
    //it is important to call the BaseWaveApplLayer function for BSM and WSM transmission
}

void SilentPeriodApp::handlePositionUpdate(cObject* obj) {
    BaseWaveApplLayer::handlePositionUpdate(obj);
    //the vehicle has moved. Code that reacts to new positions goes here.
    //member variables such as currentPosition and currentSpeed are updated in the parent class
    switch(scheme_) {
    case NO_PC_SP: {
      break;
    }

    case SIMPLE_PC_SP: {

      if (simTime() - lastPseudoChangeTime_ > pseudoChangePeriod_) {
        BaseWaveApplLayer::changePseudoId_ = true;
        lastPseudoChangeTime_ = simTime();

        if (silentPeriod_ > 0)
          BaseWaveApplLayer::isSilentPeriod_ = true;
        lastSilentPeriodTime_ = simTime();
      } else {
        BaseWaveApplLayer::changePseudoId_ = false;

        if (simTime() - lastSilentPeriodTime_ > silentPeriod_)
          BaseWaveApplLayer::isSilentPeriod_ = false;
      }
      break;
    }

    case RANDOM_PC_WITH_SP: {

      if (simTime() - lastSilentPeriodTime_ > silentPeriod_)
        BaseWaveApplLayer::isSilentPeriod_ = false;

      distanceFromLastChangePosition_ += curPosition.distance(lastPosition_);
      lastPosition_ = curPosition;
      if (distanceFromLastChangePosition_ >= nextChangePosition_) {
        nextChangePosition_ = intrand(700, 0) + 800; //output = [800, 1500)
//        std::cout << "Next Change Position: " << nextChangePosition_ << " distance: " << distanceFromLastChangePosition_ << " Sim Time: " << simTime() << std::endl;
        distanceFromLastChangePosition_ = 0.0;
        BaseWaveApplLayer::changePseudoId_ = true;

        if (silentPeriod_ > 0)
          BaseWaveApplLayer::isSilentPeriod_ = true;
        lastSilentPeriodTime_ = simTime();
      } else if (BaseWaveApplLayer::changePseudoId_) {
        BaseWaveApplLayer::changePseudoId_ = false;

        if (simTime() - lastSilentPeriodTime_ > silentPeriod_)
          BaseWaveApplLayer::isSilentPeriod_ = false;
      }

      break;

    }

    case C2C: {

      switch(state_) {

      case START_POSITION: {
//        std::cout << "Start Position: " << std::sqrt(curPosition.length()) << " Sim Time: " << simTime() << std::endl;
        BaseWaveApplLayer::changePseudoId_ = true;
        lastPosition_ = curPosition;
        state_++;

        break;
      }

      case FIRST_CHANGE: {
        if (BaseWaveApplLayer::changePseudoId_)
          BaseWaveApplLayer::changePseudoId_ = false;

        distanceFromStartPosition_ += curPosition.distance(lastPosition_);
        lastPosition_ = curPosition;

        if (distanceFromStartPosition_ >= firstChangePosition_) {
//          std::cout << "First Change Position: " << std::sqrt(curPosition.length()) << " distance: " << distanceFromStartPosition_ << " Sim Time: " << simTime() << std::endl;
          BaseWaveApplLayer::changePseudoId_ = true;
          timeAtFirstChange_ = simTime();
          state_++;
        }

        break;
      }

      case SECOND_CHANGE: {
        if (BaseWaveApplLayer::changePseudoId_)
          BaseWaveApplLayer::changePseudoId_ = false;

        distanceFromFirstChangePosition_ += curPosition.distance(lastPosition_);
        lastPosition_ = curPosition;

        if (distanceFromFirstChangePosition_ > 800.0) {
          auto timeFromFirstChange = simTime() - timeAtFirstChange_;
          if (timeFromFirstChange >= secondChangeTime_) {
//            std::cout << "Second Change Position: " << std::sqrt(curPosition.length()) << " distance: " << distanceFromFirstChangePosition_ << " Time From 1st Change: " << timeFromFirstChange << " Sim Time: " << simTime() << std::endl;
            BaseWaveApplLayer::changePseudoId_ = true;
            state_++;
          }
        }

        break;
      }

      case THIRD_CHANGE: {
        if (BaseWaveApplLayer::changePseudoId_)
          BaseWaveApplLayer::changePseudoId_ = false;

        distanceFromSecondChangePosition_ += curPosition.distance(lastPosition_);
        lastPosition_ = curPosition;

        if (distanceFromSecondChangePosition_ >= thirdChangePosition_) {
//          std::cout << "Third Change Position: " << std::sqrt(curPosition.length()) << " distance: " << distanceFromSecondChangePosition_ << " Sim Time: " << simTime() << std::endl;
          BaseWaveApplLayer::changePseudoId_ = true;
          state_++;
        }

        break;
      }

      case FOURTH_CHANGE: {
        if (BaseWaveApplLayer::changePseudoId_)
          BaseWaveApplLayer::changePseudoId_ = false;

        distanceFromThirdChangePosition_ += curPosition.distance(lastPosition_);
        lastPosition_ = curPosition;

        if (distanceFromThirdChangePosition_ >= fourthChangePosition_) {
//          std::cout << "Fourth Change Position: " << std::sqrt(curPosition.length()) << " distance: " << distanceFromThirdChangePosition_ << " Sim Time: " << simTime() << std::endl;
          BaseWaveApplLayer::changePseudoId_ = true;
          state_++;
        }

        break;
      }

      default: {
        BaseWaveApplLayer::changePseudoId_ = false;
        break;
      }
      } // switch (state_) end

//      std::cout << "State #: " << state_ << std::endl;
      break;
    }

    case C2C_WITH_SP: {

      switch(state_) {

      case START_POSITION: {
//        std::cout << "Start Position: " << curPosition.info() << " Sim Time: " << simTime() << std::endl;
        BaseWaveApplLayer::changePseudoId_ = true;
        lastPosition_ = curPosition;

        BaseWaveApplLayer::isSilentPeriod_ = true;
        lastSilentPeriodTime_ = simTime();

        state_++;

        break;
      }

      case FIRST_CHANGE: {
        if (BaseWaveApplLayer::changePseudoId_)
          BaseWaveApplLayer::changePseudoId_ = false;

        if (simTime() - lastSilentPeriodTime_ > silentPeriod_)
          BaseWaveApplLayer::isSilentPeriod_ = false;

        distanceFromStartPosition_ += curPosition.distance(lastPosition_);
        lastPosition_ = curPosition;

        if (distanceFromStartPosition_ >= firstChangePosition_) {
//          std::cout << "First Change Position: " << curPosition.info() << " distance: " << distanceFromStartPosition_ << " Sim Time: " << simTime() << std::endl;
          BaseWaveApplLayer::changePseudoId_ = true;
          timeAtFirstChange_ = simTime();

          BaseWaveApplLayer::isSilentPeriod_ = true;
          lastSilentPeriodTime_ = simTime();

          state_++;
        }

        break;
      }

      case SECOND_CHANGE: {
        if (BaseWaveApplLayer::changePseudoId_)
          BaseWaveApplLayer::changePseudoId_ = false;

        if (simTime() - lastSilentPeriodTime_ > silentPeriod_)
          BaseWaveApplLayer::isSilentPeriod_ = false;

        distanceFromFirstChangePosition_ += curPosition.distance(lastPosition_);
        lastPosition_ = curPosition;

        if (distanceFromFirstChangePosition_ > 800.0) {
          auto timeFromFirstChange = simTime() - timeAtFirstChange_;
          if (timeFromFirstChange >= secondChangeTime_) {
//            std::cout << "Second Change Position: " << curPosition.info() << " distance: " << distanceFromFirstChangePosition_ << "Time From 1st Change: " << timeFromFirstChange << " Sim Time: " << simTime() << std::endl;
            BaseWaveApplLayer::changePseudoId_ = true;

            BaseWaveApplLayer::isSilentPeriod_ = true;
            lastSilentPeriodTime_ = simTime();

            state_++;
          }
        }

        break;
      }

      case THIRD_CHANGE: {
        if (BaseWaveApplLayer::changePseudoId_)
          BaseWaveApplLayer::changePseudoId_ = false;

        if (simTime() - lastSilentPeriodTime_ > silentPeriod_)
          BaseWaveApplLayer::isSilentPeriod_ = false;

        distanceFromSecondChangePosition_ += curPosition.distance(lastPosition_);
        lastPosition_ = curPosition;

        if (distanceFromSecondChangePosition_ >= thirdChangePosition_) {
//          std::cout << "Third Change Position: " << curPosition.info() << " distance: " << distanceFromSecondChangePosition_ << " Sim Time: " << simTime() << std::endl;
          BaseWaveApplLayer::changePseudoId_ = true;

          BaseWaveApplLayer::isSilentPeriod_ = true;
          lastSilentPeriodTime_ = simTime();

          state_++;
        }

        break;
      }

      case FOURTH_CHANGE: {
        if (BaseWaveApplLayer::changePseudoId_)
          BaseWaveApplLayer::changePseudoId_ = false;

        if (simTime() - lastSilentPeriodTime_ > silentPeriod_)
          BaseWaveApplLayer::isSilentPeriod_ = false;

        distanceFromThirdChangePosition_ += curPosition.distance(lastPosition_);
        lastPosition_ = curPosition;

        if (distanceFromThirdChangePosition_ >= fourthChangePosition_) {
//          std::cout << "Fourth Change Position: " << curPosition.info() << " distance: " << distanceFromThirdChangePosition_ << " Sim Time: " << simTime() << std::endl;
          BaseWaveApplLayer::changePseudoId_ = true;

          BaseWaveApplLayer::isSilentPeriod_ = true;
          lastSilentPeriodTime_ = simTime();

          state_++;
        }

        break;
      }

      default: {
        BaseWaveApplLayer::changePseudoId_ = false;
        if (simTime() - lastSilentPeriodTime_ > silentPeriod_)
          BaseWaveApplLayer::isSilentPeriod_ = false;
        break;
      }
      } // switch (state_) end

//      std::cout << "State #: " << state_ << std::endl;
      break;
    }
    default: {
      std::runtime_error("Invalid Scenario");
    }
    } // switch (scenario_) end
}
