//-----------------------------------------------------------
// @author Raashid Ansari
// @date   06-29-2018
// @brief  This class contains commands to generate vehicles
//         in SUMO. It also provides functions to get details
//         about the scenario where a simulation is running
//-----------------------------------------------------------

#ifndef __GHOSTCARS_BASETRAFFICMANAGER_H_
#define __GHOSTCARS_BASETRAFFICMANAGER_H_

#include <omnetpp/csimplemodule.h>
#include <stdint.h>
#include <list>
#include <map>
#include <string>
#include <vector>

#include "veins/modules/mobility/traci/TraCIScenarioManager.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"

/**
 * Class to help control vehicles in a scenario
 */
class BaseTrafficManager : public omnetpp::cSimpleModule
{
public:
    ~BaseTrafficManager();
    BaseTrafficManager();
    void initialize(int stage);
    void finish();
    void createVehicle(const int vehicleType, const int vehicleIdx);
    void setRandomize(bool val);

protected:
//    typedef std::list<std::string> StringList;
    typedef std::vector<std::string>              StringVector_t;
    typedef std::map<std::string, StringVector_t> StringStringVectorMap_t;

    std::map<std::string, int>    roadIds_; // key=road_id, value=number_of_lanes
    StringVector_t                laneIds_;
    StringVector_t                junctionIds_;
    StringVector_t                routeIds_;
    StringVector_t                vehTypeIds_;
    StringStringVectorMap_t       roadIdOfLane_;
    StringStringVectorMap_t       routeStartLaneIds_;
    Veins::TraCIScenarioManager*  traciScenarioManager_;
    Veins::TraCICommandInterface* traciCmdInterface_;
    bool                          initScenario_;
    static bool                   randomize_;

    enum VehicleType {
      GENUINE,
      ATTACKER
    };

protected:
    virtual void handleMessage(cMessage* msg);
    void loadTrafficInfo();
}; // class BaseTrafficManager

#endif
