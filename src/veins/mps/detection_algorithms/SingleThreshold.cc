/*
 * SingleThreshold.cc
 *
 *  Created on: Aug 30, 2018
 *      Author: mps
 */

#include <veins/mps/detection_algorithms/SingleThreshold.h>

SingleThreshold::
SingleThreshold(const double threshold, Sender& sender)
  : threshold_(threshold),
    sender_(sender)
{}



SingleThreshold::
~SingleThreshold() {}



bool SingleThreshold::
isMisbehavior() {
  return
      sender_.getInstantRating().getArt()        > threshold_ ||
      sender_.getInstantRating().getSaw()        > threshold_ ||
      sender_.getInstantRating().getKalman()     > threshold_ ||
      sender_.getInstantRating().getMan() > threshold_ ||
      sender_.getInstantRating().getMap() > threshold_ ||
      sender_.getInstantRating().getMvn()   > threshold_ ||
      sender_.getInstantRating().getMvp()   > threshold_ ||
      sender_.getInstantRating().getMbf()        > threshold_ ||
      sender_.getInstantRating().getFbr()        > threshold_// ||
//      sender_.getInstantRating().getMdm()        > threshold_
     ;
}
