/*
 * BaseMbrApp.cc
 *
 *  Created on: Aug 30, 2018
 *      Author: mps
 */

#include <veins/mps/detection_algorithms/BaseMbrApp.h>

BaseMbrApp::
BaseMbrApp() {}



BaseMbrApp::
~BaseMbrApp()
{
}

double BaseMbrApp::getExecTime() const
{
  return execTime_;
}

void BaseMbrApp::setExecTime(double execTime)
{
  execTime_ = execTime;
}
