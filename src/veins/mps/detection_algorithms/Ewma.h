/*
 * Ewma.h
 *
 *  Created on: Sep 4, 2018
 *      Author: mps
 */

#ifndef SRC_VEINS_MPS_DETECTION_ALGORITHMS_EWMA_H_
#define SRC_VEINS_MPS_DETECTION_ALGORITHMS_EWMA_H_

#include <veins/mps/utility/TypeDefs.h>
#include <veins/mps/detection_algorithms/BaseMbrApp.h>
#include <veins/mps/utility/Sender.h>
#include <veins/mps/utility/Checks.h>

class Ewma : public BaseMbrApp
{
public:
  Ewma(const double threshold, Sender& sender, Checks& checks);
  virtual ~Ewma();
  bool isMisbehavior();

private:
  Sender& sender_;
  const double threshold_;
  Checks& checks_;

  void updateRatings();
  double calcNewRating(const ICheck& check, const int newRating, const double oldRating);
};

#endif /* SRC_VEINS_MPS_DETECTION_ALGORITHMS_EWMA_H_ */
