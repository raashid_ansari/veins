import os

import numpy as np
# from sklearn import datasets
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestClassifier

import sys
sys.path.insert(0, '{}/src/veins/src/scripts'.format(os.environ['HOME']))

from calculationCore import Data

modelTypes = ("ewma", "instant")

class MLModel:
    def __init__(self, attackType):
        # load dataset
        # digits = datasets.load_digits()
        # X, y = digits.data, digits.target

        # setup classifier
        self.base_dir = '{}/src/veins/'.format(os.environ['HOME'])
        self.clf_dir = '{}src/veins/mps/detection_algorithms/classifiers/'.format(self.base_dir)
        self.simulation_dir = '{}examples/mps/results/{}'.format(
                self.base_dir, attackType)

    def train_and_save(self, modelType):
        if modelType not in modelTypes:
            raise ValueError(
                    "Invalid model type. Valid ones are {}".format(modelTypes))

        # load training data
        data = Data()
        data.extract_trace_data("{}_training/".format(self.simulation_dir))
        data = data.df
        X = data.loc[:,
                'ratings.{}.ART'.format(modelType) : 'ratings.{}.suspicion'.format(modelType)]
        y = data['groundTruth.attackGt']
        # print(X,y)

        # train classifier
        clf = RandomForestClassifier(n_estimators = 25)
        clf.fit(X, y)

        # save classifier into a file
        joblib.dump(clf, '{}{}_clf.pkl'.format(self.clf_dir, modelType))

    def get_prediction(self, data, modelType, reporterId):
        if modelType not in modelTypes:
            raise ValueError(
                    "Invalid model type. Valid ones are {}".format(modelTypes))

        # prepare data
        test_data = np.array([[
            data['ART'], data['MBF'], data['SAW'], data['MVP'], data['MVN'],
            data['MAP'], data['MAN'], data['MDM'], data['KALMAN'], data['FBR'],
            data['suspicionRating']
            ]])

        # load the classifier from file
        clf = joblib.load('{}/{}_{}.pkl'.format(
            self.simulation_dir, modelType, reporterId))

        # predict using saved classifier
        prediction = clf.predict(test_data)
        print(prediction)
        return prediction[0]

if __name__ == "__main__":
    # TODO: generalize attack type input
    mlm = MLModel("FakeEEBL")
    for modelType in modelTypes:
        mlm.train_and_save(modelType)

