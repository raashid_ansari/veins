/*
 * BaseMbrApp.h
 *
 *  Created on: Aug 30, 2018
 *      Author: mps
 */

#ifndef SRC_VEINS_MPS_DETECTION_ALGORITHMS_BASEMBRAPP_H_
#define SRC_VEINS_MPS_DETECTION_ALGORITHMS_BASEMBRAPP_H_

#include <string>

#include <veins/mps/utility/TypeDefs.h>
#include <veins/mps/utility/Sender.h>
#include <veins/mps/utility/Checks.h>
#include <veins/mps/utility/Rating.h>

// Logging includes
#include <veins/mps/utility/rapidjson/document.h>
#include "veins/mps/utility/rapidjson/writer.h"
#include "veins/mps/utility/rapidjson/stringbuffer.h"
#include "veins/mps/utility/FileLogger.h"
#include <veins/mps/utility/HttpClient.h>
#include <Poco/Net/HTTPRequest.h>

#include <veins/mps/mb_reports/BaseMbr.h>

class BaseMbrApp : public BaseMbr {
public:
  BaseMbrApp();
  virtual ~BaseMbrApp();
  virtual bool isMisbehavior() = 0;
  double getExecTime() const;
  void setExecTime(double execTime);

private:
  double execTime_;
};

#endif /* SRC_VEINS_MPS_DETECTION_ALGORITHMS_BASEMBRAPP_H_ */
