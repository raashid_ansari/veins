//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <iostream>
#include <string>

#include <veins/mps/detection_algorithms/ML.h>

ML::ML(Sender& sender, int reporter, const std::string& pathPrefix, Checks& checks, std::string& uri, std::string mlType, Rating& ratings)
: sender_(sender),
  reporter_(reporter),
  pathPrefix_(pathPrefix),
  checks_(checks),
  uri_(uri),
  mlType_(mlType),
  ratings_(ratings)
{}

ML::~ML()
{}



bool ML::
isMisbehavior() {
  // connect to ML server
  // send basic check ratings
  auto hc = HttpClient(uri_);
  auto requestStatus = hc.sendRequest(makeSenderData(), Poco::Net::HTTPRequest::HTTP_POST);
  rapidjson::Document d;
  d.Parse(requestStatus.c_str());
  auto prediction = atoi(d["data"]["prediction"].GetString());

  if (strncmp(mlType_.c_str(), "ewma", mlType_.size()) == 0)
    sender_.setMlEwmaPrediction(prediction);
  else if (strncmp(mlType_.c_str(), "instant", mlType_.size()) == 0)
    sender_.setMlInstantPrediction(prediction);
  else
    std::runtime_error("Unknown ML type sent.");

  // get response telling if misbehavior
  // return response
  if (prediction != 0)
    return true;
  return false;
}



std::string ML::
makeSenderData() {
  // create JSON object from ratings
  auto s = rapidjson::StringBuffer{};
  auto writer = rapidjson::Writer<rapidjson::StringBuffer>(s);

  writer.StartObject();

    writer.Key("mlType");
    writer.String(mlType_.c_str());

    writer.Key("reporterId");
    writer.Uint(reporter_);

    writer.Key("selectedAttack");
    writer.String(pathPrefix_.c_str());

    writer.Key("ratings");
    writer.StartObject();
      writer.Key("ART");
      writer.Double(ratings_.getArt());
      writer.Key("MBF");
      writer.Double(ratings_.getMbf());
      writer.Key("SAW");
      writer.Double(ratings_.getSaw());
      writer.Key("MVP");
      writer.Double(ratings_.getMvp());
      writer.Key("MVN");
      writer.Double(ratings_.getMvn());
      writer.Key("MAP");
      writer.Double(ratings_.getMap());
      writer.Key("MAN");
      writer.Double(ratings_.getMan());
      writer.Key("MDM");
      writer.Double(ratings_.getMdm());
      writer.Key("KALMAN");
      writer.Double(ratings_.getKalman());
      writer.Key("FBR");
      writer.Double(ratings_.getFbr());
      writer.Key("suspicionRating");
      writer.Double(ratings_.getSuspicion());
    writer.EndObject();

    writer.Key("suspectedAttack");
    writer.Bool(sender_.isDiscarded());

    writer.Key("groundTruth");
    writer.StartObject();
      writer.Key("isAttack");
      writer.Bool(sender_.getIsAttack());
      writer.Key("attackType");
      writer.String(sender_.getAttackType().c_str());
    writer.EndObject();

  writer.EndObject();

  return std::string(s.GetString());
}
