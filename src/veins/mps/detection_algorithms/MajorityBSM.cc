//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/detection_algorithms/MajorityBSM.h>

MajorityBSM::
MajorityBSM(Sender& sender)
: sender_(sender)
{}



MajorityBSM::
~MajorityBSM()
{}



bool MajorityBSM::
isMisbehavior() {
  auto n = sender_.getFbrTrace();
  auto ones = 0;
  auto zeroes = 0;

  while (n != 0) {
    auto r = n % 10;
    if (r == 1)
      ones++;
    if (r == 0)
      zeroes++;
    n = n / 10;
  }

  return ones > zeroes;
}
