/*
 * EEBL.h
 *
 *  Created on: Oct 15, 2018
 *      Author: mps
 */

#ifndef SRC_VEINS_MPS_V2X_APPLICATIONS_EEBL_H_
#define SRC_VEINS_MPS_V2X_APPLICATIONS_EEBL_H_

#include <veins/mps/v2x_applications/IV2xApp.h>
#include <veins/base/utils/Coord.h>
#include <veins/modules/messages/BasicSafetyMessage_m.h>

class EEBL : IV2xApp
{
public:
  EEBL(const BasicSafetyMessage& bsm, const Coord& currPos, const Coord& currDirection, const Coord& currSpeed);
  virtual ~EEBL();
  bool warning() override;
private:
  const BasicSafetyMessage& bsm_;
  const Coord& currPos_;
  const Coord& currDirection_;
  const Coord& currSpeed_;
};

#endif /* SRC_VEINS_MPS_V2X_APPLICATIONS_EEBL_H_ */
