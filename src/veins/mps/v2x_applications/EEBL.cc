/*
 * EEBL.cc
 *
 *  Created on: Oct 15, 2018
 *      Author: mps
 */

#include <veins/mps/utility/SupportFunctions.h>
#include <veins/mps/v2x_applications/EEBL.h>

EEBL::
EEBL(const BasicSafetyMessage& bsm, const Coord& currPos, const Coord& currDirection, const Coord& currSpeed)
: bsm_(bsm),
  currPos_(currPos),
  currDirection_(currDirection),
  currSpeed_(currSpeed)
{}



EEBL::
~EEBL()
{}



bool EEBL::
warning() {
  if (!bsm_.getEventHardBraking())
    return false;

  // SAE J2945/1 defines hard braking as a deceleration greater than 0.4g = 3.92 m/s^2
  const double DECELERATION_THRESHOLD = 3.92;
  if (std::sqrt(bsm_.getSenderAcceleration().squareLength()) < DECELERATION_THRESHOLD)
    return false;

  auto support = SupportFunctions();
  if (support.isBehind(currPos_, bsm_.getSenderPos(), currDirection_))
    return false;

  if (currPos_.distance(bsm_.getSenderPos()) > support.getSafetyDistance(currSpeed_))
    return false;

  return true;
}
