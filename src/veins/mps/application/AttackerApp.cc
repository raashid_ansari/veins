//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <thread>
#include <chrono>

#include <veins/mps/application/AttackerApp.h>
#include <veins/mps/utility/SupportFunctions.h>
#include <veins/mps/utility/GhostVehicle.h>
#include <veins/mps/utility/CSVWriter/include/CSVWriter.h>

Define_Module(AttackerApp);



//auto at = std::time(0);
//auto anow = std::localtime(&at);
//char aepoch[100];
//int an = sprintf(aepoch, "%04d/%02d/%02d_%02d:%02d", anow->tm_year + 1900, anow->tm_mon + 1, anow->tm_mday, anow->tm_hour, anow->tm_min);
//const std::string AttackerApp::pathPrefix_ = "results/" + std::string(aepoch) + "/";

AttackerApp::
AttackerApp()
//: writer_(rapidjson::Writer<rapidjson::StringBuffer>(s_))
{
//  writer_.StartObject();
//  writer_.Key("groundTruth");
//  writer_.StartObject();

}


AttackerApp::
~AttackerApp()
{
//  writer_.EndObject();
//  writer_.EndObject();
//
//  auto fileLogger = FileLogger(findHost()->getId(), "groundTruthData");
//  fileLogger.log(s_.GetString());
}



void AttackerApp::
initialize(int stage) {
  BaseWaveApplLayer::initialize(stage);

  if (stage == 0) {
    connManager_ = FindModule<ConnectionManager*>::findGlobalModule();

    ghostVehDistVec_.setName("ghostVehicleDistance");
    ghostVelocityVec_.setName("ghostVehicleVelocity");
    attackDurationVec_.setName("attack");
    constantPositionAttackOffsetFlag_ = true;
    beaconPriority = par("beaconPriority").intValue();

    canvas_ = getParentModule()->getParentModule()->getCanvas();
    trafficManager_ = FindModule<TrafficManager*>::findGlobalModule();
    attackFrom_ = par("attackFrom").intValue();
    attackUntil_ = par("attackUntil").intValue();
    attackerType_ = par("attackerType").intValue();
    dosMessages_ = par("dosMessages").intValue();
  }

  if (stage == 1) {
    auto os = std::stringstream{};
    os << par("type").stringValue() << findHost()->getId();
    findHost()->setName(os.str().c_str());
    visuals_ = Visuals(findHost());

    for (auto i = 0; i < trafficManager_->getGhostCarPerAttacker(); ++i)
      ghostVehDistMap_.insert(std::make_pair(i, connManager_->getInterfDist()));
  }
  auto color = Veins::TraCIColor(255, 0, 0, 0);
  traciVehicle->setColor(color);
}



void AttackerApp::
finish() {
  BaseWaveApplLayer::finish();
}



void AttackerApp::
handleSelfMsg(cMessage* msg) {
  BaseWaveApplLayer::handleSelfMsg(msg);
}



void AttackerApp::
handlePositionUpdate(cObject* obj) {
  BaseWaveApplLayer::handlePositionUpdate(obj);
  curAcceleration_ = mobility->getCurrentAcceleration();
  curDirection_    = mobility->getCurrentDirection();
  curOrientation_  = mobility->getCurrentOrientation();
}



void AttackerApp::
onBSM(BasicSafetyMessage* bsm) {
  visuals_.showBSMBubble(bsm->getSenderAddress());
  // Don't attack ghost vehicles
  if (strncmp(bsm->getWsmData(), "ghost", 5) == 0)
    return;

  for (int i = 0; i < trafficManager_->getGhostCarPerAttacker(); ++i) {
    auto attackBsm = new BasicSafetyMessage();
    populateWSM(attackBsm);
    populateAttackBsm(bsm, attackBsm, i);

    // Select attack according to the ATTACK_TYPE
    switch (attackerType_) {
    case ATTACK_NO:
      break;
    case ATTACK_SUDDEN_APPEARANCE:
      suddenAppearance(*bsm, *attackBsm, i);
      break;
    case ATTACK_CONSTANT_POSITION:
      constantPosition(*bsm, *attackBsm, i);
      break;
    case ATTACK_BRAKE_COMM_RANGE:
      brakeFromCommRange(*bsm, *attackBsm, i);
      break;
    case ATTACK_RANDOM_POSITION:
      randomPosition(*bsm, *attackBsm, i);
      break;
    case ATTACK_DOS:
      dos(*bsm, *attackBsm, i);
      break;
    case ATTACK_FAKE_EEBL:
      fakeEebl(*bsm, *attackBsm, i);
      break;
    default:
      error("No attacker type defined for attackerType_ index: \%s\\", attackerType_);
      break;
    }

    // Transmit attackBSM
    sendDown(attackBsm);

    visuals_.updateRing(connManager_->getInterfDist(), "yellow");
    visuals_.updateGhostVisuals(*bsm, *attackBsm, *canvas_);
  }
}



void AttackerApp::
suddenAppearance(const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i) {
  attackBsm.setAttackType("SuddenAppearance");

  auto support = SupportFunctions();

  // get ghost position
  const auto targetSd = support.getSafetyDistance(bsm.getSenderSpeed()); // get target's safety distance
  const auto ghostVehPos = targetSd - 0.1; // put ghost just within target's safety distance
  const auto posOffset = support.getPosOffset(
      mobility->getCurrentPosition(),
      bsm.getSenderPos(),
      ghostVehPos
      ); // calculate ghost vehicle's position w.r.t. target's safety distance
  attackBsm.setSenderPos(posOffset - Coord(i,i,0));
  attackBsm.setSenderSpeed(Coord()); // creates a speed object with speed = 0
}



void AttackerApp::
constantPosition(const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i) {
  if (constantPositionAttackOffsetFlag_) { // This flag is used to stop code from updating ghost position
    constantPositionAttackOffsetFlag_ = false;
    auto ghostPosOffset = par("ghostPosOffset").doubleValue();

    auto support = SupportFunctions();
    // calculate ghost vehicle's position w.r.t. target's safety distance
    ghostPos_ = support.getPosOffset(mobility->getCurrentPosition(), bsm.getSenderPos(), ghostPosOffset);
  }

  attackBsm.setAttackType("ConstantPosition");
  attackBsm.setSenderPos(ghostPos_ - Coord(i,i,0));
  attackBsm.setSenderSpeed(Coord()); // creates a speed object with speed = 0
}



void AttackerApp::
brakeFromCommRange(const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i) {
  attackBsm.setAttackType("BrakeFromCommRange");

  auto support = SupportFunctions();
  auto posOffset = support.getPosOffset(
      mobility->getCurrentPosition(),
      bsm.getSenderPos(),
      ghostVehDistMap_.find(i)->second
      ); // Get offset for vehicle position
  auto ghostVehPos = posOffset - Coord(i, i, 0);
  attackBsm.setSenderPos(ghostVehPos);
  attackBsm.setSenderSpeed(mobility->getCurrentSpeed());

  // keep ghost vehicle from going behind target vehicle
  if (ghostVehDistMap_.find(i)->second > 5) // TODO: Change this to if ghost distance is less than 5 then throw exception
    ghostVehDistMap_.find(i)->second -= 5;
}



void AttackerApp::
randomPosition(const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i) {
  attackBsm.setAttackType("RandomPosition");

  auto world = FindModule<BaseWorldUtility*>::findGlobalModule();
  ghostPos_ = world->getRandomPosition(); // get ghost vehicle's random position on the playground
  attackBsm.setSenderPos(ghostPos_);
  attackBsm.setSenderSpeed(Coord()); // creates a speed object with speed = 0
}



void AttackerApp::
dos(const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i) {
  attackBsm.setAttackType("DoS");
  std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<int>((beaconInterval.dbl() / dosMessages_) * 1000)));
}



void AttackerApp::
fakeEebl(const BasicSafetyMessage& bsm, BasicSafetyMessage& attackBsm, const int i) {
  attackBsm.setAttackType("FakeEEBL");

  auto support = SupportFunctions();

  // get ghost position
  const auto targetSd = support.getSafetyDistance(bsm.getSenderSpeed()); // get target's safety distance
  const auto ghostVehPos = targetSd - 0.1; // put ghost just within target's safety distance
  const auto posOffset = support.getPosOffset(
      mobility->getCurrentPosition(),
      bsm.getSenderPos(),
      ghostVehPos
      ); // calculate ghost vehicle's position w.r.t. target's safety distance
  attackBsm.setSenderPos(posOffset - Coord(i,i,0));

  // get ghost velocity
  attackBsm.setSenderSpeed(Coord()); // creates a speed object with speed = 0
  attackBsm.setEventHardBraking(true);
  auto x_accel = normal(10, 1);
  auto y_accel = normal(10, 1);
//  auto x_accel = uniform(2, 51);
//  auto y_accel = uniform(2, 51);
//  auto x_accel = 100;
//  auto y_accel = 100;
  attackBsm.setSenderAcceleration(Coord(x_accel,y_accel,0));
}



void AttackerApp::
populateAttackBsm(const BasicSafetyMessage* bsm, BasicSafetyMessage* attackBsm, const int ghostIndex) {
//    spoof_bsm->setSenderAddress(intrand(RAND_MAX)); // random ID being sent in spoofed BSM
  auto id = findHost()->getId() + ghostIndex + 1;
//  auto id = bsm->getSenderAddress() + ghostIndex + 1;
  attackBsm->setSenderAddress(id);
  attackBsm->setWsmData("ghost");
  attackBsm->setIsAttack(true);
}
