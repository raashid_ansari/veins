//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
#include <ctime>
#include <cstdio>

#include <veins/mps/application/GenuineCarApp.h>

// #include "veins/modules/mobility/traci/TraCIColor.h"

#include <veins/mps/detection_algorithms/Ewma.h>
#include <veins/mps/detection_algorithms/SingleThreshold.h>
#include <veins/mps/detection_algorithms/ML.h>
#include <veins/mps/detection_algorithms/MajorityBSM.h>

#include <veins/mps/utility/SupportFunctions.h>
#include <veins/mps/utility/Sender.h>
#include <veins/mps/utility/Latency.h>
#include <veins/mps/utility/ExecutionTimer.h>
#include <veins/mps/utility/Trace.h>

// V2X Applications
#include <veins/mps/v2x_applications/EEBL.h>

// MBD report
#include <veins/mps/mb_reports/BaseMbr.h>

Define_Module(GenuineCarApp);

GenuineCarApp::
GenuineCarApp()
{
  senderTable_ = SenderTable();
}



GenuineCarApp::
~GenuineCarApp()
{}



void GenuineCarApp::
initialize(int stage) {
  BaseWaveApplLayer::initialize(stage);

  if (stage == 0) {
    connManager_ = FindModule<ConnectionManager*>::findGlobalModule();
    canvas_ = getParentModule()->getParentModule()->getCanvas();
//    brakeVar_ = true;

    shouldSenderCleanUp_ = par("shouldSenderCleanUp").boolValue();

    shouldDetect_ = par("detection").boolValue();
    predictor_    = par("predictor").intValue();
    nPredictions_ = par("nPredictions").intValue();
    movingWindow_ = par("movingWindow").intValue();
    suspicionThreshold_ = par("suspicionThreshold").doubleValue();
    distanceThreshold_ = par("distanceThreshold").doubleValue();
    speedThreshold_ = par("speedThreshold").doubleValue();

    doSingleThreshold_ = par("doSingleThreshold").boolValue();
    doEwma_ = par("doEwma").boolValue();
    doMl_ = par("doMl").boolValue();
    doMbsm_ = par("doMbsm").boolValue();
    mpsServerAddress_ = par("mpsServerAddress").stringValue();
    logType_ = par("logType").intValue();

    rectLen_ = par("rectLen").doubleValue();
    rectWidth_ = par("rectWidth").doubleValue();;
    vehLen_ = par("vehLen").doubleValue();
    vehWidth_ = par("vehWidth").doubleValue();
    latSpace_ = par("latSpace").doubleValue();
    lonSpace_ = par("lonSpace").doubleValue();

    accelCi95_ = XY<Range>{Range{0,0}};
    accelCi95_.x.low = par("accelCi95xLow").doubleValue();
    accelCi95_.x.high = par("accelCi95xHigh").doubleValue();
    accelCi95_.y.low = par("accelCi95yLow").doubleValue();
    accelCi95_.y.high = par("accelCi95yHigh").doubleValue();

    accelCi99_ = XY<Range>{Range{0,0}};
    accelCi99_.x.low = par("accelCi99xLow").doubleValue();
    accelCi99_.x.high = par("accelCi99xHigh").doubleValue();
    accelCi99_.y.low = par("accelCi99yLow").doubleValue();
    accelCi99_.y.high = par("accelCi99yHigh").doubleValue();

    confidenceIntervalFile_ = par("confidenceIntervalFile").stringValue();
    getConfidenceIntervals();

    auto art = AcceptanceRangeThreshold();
    auto saw = SuddenAppearanceWarning();
    auto mbf = MaxBeaconingFrequency();
    auto mvp = PlausibleVelocity();
    auto mvn = MaxVelocity();
    auto map = PlausibleAcceleration();
    auto man = MaxAcceleration();
    auto kalman = Kalman();
    auto fbr = FirstBSMRSSI();
    auto psa = PositionSpeedAcceleration();
    auto sa = SpeedAcceleration();
    auto po = PositionOverlap();
    auto mdt = MaxDensityThreshold();
    auto ba = BrakeAcceleration();
    auto lp = LocationPlausiblity();
    auto mp1 = MovementPlausibility();
    auto mp2 = MovementPlausibility();

    art.setFadeFactor(par("artFading").doubleValue());
    art.setGain(par("artGain").intValue());
    saw.setFadeFactor(par("sawFading").doubleValue());
    saw.setGain(par("sawGain").intValue());
    mbf.setFadeFactor(par("mbfFading").doubleValue());
    mbf.setGain(par("mbfGain").intValue());
    mvp.setFadeFactor(par("maVelPosFading").doubleValue());
    mvp.setGain(par("maVelPosGain").intValue());
    mvn.setFadeFactor(par("maVelNegFading").doubleValue());
    mvn.setGain(par("maVelNegGain").intValue());
    map.setFadeFactor(par("maAccelPosFading").doubleValue());
    map.setGain(par("maAccelPosGain").intValue());
    man.setFadeFactor(par("maAccelNegFading").doubleValue());
    man.setGain(par("maAccelNegGain").intValue());
    kalman.setFadeFactor(par("kalmanFading").doubleValue());
    kalman.setGain(par("kalmanGain").intValue());
    fbr.setFadeFactor(par("fbrFading").doubleValue());
    fbr.setGain(par("fbrGain").intValue());
    psa.setFadeFactor(par("psaFading").doubleValue());
    psa.setGain(par("psaGain").intValue());
    sa.setFadeFactor(par("saFading").doubleValue());
    sa.setGain(par("saGain").intValue());
    po.setFadeFactor(par("poFading").doubleValue());
    po.setGain(par("poGain").intValue());
    mdt.setFadeFactor(par("mdtFading").doubleValue());
    mdt.setGain(par("mdtGain").intValue());
    ba.setFadeFactor(par("baFading").doubleValue());
    ba.setGain(par("baGain").intValue());
    lp.setFadeFactor(par("lpFading").doubleValue());
    lp.setGain(par("lpGain").intValue());
    mp1.setFadeFactor(par("mp1Fading").doubleValue());
    mp1.setGain(par("mp1Gain").intValue());
    mp2.setFadeFactor(par("mp2Fading").doubleValue());
    mp2.setGain(par("mp2Gain").intValue());

    checks_ = Checks(art, saw, mbf, mvp, mvn, map, man, fbr, kalman, psa, sa, po, mdt, ba, lp, mp1, mp2);

    auto traceFileStream = std::ostringstream{};
    traceFileStream << logPath_ << "t_" << suspicionThreshold_ << "_r_" << repNum_ << "_trace";
    traceFileName_ = traceFileStream.str();

    auto vehicleDensityFileStream = std::ostringstream{};
    vehicleDensityFileStream << logPath_ << "r_" << repNum_ << "_vehicle_density";
    vehicleDensityFile_ = vehicleDensityFileStream.str();

//    csvWriter.writeToFile(traceFileName_, true);
//
//    auto csv = CSVWriter(",");
//    csv <<
//        "simTime" <<
//        "vehicleDensity" <<
//        "repNum" <<
//        "vehicleType";

//    csv.writeToFile(vehicleDensityFile_, true);
//    }
  }

  if (stage == 1) {
    auto os = std::stringstream{};
    os << par("type").stringValue() << findHost()->getId();
    findHost()->setName(os.str().c_str());

    visuals_ = Visuals(findHost());

    // setup timer to check MDM periodically
    if (shouldDetect_) {
      mdmCheck_       = new cMessage("mdmCheck");
      mdmCheckPeriod_ = par("mdmCheckPeriod").intValue();
      mdmThreshMultiplier_ = par("mdmThreshMultiplier").intValue();

      scheduleAt(simTime() + mdmCheckPeriod_, mdmCheck_);
    }
    auto color = Veins::TraCIColor(0, 255, 0, 0);
    traciVehicle->setColor(color);

    // schedule sender clean up
    if (shouldSenderCleanUp_) {
      senderCleanUp_ = new cMessage("senderCleanUp");
      lookBackTime_ = par("lookBackTime").intValue();
      senderCleanUpPeriod_ = par("senderCleanUpPeriod").doubleValue();
      scheduleAt(simTime() + senderCleanUpPeriod_, senderCleanUp_);
    }

  }
}



void GenuineCarApp::
finish() {
  if (mdmCheck_->isScheduled())
    cancelAndDelete(mdmCheck_);
  else if (mdmCheck_)
    delete(mdmCheck_);

  BaseWaveApplLayer::finish();
}



void GenuineCarApp::
handleSelfMsg(cMessage* msg) {
  if (msg == mdmCheck_) {
    // Perform MDM check for each sender in SenderTable
    if (!senderTable_.getSenders().empty())
      MinimumDistanceMoved(mdmThreshMultiplier_).execute(senderTable_, connManager_->getInterfDist());

    // Schedule MDM check again
    scheduleAt(simTime() + mdmCheckPeriod_, mdmCheck_);
  } else if (msg == senderCleanUp_) {
    cleanUpSenders();
  } else {
      if (!sendBeaconEvt->isScheduled())
        BaseWaveApplLayer::handleSelfMsg(msg);
  }

}



void GenuineCarApp::
handlePositionUpdate(cObject* obj) {
  BaseWaveApplLayer::handlePositionUpdate(obj);
  curAcceleration_ = mobility->getCurrentAcceleration();
  curDirection_    = mobility->getCurrentDirection();
  curOrientation_  = mobility->getCurrentOrientation();

  const int LOGGING_INTERVAL = 5.0;
  if (simTime().dbl() - lastVehicleDensityLogTime_ < LOGGING_INTERVAL)
    return;
  lastVehicleDensityLogTime_ = simTime().dbl();

  auto serverAddress = std::ostringstream{};
  serverAddress << mpsServerAddress_ << "/vehicleDensity";
  auto address = serverAddress.str();
  auto trace = Trace(vehicleDensityFile_, address);
  trace.setSimTime(simTime().dbl());
  trace.setVehicleDensity(mobility->getManager()->getManagedHosts().size());
  trace.setRepNum(repNum_);

  switch(logType_) {
  case LOGTYPE_CSV:
    trace.printVehicleDensityCsv();
    break;
  case LOGTYPE_JSON:
    trace.printVehicleDensityJson();
    break;
  case LOGTYPE_ALL:
    trace.printVehicleDensityCsv();
    trace.printVehicleDensityJson();
    break;
  default:
    std::runtime_error("Invalid log type given!");
  }
}



//inline void
//reactToBSM(const Coord& senderPos) {
//  isDanger(senderPos) ? brake() : speedUp();
//}



void GenuineCarApp::
onBSM(BasicSafetyMessage* bsm) {
  visuals_.showBSMBubble(bsm->getSenderAddress());
  if (!shouldDetect_)
    return;

  if (senderTable_.isNew(bsm->getSenderAddress())) {
    // record first received position from a new sender.
    // this will be used in MDM check
    const int N_STATES   = 4;
    const int N_CONTROLS = 2;
    const int N_MEASURES = 4;

    auto sender = Sender(
        *bsm,
        N_STATES,
        N_CONTROLS,
        N_MEASURES,
        bsm->getSenderAddress(), // Sender ID
        bsm->getSenderPos(), // this is for sender.firstPos
        simTime().dbl(), // timestamp of last beacon received from sender, also used for firstBeaconTime
        movingWindow_ // for standard deviation calculation
        );
    sender.addToPosTrace(sender.getBsm().getSenderPos());

    auto distanceFromSender = curPosition.distance(sender.getBsm().getSenderPos());
    sender.addToDistanceTrace(distanceFromSender);

    // initialize predictor
    sender.initializeTracker(
        par("predictionTimeStep").doubleValue(),
        par("predictor").intValue());

    senderTable_.add(sender);
  }

  auto sender = senderTable_.find(bsm->getSenderAddress());
  sender->setBsm(*bsm);
  sender->setIsFirstBeacon(false);
  sender->setIsAttack(bsm->getIsAttack());
  sender->setAttackType(bsm->getAttackType());
  sender->makePredictions(nPredictions_);
  sender->setRssi         (bsm->getRSSI());
  if (sender->getIsAttack() && sender->getIsFirstAttack()) {
    sender->setEwmaLatency(Latency(curPosition));
    sender->setIsFirstAttack(false);
  }

  // *********************************************
  // VISUALISATION
  // Plot predictions received from Kalman Filter
  // *********************************************
  visuals_.plotPredictions(par("nPredictions").intValue(), sender->getId(), *canvas_, sender->getPredictions());

  executeChecks(sender->getId()); // change brake variable in this function
  sender->calcCi(accelCi95_, accelCi99_);
  executeDetectionAlgorithms(*sender);
  executeV2XApplications(*bsm);
  writeTrace(*sender);


//  isDanger(bsm->getSenderPos()) ? brake() : speedUp();

  // Record communication range throughout the simulation
//  auto communicationRange = connManager_->getInterfDist();
//  commRangeVec_.record(communicationRange);

}



void GenuineCarApp::
executeDetectionAlgorithms(Sender& sender) {
  auto isMisbehavior = false;
  if (doSingleThreshold_) {
    auto singleThreshold = SingleThreshold(suspicionThreshold_, sender);
    {
      ExecutionTimer<> timer;
      isMisbehavior = singleThreshold.isMisbehavior();
      singleThreshold.setExecTime(timer.stop());
    }

    if (isMisbehavior) {
      senderTable_.remove(sender.getId());
      singleThreshold.writeReport(sender, logPath_, findHost()->getId(), "st");
    }
  }

  if (doEwma_) {
    isMisbehavior = false;
    auto ewma = Ewma(suspicionThreshold_, sender, checks_);

    if (!sender.isDiscarded())
      sender.getEwmaLatency().update(curPosition);

    {
      ExecutionTimer<> timer;
      isMisbehavior = ewma.isMisbehavior();
      ewma.setExecTime(timer.stop());
    }

    if (isMisbehavior) {
      senderTable_.remove(sender.getId());
      ewma.writeReport(sender, logPath_, findHost()->getId(), "ewma");
    }
  }

  if (doMl_) {
    isMisbehavior = false;
    auto mlAddressStream = std::ostringstream{};
    mlAddressStream << mpsServerAddress_ << "/ml";
    auto mlAddress = mlAddressStream.str();


    // EWMA ML
    auto ewmaRatings = sender.getEwmaRating();
    auto ewmaML = ML(sender, findHost()->getId(), logPath_, checks_, mlAddress, "ewma", ewmaRatings);
    {
      ExecutionTimer<> timer;
      isMisbehavior = ewmaML.isMisbehavior();
      eMlExecTime_ = timer.stop();
    }
    if (isMisbehavior) {
      senderTable_.remove(sender.getId());
      ewmaML.writeReport(sender, logPath_, findHost()->getId(), "ewmaML");
    }


    // Instantaneous ML
    auto instantRatings = sender.getEwmaRating();
    auto instantML = ML(sender, findHost()->getId(), logPath_, checks_, mlAddress, "instant", instantRatings);
    {
      ExecutionTimer<> timer;
      isMisbehavior = instantML.isMisbehavior();
      iMlExecTime_ = timer.stop();
    }
    if (isMisbehavior) {
      senderTable_.remove(sender.getId());
      instantML.writeReport(sender, logPath_, findHost()->getId(), "instantML");
    }
  }

  if (doMbsm_) {
    isMisbehavior = false;
    auto mBsm = MajorityBSM(sender);

    {
      ExecutionTimer<> timer;
      isMisbehavior = mBsm.isMisbehavior();
      mBsm.setExecTime(timer.stop());
    }

    if (isMisbehavior) {
      senderTable_.remove(sender.getId());
      mBsm.writeReport(sender, logPath_, findHost()->getId(), "mbsm");
    }
  }
}



void GenuineCarApp::
executeChecks(const int senderId) {
  auto sender = senderTable_.find(senderId);

  // *********************************
  // Plausibility Checks
  // *********************************
  auto& art = checks_.getArt();
  auto& saw = checks_.getSaw();
  auto& mbf = checks_.getMbf();
  auto& mvp = checks_.getMvp();
  auto& mvn = checks_.getMvn();
  auto& map = checks_.getMap();
  auto& man = checks_.getMan();
  auto& psa  = checks_.getPsa();
  auto& sa  = checks_.getSa();
  auto& po  = checks_.getPo();
  auto& mdt = checks_.getMdt();
  auto& ba = checks_.getBa();
  auto& lp = checks_.getLp();
  auto& mp1 = checks_.getMp1();
  auto& mp2 = checks_.getMp2();

  // TODO: move execution timer internal to each check
  {
    ExecutionTimer<> timer;
    art.execute(*sender, curPosition, connManager_->getInterfDist());
    art.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    saw.execute(*sender, curPosition, connManager_->getInterfDist(), curDirection_);
    saw.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    mbf.execute(*sender, simTime().dbl(), beaconInterval.dbl());
    mbf.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    mvp.execute(*sender, senderTable_, curSpeed);
    mvp.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    mvn.execute(*sender);
    mvn.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    map.execute(*sender, senderTable_, curAcceleration_);
    map.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    man.execute(*sender);
    man.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    psa.execute(*sender, distanceThreshold_);
    psa.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    sa.execute(*sender, speedThreshold_);
    sa.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    po.execute(*sender, senderTable_);
    po.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    mdt.execute(curPosition, *sender, senderTable_, rectLen_, rectWidth_, vehLen_, vehWidth_, Coord(latSpace_, lonSpace_));
    mdt.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    ba.execute(*sender);
    ba.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    lp.execute(*sender);
    lp.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    mp1.execute(*sender);
    mp1.execTime_ = timer.stop();
  }

  {
    ExecutionTimer<> timer;
    mp2.execute(*sender);
    mp2.execTime_ = timer.stop();
  }

  // update lastBeaconTime here, otherwise maxBeaconingFreqCheck will not work
  if (!senderTable_.isNew(senderId))
    sender->setLastBeaconTime(simTime().dbl());

  // *********************************
  // Mobility Checks
  // *********************************
  auto& kalman = checks_.getKalman();

  {
    ExecutionTimer<> timer;
    kalman.execute(*sender, distanceThreshold_);
    kalman.execTime_ = timer.stop();
  }
//  if (isEstimateAndMeasureTooFar) {
//    // record spoof BSM
//
//    attackDetectedVec_.record(80.0);
//    brakeVar_ = false;
//  }

  // *********************************
  // PHY Checks
  // *********************************
  auto& fbr = checks_.getFbr();

  {
    ExecutionTimer<> timer;
    fbr.execute(*sender, curPosition, confidenceInterval_);
    fbr.execTime_ = timer.stop();
  }

  sender->updatePosTrace();
  sender->updateDistanceTrace(curPosition.distance(sender->getBsm().getSenderPos()));
}



void GenuineCarApp::
writeTrace(Sender& sender) {
  auto support = SupportFunctions();
  auto os = std::ostringstream{};
  os << mpsServerAddress_ << "/data";
  auto dataAddress = os.str();
  auto trace = Trace(traceFileName_, dataAddress);

  // *************************************
  // LOGGING
  // *************************************
  // Record position, distance trace and standard deviation in external file
  trace.setRvId(sender.getId());
  trace.setHvId(findHost()->getId());
  trace.setSimTime(simTime().dbl());
  trace.setKalman(sender.getPredictions()[0][0], sender.getPredictions()[0][1]);
  trace.setPos(sender.getBsm().getSenderPos().x, sender.getBsm().getSenderPos().y);
  trace.setEeblMpsOff(sender.getV2xAppFlags().isEeblBefore());
  trace.setEeblMpsOn(sender.getV2xAppFlags().isEeblAfter());
  trace.setRssi(sender.getRssi());
  trace.setMlInstantPrediction(sender.getMlInstantPrediction());
  trace.setMlEwmaPrediction(sender.getMlEwmaPrediction());
  trace.setWsmData(sender.getBsm().getWsmData());
  trace.setChecks(checks_);
  trace.setMlInstantExecTime(iMlExecTime_);
  trace.setMlEwmaExecTime(eMlExecTime_);
  trace.setSuspectedAttack(sender.isDiscarded());
  trace.setAttackGt(sender.getIsAttack());
  trace.setAttackType(sender.getAttackType());
  trace.setSuspicionThreshold(suspicionThreshold_);
  trace.setRepNum(repNum_);
  trace.setVel(sender.getBsm().getSenderSpeed().x, sender.getBsm().getSenderSpeed().y);
  trace.setDistance(sender.getDistanceTrace()[sender.getDistanceTrace().size() - 1]);

  // get position as SUMO/GPS coordinates instead of Omnet++ coordinates
  auto gpsCoord = traci->getLonLat(sender.getBsm().getSenderPos());
  trace.setGps(gpsCoord.first, gpsCoord.second);

  // update sender's position standard deviation value
  auto senderPositionStandardDeviation = support.getMovingStandardDeviation(sender.getPosTrace());
  trace.setPosSd(senderPositionStandardDeviation.x, senderPositionStandardDeviation.y);

  // update sender's distance standard deviation value
  auto senderDistanceStandardDeviation = support.getMovingStandardDeviation(sender.getDistanceTrace());
  trace.setDistanceSd(senderDistanceStandardDeviation);

  auto& ewma = sender.getEwmaRating();
  trace.setEwma(ewma);

  auto& instant = sender.getInstantRating();
  trace.setInstant(instant);

  // Log latency values
  auto latency = Latency();
  auto NaN = std::nan("0");
  latency.setElapsedSimTime(NaN);
  latency.setElapsedTime(NaN);
  latency.setBsm(NaN);
  latency.setDistance(NaN);

  if (sender.getEwmaLatency().isLogFlag()) {
    sender.getEwmaLatency().setLogFlag(false);
    latency = sender.getEwmaLatency();
  }

  trace.setLatency(latency);

  switch(logType_) {
  case LOGTYPE_CSV:
    trace.printCsv();
    break;
  case LOGTYPE_JSON:
    trace.printJson();
    break;
  case LOGTYPE_ALL:
    trace.printCsv();
    trace.printJson();
    break;
  default:
    std::runtime_error("Invalid log type given");
  }
}



void GenuineCarApp::
speedUp() {
  // FIXME: updateRing() is causing ID of a stored sender to change to 0
  visuals_.updateRing(connManager_->getInterfDist(), "-"); // restore interference distance annotation
  traciVehicle->slowDown(14.0, 3); // Speed up normal vehicle to given speed, in 3 seconds
}



bool GenuineCarApp::
isDanger(const Coord& senderPos) const {
  auto support = SupportFunctions();

  auto safetyDistance = support.getSafetyDistance(curSpeed);

  // FIXME: (FIXED) Distance is always positive, need negative distances or is it displacement?
  // Workaround: Implemented isBehind() function that helps calculating if a vehicle is ahead or behind
  auto distanceFromSender = curPosition.distance(senderPos);
  auto isSenderBehind     = support.isBehind(curPosition, senderPos, curDirection_);
  return distanceFromSender < safetyDistance && !isSenderBehind;
}



void GenuineCarApp::
brake() {
  // ****************************************************************************
  // Initiate random number generator
  // Gaussian Distribution: values near the mean are the most likely,
  // standard deviation affects the dispersion of generated values from the mean
  // ****************************************************************************

//  generator_.seed(rndSeed_());
//  auto dist               = std::normal_distribution<>(0.5, 0.5);

  auto support    = SupportFunctions();
  auto safetyDist = support.getSafetyDistance(curSpeed);
//  auto brakingProbability = par("brakingProbability").doubleValue();

//  if (brakingProbability > 1 || brakingProbability < 0)
//    error("braking probability has to be within 0 and 1, current value is: %d", brakingProbability);

  // if Car is within safety distance, brake with given probability.
//  if (
//      (dist(generator_) <= brakingProbability && // if braking probability is higher than random number
//          brakeVar_ && // if brake variable is true i.e. should the vehicle react
//          brakingProbability != 0.0
//      ) || // if braking probability is 0 then don't go further
//      brakingProbability == 1.0
//     ) { // if braking probability is 1 then always execute following statements

    traciVehicle->setSpeed(0.0); // Stops the vehicle
  //FIXME: updateRing() is causing ID of a stored sender to change to 0
    visuals_.updateRing(safetyDist, "red");
//    lastBrakeAt_ = simTime();
//  }
//  else {
//    visuals_.updateRing(safetyDist, "green");
//  }

//  if (simTime()-lastBrakeAt_ > 50) {
//    speedUp();
//    speedUpVec_.record(60.0); // record why 60?
//  }
//
//  if (!brakeVar_)
//    brakeVar_ = true;
}



void GenuineCarApp::
executeV2XApplications(const BasicSafetyMessage& bsm) {
  auto sender = senderTable_.find(bsm.getSenderAddress());

  auto eeblWarning = EEBL(bsm, mobility->getCurrentPosition(), mobility->getCurrentDirection(), mobility->getCurrentSpeed()).warning();
  sender->getV2xAppFlags().setEeblBefore(eeblWarning);
  sender->getV2xAppFlags().setEeblAfter(eeblWarning && !sender->isDiscarded());
}



void GenuineCarApp::
cleanUpSenders() {
  // get all sender's latest beacon
  // if beacon is sent before lookBackTime, delete that sender from senderTable
}



void GenuineCarApp::
getConfidenceIntervals() {
  std::ifstream fp(confidenceIntervalFile_, std::ios::in);

  if (!fp.is_open()) {
    std::cerr << "ERROR: file couldn't be opened" << std::endl;
    return;
  }

  double low, high;
  for(int row = 0; row < MAX_DISTANCE_INDEX; ++row) {
    fp >> low >> high;
    confidenceInterval_.push_back(Range{low, high});

    // stop loops if nothing to read
    if (!fp) {
      std::cerr << "ERROR: Cannot read file for element at " << row << std::endl;
      return;
    }
  }

  fp.close();
}
