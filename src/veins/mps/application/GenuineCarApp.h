//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GHOSTCARS_GENUINECARAPP_H_
#define __GHOSTCARS_GENUINECARAPP_H_

//#include <algorithm>
//#include <random>

#include <string>

#include <omnetpp.h>
#include <veins/mps/utility/TypeDefs.h>
#include "veins/mps/utility/SenderTable.h"
#include "veins/mps/traffic/TrafficManager.h"
#include "veins/mps/utility/Visuals.h"
#include <veins/mps/utility/Checks.h>

#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/base/connectionManager/ConnectionManager.h"

#include "veins/mps/utility/rapidjson/writer.h"
#include "veins/mps/utility/rapidjson/stringbuffer.h"

/**
 * Application layer for Normal Car
 */

class GenuineCarApp : public BaseWaveApplLayer
{
public:
  GenuineCarApp ();
  ~GenuineCarApp();
  void initialize    (int stage) override;
  void finish        ()          override;

//  virtual int numInitStages() const { return std::max(cSimpleModule::numInitStages(), 4); }

protected:
  ConnectionManager* connManager_;
  cCanvas*           canvas_;

//  simtime_t          lastBrakeAt_;
//  std::random_device rndSeed_;
//  std::mt19937       generator_;
//  bool               brakeVar_;
  cMessage* mdmCheck_;
  int       mdmCheckPeriod_;

  bool      shouldSenderCleanUp_;
  cMessage* senderCleanUp_;
  double    senderCleanUpPeriod_;
//  static int writeTraceHeader_;
//
//  static class InitTraceHeader
//  {
//  public:
//    InitTraceHeader() { writeTraceHeader_ = true; }
//  } _initTraceHeader;

protected:
  void brake                 ();
  void speedUp               ();
  void handleSelfMsg         (cMessage* msg)           override;
  void handlePositionUpdate  (cObject* obj)            override;
  void onBSM                 (BasicSafetyMessage* bsm) override;
  void executeV2XApplications(const BasicSafetyMessage& bsm);
  void executeChecks     (const int senderId);
  void executeDetectionAlgorithms(Sender& sender);
  void writeTrace(Sender& sender);
  bool isDanger              (const Coord& senderPos) const;
  void cleanUpSenders();
  void getConfidenceIntervals();

private:
  int         movingWindow_;
  int         nPredictions_;
  SenderTable senderTable_;
  Coord       curAcceleration_;
  Coord       curDirection_;
  Coord       curOrientation_;
  Visuals     visuals_;
  double      suspicionThreshold_;
  double      distanceThreshold_;
  double      speedThreshold_;
  int         mdmThreshMultiplier_;

  bool        shouldDetect_;
  int         predictor_;

  std::string traceFileName_;
  std::string vehicleDensityFile_;
  std::string confidenceIntervalFile_;
  std::string mpsServerAddress_;
  int logType_;

//  rapidjson::StringBuffer s_;
//  rapidjson::Writer<rapidjson::StringBuffer> writer_;

  bool doSingleThreshold_;
  bool doEwma_;
  bool doMl_;
  bool doMbsm_;

  Checks checks_;
  double eMlExecTime_;
  double iMlExecTime_;

  int lookBackTime_;
  double rectLen_;
  double rectWidth_;
  double vehLen_;
  double vehWidth_;
  double latSpace_;
  double lonSpace_;

  XY<Range> accelCi95_;
  XY<Range> accelCi99_;

  const int MAX_DISTANCE_INDEX = 398;
  std::vector<Range> confidenceInterval_;
};

#endif
