/**
* Kalman filter implementation using Eigen. Based on the following
* introductory paper:
*
*     http://www.cs.unc.edu/~welch/media/pdf/kalman_intro.pdf
*
* @author: Hayk Martirosyan, Raashid Ansari
* @date: 04/06/2018
*/

#ifndef MODULES_PREDICTORS_BASEKALMANFITLER_H_
#define MODULES_PREDICTORS_BASEKALMANFITLER_H_

#include <Eigen/Dense>

class BaseKalmanFilter
{
public:
  /**
   * @brief Create a Kalman filter with the specified matrices.
   *   A - System dynamics matrix
   *   B - Control dynamics matrix
   *   H - Output matrix
   *   Q - Process noise covariance
   *   R - Measurement noise covariance
   *   P - Estimate error covariance
   */
  BaseKalmanFilter ();
  virtual ~BaseKalmanFilter();

  void         setTimeStep(const double deltaT);
  const double getTimeStep();

  virtual void setA();
  virtual void setB();
  virtual void setH();
  virtual void setQ();
  virtual void setR();
  virtual void setP();
  virtual void setW();

  /**
   * @brief Initialize the filter with initial values from
   * implementations using this library
   * @param x0 initial values of state variables
   */
  void setX(Eigen::VectorXd& x0);

  void setI();

  void setInitialized(const bool initialized);
  const bool isInitialized();


  /**
   * @brief set state vector size used in Kalman Filter
   * n = size of state vector
   */
  void setStateVectorSize       (const int stateVectorSize);
  int  getStateVectorSize       ();

  /**
   * @brief set control vector size used in Kalman Filter
   * nu = size of input/control vector
   */
  void setControlVectorSize     (const int controlVectorSize);
  int  getControlVectorSize     ();

  /**
   * @brief set measurement vector size used in Kalman Filter
   * m = size of mesurement vector
   */
  void setMeasurementVectorSize (const int measurementVectorSize);
  int  getMeasurementVectorSize ();

  /**
   * @brief Initialize the filter with initial states as zero.
   */
  void init();

  /**
   * @brief Update the estimated state based on measured values. The
   * time step is assumed to remain constant.
   * @param z measurement input vector
   * @param v measurement noise vector
   */
  void update(const Eigen::VectorXd& z, const Eigen::VectorXd& v);

  /**
  * @brief Predict new states
  * @param u control input
  */
  void predict(const Eigen::VectorXd& u);

  /**
   * @brief Run the Kalman Filter through one predict and update step to get next states
   * @param u control input vector
   * @param z measurement input vector
   * @param v measurement noise vector
   * @author Raashid Ansari
   */
  void step(const Eigen::VectorXd& u, const Eigen::VectorXd& z, const Eigen::VectorXd& v);

  /**
   * @brief Return the current state
   */
  Eigen::VectorXd getState();

protected:
  /**
   * Matrices for computation
   */
  Eigen::MatrixXd A_, B_, H_, Q_, R_, P_, K_;

  /**
   * Error Matrices
   */
  Eigen::VectorXd W_;

  /**
   * Is the filter initialized?
   */
  bool initialized_;

  /**
   * n-size identity
   */
  Eigen::MatrixXd I_;

  /**
   * Estimated states
   */
  Eigen::VectorXd xHat_;

  /** System dimensions
   * n_ = size of state vector
   * nu_ = size of input/control vector
   * m_ = size of measurement vector
   */
  int stateVectorSize_, controlVectorSize_, measurementVectorSize_;

  /**
   * Discrete time step
   */
  double timeStep_;
};

#endif /* MODULES_PREDICTORS_BASEKALMANFILTER_H_ */
