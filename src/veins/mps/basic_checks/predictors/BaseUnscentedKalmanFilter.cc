#include <exception>
#include <veins/mps/basic_checks/predictors/BaseUnscentedKalmanFilter.h>

BaseUnscentedKalmanFilter::
BaseUnscentedKalmanFilter()
{
  setInitialized(false);
}



BaseUnscentedKalmanFilter::
~BaseUnscentedKalmanFilter() {}



void BaseUnscentedKalmanFilter::
setSigPred() {
  xSigPred_ = Eigen::MatrixXd(getStateVectorSize(), 2 * getStateVectorSize());
}



void BaseUnscentedKalmanFilter::
setWeights() {
  weights_ = Eigen::VectorXd(2 * getStateVectorSize() + 1);
}



// @author: Cong Chen
void BaseUnscentedKalmanFilter::
update(const Eigen::VectorXd& z, const Eigen::VectorXd& v) {
	auto Zsig = Eigen::MatrixXd(getStateVectorSize(), 2 * getStateVectorSize() + 1);
	for (int i = 0; i < 2 * getStateVectorSize() + 1; i++)
		Zsig.col(i) = H_ * xSigPred_.col(i);
	
	  //mean predicted measurement
    auto z_pred = Eigen::VectorXd(getStateVectorSize());
    z_pred.fill(0.0);

    for (int i = 0; i < 2 * getStateVectorSize() + 1; i++)
      z_pred = z_pred + weights_(i) * Zsig.col(i);

	  //measurement covariance matrix S
	auto S = Eigen::MatrixXd(getStateVectorSize(), getStateVectorSize());
	S.fill(0.0);
	for (int i = 0; i < 2 * getStateVectorSize() + 1; i++) {  //2n+1 simga points
		//residual
		auto z_diff = Zsig.col(i) - z_pred;

		S = S + weights_(i) * z_diff * z_diff.transpose();
	}
	  
	S = S + R_;
	
  //create matrix for cross correlation Tc
  auto Tc = Eigen::MatrixXd(getStateVectorSize(), getStateVectorSize());

  /*****************************************************************************
  *  UKF  Measurement Update
  ****************************************************************************/
  //calculate cross correlation matrix
  Tc.fill(0.0);
  for (int i = 0; i < 2 * getStateVectorSize() + 1; i++) {  //2n+1 simga points
		//residual
		auto z_diff = Zsig.col(i) - z_pred;

		// state difference
		auto x_diff = xSigPred_.col(i) - xHat_;

		Tc = Tc + weights_(i) * x_diff * z_diff.transpose();
  }

	//Kalman gain K;
	auto K = Tc * S.inverse();

	//residual
	auto z_diff = z - z_pred;

	//update state mean and covariance matrix
	xHat_ = xHat_ + K * z_diff;
	P_ = P_ - K*S*K.transpose();
}



/*
 * @brief Predict next state of the system
 * @author Cong Chen
 */
void BaseUnscentedKalmanFilter::
predict(const Eigen::VectorXd& u) {
  if(!initialized_)
    throw std::runtime_error("Filter is not initialized!");
	
	/*  Generate Sigma Points
	****************************************************************************/
	//create sigma point matrix
	auto Xsig = Eigen::MatrixXd(getStateVectorSize(), 2 * getStateVectorSize() + 1);
	
	//calculate square root of P
	auto rP = static_cast<Eigen::MatrixXd>(P_.llt().matrixL());
	
	//set lambda for non-augmented sigma points
	auto lambda = 3 - getStateVectorSize();
	
	//set first column of sigma point matrix
	Xsig.col(0) = xHat_;
	
	//set remaining sigma points
	for (int i = 0; i < getStateVectorSize(); i++) {
		Xsig.col(i + 1) = xHat_ + sqrt(lambda + getStateVectorSize()) * rP.col(i);
		Xsig.col(i + 1 + getStateVectorSize()) = xHat_ - sqrt(lambda + getStateVectorSize()) * rP.col(i);
	}

	for(int i = 0; i < 2 * getStateVectorSize() + 1; i++)
		xSigPred_.col(i) = A_ * Xsig.col(i) + B_ * u + W_;
	
	/*****************************************************************************
	*  Convert Predicted Sigma Points to Mean/Covariance
	****************************************************************************/
	
	// set weights
	auto weight_0 = lambda / (lambda + getStateVectorSize());
	weights_(0) = weight_0;

	for (int i = 1; i < 2 * getStateVectorSize() + 1; i++) {  //2n+1 weights
		double weight = 0.5 / (getStateVectorSize() + lambda);
		weights_(i) = weight;
	}
	
	//predicted state mean
	xHat_.fill(0.0);             //******* necessary? *********
	for (int i = 0; i < 2 * getStateVectorSize() + 1; i++) {  //iterate over sigma points
		xHat_ = xHat_ + weights_(i) * xSigPred_.col(i);
	}
	
	//predicted state covariance matrix
	P_.fill(0.0);             //******* necessary? *********
	for (int i = 0; i < 2 * getStateVectorSize() + 1; i++) {  //iterate over sigma points
		
		// state difference
		auto x_diff = xSigPred_.col(i) - xHat_;

		
		P_ = P_ + weights_(i) * x_diff * x_diff.transpose();
	}

    P_ = P_ + Q_;
}
