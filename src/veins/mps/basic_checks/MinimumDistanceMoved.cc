//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/MinimumDistanceMoved.h>

MinimumDistanceMoved::MinimumDistanceMoved(const int multiplier)
: multiplier_(multiplier)
{}

MinimumDistanceMoved::~MinimumDistanceMoved()
{}

void MinimumDistanceMoved::
execute(SenderTable& senderTable, const double commRange) {

  const auto D_MDM = multiplier_ * commRange;

  for (auto& sender : senderTable.getSenders()) {
    auto distance = sender.getBsm().getSenderPos().distance(sender.getFirstPos());

    if (distance < D_MDM) {
#ifdef DEBUG
      std::cout << "MDM failed for " << sender.getId() << std::endl;
#endif
      sender.getInstantRating().setMdm(1);
    }
  }
}
