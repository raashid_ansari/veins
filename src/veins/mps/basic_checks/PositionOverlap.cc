//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/PositionOverlap.h>
#include <veins/mps/utility/SupportFunctions.h>

PositionOverlap::PositionOverlap()
{
  // TODO Auto-generated constructor stub

}

PositionOverlap::~PositionOverlap()
{
  // TODO Auto-generated destructor stub
}

void PositionOverlap::
execute(Sender& sender, SenderTable& senderTable) {
  auto support = SupportFunctions();
  // calculate vertices of rectangle of a car using length and width from BSM
  auto hvPos = sender.getBsm().getSenderPos();
  auto hvLength = sender.getBsm().getLength();
  auto hvWidth = sender.getBsm().getWidth();
  auto hvVerts = support.getVertices(hvPos, hvLength, hvWidth);

  // compare if sender overlaps any other sender
  for (auto& rv : senderTable.getSenders()) {
    // don't compare sender with itself
    if (rv.getId() == sender.getId())
      continue;

    auto rvPos = rv.getBsm().getSenderPos();
    auto rvLength = rv.getBsm().getLength();
    auto rvWidth = rv.getBsm().getWidth();
    auto rvVerts = support.getVertices(rvPos, rvLength, rvWidth);

    if (isOverlapping(hvVerts.d, hvVerts.b, rvVerts.d, rvVerts.b)) {
      sender.getInstantRating().setPo(1);
      break;
    }
  }
}

bool PositionOverlap::
isOverlapping(const Coord& l1, const Coord& r1, const Coord& l2, const Coord& r2) const {
  // If one rectangle is on left side of other
  if (l1.x > r2.x || l2.x > r1.x)
      return false;

  // If one rectangle is above other
  if (l1.y < r2.y || l2.y < r1.y)
      return false;

  return true;
}
