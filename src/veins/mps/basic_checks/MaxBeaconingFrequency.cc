//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/MaxBeaconingFrequency.h>

MaxBeaconingFrequency::MaxBeaconingFrequency() {}

MaxBeaconingFrequency::~MaxBeaconingFrequency()
{}

void MaxBeaconingFrequency::
execute(Sender& sender, const double currentBeaconTime, const double maxBeaconingThreshold) {
  if (sender.getLastBeaconTime() - currentBeaconTime < maxBeaconingThreshold &&
      !sender.isFirstBeacon())
    return;

  sender.getInstantRating().setMbf(1);
//  updateSenderRating(MBF, -1);
}
