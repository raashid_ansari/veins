//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/LocationPlausiblity.h>
#include <veins/mps/utility/TypeDefs.h>

LocationPlausiblity::LocationPlausiblity()
{
  // TODO Auto-generated constructor stub

}

LocationPlausiblity::~LocationPlausiblity()
{
  // TODO Auto-generated destructor stub
}

void LocationPlausiblity::
execute(Sender& sender) {
  // if this is first beacon then do not process further
  if (sender.isFirstBeacon())
    return;

  auto score = XY<int>{};
  auto range95 = sender.getRange95();
  auto range99 = sender.getRange99();
  auto pos = sender.getBsm().getSenderPos();

  // check if new position is out of 95 CI
  if (range95.x.low >= pos.x || range95.x.high <= pos.x) {
    score.x = 1;
  }

  if (range95.y.low >= pos.y || range95.y.high <= pos.y) {
    score.y = 1;
  }

  // check if new position is out of 99 CI
  if (range99.x.low >= pos.x || range99.x.high <= pos.x) {
    score.x = 2;
  }

  if (range99.y.low >= pos.y || range99.y.high <= pos.y) {
    score.y = 2;
  }

  sender.getInstantRating().setLp(score.x + score.y > 0 ? 1 : 0);
}
