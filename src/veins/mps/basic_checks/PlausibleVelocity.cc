//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/PlausibleVelocity.h>

PlausibleVelocity::PlausibleVelocity()
{}

PlausibleVelocity::~PlausibleVelocity()
{}

void PlausibleVelocity::
execute(Sender& sender, SenderTable& senderTable, const Coord& selfSpeed) {
  // Calculate the distribution of velocities
  auto sumOfSpeeds = selfSpeed.length();
  for (auto& sender : senderTable.getSenders())
    sumOfSpeeds += sender.getBsm().getSenderSpeed().length();

  const auto N_SENDERS  = senderTable.getSenders().size();
  const auto MEAN_SPEED = sumOfSpeeds / N_SENDERS;

  auto variance  = 0.0;
  for (auto& sender : senderTable.getSenders()) {
    auto senderSpeed = sender.getBsm().getSenderSpeed().length();
    auto tmp = senderSpeed - MEAN_SPEED;
    variance += tmp * tmp;
  }
  variance /= N_SENDERS;

  const auto SAMPLE_SIZE                     = 1000.0;
  const auto CONFIDENCE_INTERVAL_95_CONSTANT = 1.96;

  // Multiply by 1.96 for 95% confidence interval
  // marginOfError = 1.96 * standardDeviation / squareRootOfSampleSize
  const auto MARGIN_OF_ERROR    = CONFIDENCE_INTERVAL_95_CONSTANT *
                                  std::sqrt(variance) / std::sqrt(SAMPLE_SIZE);

  auto senderSpeed              = sender.getBsm().getSenderSpeed().length();

  // Check if sender speed is less/greater than 95% confidence interval
  if (senderSpeed > MEAN_SPEED + MARGIN_OF_ERROR ||
      senderSpeed < MEAN_SPEED - MARGIN_OF_ERROR)
    sender.getInstantRating().setMvp(1);
//    updateSenderRating(MA_VEL_POS, -1);
//  else
//    sender_.setMaVelPosInstantRating(1);
//    updateSenderRating(MA_VEL_POS, 1);
}
