//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/SpeedAcceleration.h>

SpeedAcceleration::SpeedAcceleration()
{
  // TODO Auto-generated constructor stub

}

SpeedAcceleration::~SpeedAcceleration()
{
  // TODO Auto-generated destructor stub
}

void SpeedAcceleration::
execute(Sender& sender, double threshold) {
  auto u = sender.getBsm().getSenderSpeed();
  if (!sender.isFirstBeacon()) {
    // Part 1:
    // get previous estimate of next speed
    auto nextSpd = sender.getNextSpdEstimate();
    // compare distance between current and estimated speeds against a threshold
    // output pass/fail value
    sender.getInstantRating().setSa(nextSpd.length() - u.length() > threshold);
  }

  // Part 2:
  // get current acceleration
  auto a = sender.getBsm().getSenderAcceleration();
  // TODO: maybe "dt" should be the difference between current time
  // and estimated next beacon time
  auto dt = simTime().dbl() - sender.getLastBeaconTime();
  // calculate next speed estimate
  // v = u + a*t
  auto v = u + a*dt;
  // save into Sender
  sender.setNextSpdEstimate(v);
}

