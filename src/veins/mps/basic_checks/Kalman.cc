//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/Kalman.h>

Kalman::Kalman()
{}

Kalman::~Kalman()
{}

void Kalman::
execute(Sender& sender, const double distanceThreshold) {
  auto predictions = sender.getPredictions();
  auto distances   = DoubleVector_t{};

  if (predictions.size() <= 0)
    throw std::runtime_error("No predictions found!");

  for (auto& prediction : predictions) {
    auto estimatedPosition = Coord(prediction[0], prediction[1], 0.0);
    auto estimatedVsMeasuredPosition = estimatedPosition.distance(sender.getBsm().getSenderPos());
    distances.push_back(estimatedVsMeasuredPosition);
  }

  if (distances.size() <= 0)
    throw std::runtime_error("No distances found!");

  auto maxDistance = *std::max_element(distances.begin(), distances.end());
  if (maxDistance > distanceThreshold) {
    sender.getInstantRating().setKalman(1);
    //  else if (sender_position.distance(sender_last_pos)-sender_last_vel*(simTime()-last_bsm_recv_time)>distance_threshold
    //      && sender_last_pos != Coord()
    //      && sender_last_vel != Coord())
    //    return true;
  }
}
