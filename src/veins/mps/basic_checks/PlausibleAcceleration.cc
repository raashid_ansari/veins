//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/PlausibleAcceleration.h>

PlausibleAcceleration::PlausibleAcceleration() {}

PlausibleAcceleration::~PlausibleAcceleration()
{}

void PlausibleAcceleration::execute(Sender& sender, SenderTable& senderTable, const Coord& selfAcceleration) {
  // Calculate the distribution of accelerations
  auto sumOfAccelerations = selfAcceleration.length();

  for (auto& s : senderTable.getSenders())
    sumOfAccelerations += s.getBsm().getSenderAcceleration().length();

  const auto N_SENDERS         = senderTable.getSenders().size();
  const auto MEAN_ACCELERATION = sumOfAccelerations / N_SENDERS;

  auto variance  = 0.0;
  for (auto& s : senderTable.getSenders()) {
    auto senderAcceleration = s.getBsm().getSenderAcceleration().length();
    auto tmp = senderAcceleration - MEAN_ACCELERATION;
    variance += tmp * tmp;
  }
  variance /= N_SENDERS;

  const auto SAMPLE_SIZE                     = 1000.0;
  const auto CONFIDENCE_INTERVAL_95_CONSTANT = 1.96;

  // Multiply by 1.96 for 95% confidence interval
  // marginOfError = 1.96 * standardDeviation / squareRootOfSampleSize
  const auto MARGIN_OF_ERROR = CONFIDENCE_INTERVAL_95_CONSTANT *
                               std::sqrt(variance) / std::sqrt(SAMPLE_SIZE);

  // get this sender's acceleration for comparison with the distribution
  auto senderAcceleration    = sender.getBsm().getSenderAcceleration().length();

  if (senderAcceleration > MEAN_ACCELERATION + MARGIN_OF_ERROR ||
      senderAcceleration < MEAN_ACCELERATION - MARGIN_OF_ERROR) {
    sender.getInstantRating().setMap(1);
//    updateSenderRating(MA_ACCEL_POS, -1);
//  } else {
//    sender_.setMaAccelPosInstantRating(1);
//    updateSenderRating(MA_ACCEL_POS, 1);
  }
}
