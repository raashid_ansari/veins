//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/basic_checks/MaxVelocity.h>

MaxVelocity::MaxVelocity() {}

MaxVelocity::~MaxVelocity()
{}

void MaxVelocity::
execute(Sender& sender) {
  // Maximum possible speed is being calculated by taking
  // 95% minimum confidence interval value from the values
  // obtained from this link:
  // https://en.wikipedia.org/wiki/Production_car_speed_record#Record-breaking_production_vehicles
  const auto MAX_POSSIBLE_SPEED = 260.62; //kmph

  auto senderSpeed = sender.getBsm().getSenderSpeed().length();

  if (senderSpeed > MAX_POSSIBLE_SPEED) {
    sender.getInstantRating().setMan(1);
//    updateSenderRating(MA_VEL_NEG, -1);
  } else {
    sender.getInstantRating().setMan(0);
//    updateSenderRating(MA_VEL_NEG, 0);
  }
}
