//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/mb_reports/BaseMbr.h>

#include "veins/mps/utility/rapidjson/prettywriter.h"
#include "veins/mps/utility/rapidjson/stringbuffer.h"
#include "veins/mps/utility/FileLogger.h"

BaseMbr::BaseMbr()
{}

BaseMbr::~BaseMbr()
{}

void BaseMbr::
writeReport(Sender& sender, std::string& pathPrefix, int reporter, std::string reportType) {
  auto s = rapidjson::StringBuffer{};
  auto writer = rapidjson::PrettyWriter<rapidjson::StringBuffer>(s);

  writer.StartObject();

    auto id  = std::ostringstream{};
    id << sender.getId();
    writer.Key(id.str().c_str());

    writer.StartObject();

      writer.Key("ART");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getArt());
        writer.Double(sender.getEwmaRating().getArt());
      writer.EndArray();

      writer.Key("SAW");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getSaw());
        writer.Double(sender.getEwmaRating().getSaw());
      writer.EndArray();

      writer.Key("MDM");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getMdm());
        writer.Double(sender.getEwmaRating().getMdm());
      writer.EndArray();

      writer.Key("MBF");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getMbf());
        writer.Double(sender.getEwmaRating().getMbf());
      writer.EndArray();

      writer.Key("KLMN");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getKalman());
        writer.Double(sender.getEwmaRating().getKalman());
      writer.EndArray();

      writer.Key("MVP");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getMvp());
        writer.Double(sender.getEwmaRating().getMvp());
      writer.EndArray();

      writer.Key("MVN");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getMvn());
        writer.Double(sender.getEwmaRating().getMvn());
      writer.EndArray();

      writer.Key("MAP");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getMap());
        writer.Double(sender.getEwmaRating().getMap());
      writer.EndArray();

      writer.Key("MAN");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getMan());
        writer.Double(sender.getEwmaRating().getMan());
      writer.EndArray();

      writer.Key("FBR");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getFbr());
        writer.Double(sender.getEwmaRating().getFbr());
      writer.EndArray();

      writer.Key("PSA");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getPsa());
        writer.Double(sender.getEwmaRating().getPsa());
      writer.EndArray();

      writer.Key("SA");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getSa());
        writer.Double(sender.getEwmaRating().getSa());
      writer.EndArray();

      writer.Key("PO");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getPo());
        writer.Double(sender.getEwmaRating().getPo());
      writer.EndArray();

      writer.Key("MDT");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getMdt());
        writer.Double(sender.getEwmaRating().getMdt());
      writer.EndArray();

      writer.Key("BA");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getBa());
        writer.Double(sender.getEwmaRating().getBa());
      writer.EndArray();

      writer.Key("LP");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getLp());
        writer.Double(sender.getEwmaRating().getLp());
      writer.EndArray();

      writer.Key("MP1");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getMp1());
        writer.Double(sender.getEwmaRating().getMp1());
      writer.EndArray();

      writer.Key("MP2");
      writer.StartArray();
        writer.Double(sender.getInstantRating().getMp2());
        writer.Double(sender.getEwmaRating().getMp2());
      writer.EndArray();

      writer.Key("SUSP");
      writer.Double(sender.getEwmaRating().getSuspicion());

    writer.EndObject();

  writer.EndObject();

  auto currTime = std::chrono::system_clock::now();
  auto timeSinceEpoch = std::chrono::duration_cast<std::chrono::nanoseconds>(currTime.time_since_epoch()).count();
  auto os = std::ostringstream{};
  os << sender.getId() << "_" << reporter << "_" << timeSinceEpoch << "_" << reportType << "_mbr.json";

  auto fileLogger = FileLogger(pathPrefix, os.str());
  fileLogger.log(s.GetString());
}
