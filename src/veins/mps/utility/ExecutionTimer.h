//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// Code inspired from
// https://stackoverflow.com/a/49090786/

#ifndef SRC_VEINS_MPS_UTILITY_EXECUTIONTIMER_H_
#define SRC_VEINS_MPS_UTILITY_EXECUTIONTIMER_H_

#include <chrono>
#include <type_traits>
#include <sstream>
#include <iostream>

template <class Resolution = std::chrono::nanoseconds>
class ExecutionTimer {
public:
    std::conditional<std::chrono::high_resolution_clock::is_steady,
        std::chrono::high_resolution_clock,
        std::chrono::steady_clock>::type Clock;

private:
    const Clock::time_point mStart = Clock::now();

public:
    ExecutionTimer() = default;
    ~ExecutionTimer() {
        const auto end = Clock::now();
        std::ostringstream strStream;
        strStream << "Destructor Elapsed: "
            << std::chrono::duration_cast<Resolution>(end - mStart).count()
            << std::endl;
//        std::cout << strStream.str() << std::endl;
    }

    inline double stop() {
        const auto end = Clock::now();
        return std::chrono::duration_cast<Resolution>(end - mStart).count();
//        std::ostringstream strStream;
//        strStream << "Stop Elapsed: "
//            << std::chrono::duration_cast<Resolution>(end - mStart).count()
//            << std::endl;
//        std::cout << strStream.str() << std::endl;
    }
};

#endif /* SRC_VEINS_MPS_UTILITY_EXECUTIONTIMER_H_ */
