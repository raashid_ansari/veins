//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <veins/mps/utility/Checks.h>

Checks::Checks() {}

Checks::Checks(
    AcceptanceRangeThreshold art,
    SuddenAppearanceWarning saw,
    MaxBeaconingFrequency mbf,
    PlausibleVelocity mvp,
    MaxVelocity mvn,
    PlausibleAcceleration map,
    MaxAcceleration man_,
    FirstBSMRSSI fbr,
    Kalman kalman,
    PositionSpeedAcceleration psa,
    SpeedAcceleration sa,
    PositionOverlap po,
    MaxDensityThreshold mdt,
    BrakeAcceleration ba,
    LocationPlausiblity lp,
    MovementPlausibility mp1,
    MovementPlausibility mp2
    )
: art_(art),
  saw_(saw),
  mbf_(mbf),
  mvp_(mvp),
  mvn_(mvn),
  map_(map),
  man_(man_),
  kalman_(kalman),
  fbr_(fbr),
  psa_(psa),
  sa_(sa),
  po_(po),
  mdt_(mdt),
  ba_(ba),
  lp_(lp),
  mp1_(mp1),
  mp2_(mp2)
{}

Checks::~Checks()
{
}

Checks& Checks::operator=(const Checks& other) {
  art_ = other.art_;
  saw_ = other.saw_;
  mbf_ = other.mbf_;
  mvp_ = other.mvp_;
  mvn_ = other.mvn_;
  map_ = other.map_;
  man_ = other.man_;
  fbr_ = other.fbr_;
  kalman_ = other.kalman_;
  psa_ = other.psa_;
  sa_ = other.sa_;
  po_ = other.po_;
  mdt_ = other.mdt_;
  ba_ = other.ba_;
  lp_ = other.lp_;
  mp1_ = other.mp1_;
  mp2_ = other.mp2_;
  return *this;
}

AcceptanceRangeThreshold& Checks::getArt()
{
  return art_;
}

Kalman& Checks::getKalman()
{
  return kalman_;
}

MaxAcceleration& Checks::getMan()
{
  return man_;
}

PlausibleAcceleration& Checks::getMap()
{
  return map_;
}

MaxVelocity& Checks::getMvn()
{
  return mvn_;
}

PlausibleVelocity& Checks::getMvp()
{
  return mvp_;
}

MaxBeaconingFrequency& Checks::getMbf()
{
  return mbf_;
}

SuddenAppearanceWarning& Checks::getSaw()
{
  return saw_;
}

void Checks::setArt(const AcceptanceRangeThreshold& art)
{
  art_ = art;
}

void Checks::setKalman(const Kalman& kalman)
{
  kalman_ = kalman;
}

void Checks::setMan(const MaxAcceleration& man)
{
  man_ = man_;
}

void Checks::setMap(const PlausibleAcceleration& map)
{
  map_ = map;
}

void Checks::setMvn(const MaxVelocity& mvn)
{
  mvn_ = mvn;
}

void Checks::setMvp(const PlausibleVelocity& mvp)
{
  mvp_ = mvp;
}

void Checks::setMbf(const MaxBeaconingFrequency& mbf)
{
  mbf_ = mbf;
}

void Checks::setSaw(const SuddenAppearanceWarning& saw)
{
  saw_ = saw;
}

void Checks::setFbr(const FirstBSMRSSI& fbr) {
    fbr_ = fbr;
}

FirstBSMRSSI& Checks::getFbr() {
    return fbr_;
}

void Checks::setPsa(const PositionSpeedAcceleration& psa) {
    psa_ = psa;
}

PositionSpeedAcceleration& Checks::getPsa() {
    return psa_;
}

void Checks::setSa(const SpeedAcceleration& sa) {
  sa_ = sa;
}

SpeedAcceleration& Checks::getSa() {
  return sa_;
}

PositionOverlap& Checks::getPo()
{
  return po_;
}

void Checks::setPo(const PositionOverlap& po)
{
  po_ = po;
}

MaxDensityThreshold& Checks::getMdt()
{
  return mdt_;
}

void Checks::setMdt(const MaxDensityThreshold& mdt)
{
  mdt_ = mdt;
}

BrakeAcceleration& Checks::getBa()
{
  return ba_;
}

void Checks::setBa(const BrakeAcceleration& ba)
{
  ba_ = ba;
}

LocationPlausiblity& Checks::getLp()
{
  return lp_;
}

void Checks::setLp(const LocationPlausiblity& lp)
{
  lp_ = lp;
}

MovementPlausibility& Checks::getMp1()
{
  return mp1_;
}

void Checks::setMp1(const MovementPlausibility& mp1)
{
  mp1_ = mp1;
}

MovementPlausibility& Checks::getMp2()
{
  return mp2_;
}

void Checks::setMp2(const MovementPlausibility& mp2)
{
  mp2_ = mp2;
}
