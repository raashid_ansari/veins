/*
 * Vehicle.cc
 *
 *  Created on: Jun 29, 2018
 *      Author: veins
 */

#include "veins/mps/utility/Vehicle.h"

Vehicle::
Vehicle() {}



Vehicle::
~Vehicle() {}



const std::string& Vehicle::
getId() const {
    return id_;
}



void Vehicle::
setId(const std::string& id) {
    id_ = id;
}



const int8_t Vehicle::
getLane() const {
    return lane_;
}



void Vehicle::
setLane (int8_t lane) {
    lane_ = lane;
}



const double Vehicle::
getPosition() const {
    return position_;
}



void Vehicle::
setPosition(double position) {
    position_ = position;
}



const double Vehicle::getSpeed() const {
    return speed_;
}



void Vehicle::
setSpeed(double speed) {
    speed_ = speed;
}



const std::string& Vehicle::getType() const {
    return type_;
}



void Vehicle::setType(const std::string& type) {
    type_ = type;
}
