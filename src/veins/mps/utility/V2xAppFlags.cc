/*
 * V2xAppFlags.cc
 *
 *  Created on: Oct 16, 2018
 *      Author: mps
 */

#include <veins/mps/utility/V2xAppFlags.h>

V2xAppFlags::V2xAppFlags()
: eeblBefore_(false),
  dnpwBefore_(false),
  fcwBefore_(false),
  imaBefore_(false),
  eeblAfter_(false),
  dnpwAfter_(false),
  fcwAfter_(false),
  imaAfter_(false)
{}

V2xAppFlags::~V2xAppFlags()
{
}

bool V2xAppFlags::isDnpwAfter() const
{
  return dnpwAfter_;
}

void V2xAppFlags::setDnpwAfter(bool dnpwAfter)
{
  dnpwAfter_ = dnpwAfter;
}

bool V2xAppFlags::isDnpwBefore() const
{
  return dnpwBefore_;
}

void V2xAppFlags::setDnpwBefore(bool dnpwBefore)
{
  dnpwBefore_ = dnpwBefore;
}

bool V2xAppFlags::isEeblAfter() const
{
  return eeblAfter_;
}

void V2xAppFlags::setEeblAfter(bool eeblAfter)
{
  eeblAfter_ = eeblAfter;
}

bool V2xAppFlags::isEeblBefore() const
{
  return eeblBefore_;
}

void V2xAppFlags::setEeblBefore(bool eeblBefore)
{
  eeblBefore_ = eeblBefore;
}

bool V2xAppFlags::isFcwAfter() const
{
  return fcwAfter_;
}

void V2xAppFlags::setFcwAfter(bool fcwAfter)
{
  fcwAfter_ = fcwAfter;
}

bool V2xAppFlags::isFcwBefore() const
{
  return fcwBefore_;
}

void V2xAppFlags::setFcwBefore(bool fcwBefore)
{
  fcwBefore_ = fcwBefore;
}

bool V2xAppFlags::isImaAfter() const
{
  return imaAfter_;
}

void V2xAppFlags::setImaAfter(bool imaAfter)
{
  imaAfter_ = imaAfter;
}

bool V2xAppFlags::isImaBefore() const
{
  return imaBefore_;
}

void V2xAppFlags::setImaBefore(bool imaBefore)
{
  imaBefore_ = imaBefore;
}
