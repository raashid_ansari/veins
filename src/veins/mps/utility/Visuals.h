/*
 * Visuals.h
 *
 *  Created on: Aug 24, 2017
 *      Author: veins
 */

#ifndef MODULES_UTILITY_VISUALS_H_
#define MODULES_UTILITY_VISUALS_H_

#include <Eigen/Dense>

#include <veins/modules/messages/BasicSafetyMessage_m.h>

class Visuals {
public:
  Visuals ();
  explicit Visuals       (const cModule* car);
  virtual ~Visuals       ();
  void showBSMBubble     (const int senderId) const;
  void updateRing        (const int radius, const std::string& color) const;
  void updateGhostVisuals(const BasicSafetyMessage& bsm, const BasicSafetyMessage& spoofedBsm, cCanvas& canvas);
  void plotPredictions   (const int numPreds, const int senderId, cCanvas& canvas, const std::vector<Eigen::VectorXd>& predictions);

protected:
  bool shouldVisualize() const;

private:
    const cModule* car_;
};

#endif /* MODULES_UTILITY_VISUALS_H_ */
