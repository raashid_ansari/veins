/*
 * Latency.h
 *
 *  Created on: Nov 3, 2018
 *      Author: mps
 */

#ifndef SRC_VEINS_MPS_MODULES_UTILITY_LATENCY_H_
#define SRC_VEINS_MPS_MODULES_UTILITY_LATENCY_H_

#include <veins/base/utils/Coord.h>
#include <veins/mps/utility/TypeDefs.h>

class Latency
{
public:
  Latency();
  explicit Latency(Coord initPos);
  virtual ~Latency();
  void update(Coord curPos);
  double getDistance() const;
  double getElapsedTime() const;
  double getBsm() const;
  TimePoint getInitTime() const;
  bool isLogFlag() const;
  void setLogFlag(bool logFlag);
  double getElapsedSimTime() const;
  void setElapsedSimTime(double elapsedSimTime);
  void setDistance(double distance);
  void setElapsedTime(double elapsedTime);
  void setBsm(double bsm);

private:
  TimePoint initTime_;
  double initSimTime_;
  Coord  lastPos_;
  double elapsedTime_;
  double elapsedSimTime_;
  double    nBsm_;
  double distance_;
  bool logFlag_;
};

#endif /* SRC_VEINS_MPS_MODULES_UTILITY_LATENCY_H_ */
