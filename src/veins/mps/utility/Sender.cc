/*
  * Sender.cc
 *
 *  Created on: Jun 29, 2018
 *      Author: veins
 */

#include "veins/mps/utility/Sender.h"

#include "veins/mps/basic_checks/predictors/ExtendedKalmanFilter.h"
#include "veins/mps/basic_checks/predictors/KalmanFilter.h"
#include "veins/mps/basic_checks/predictors/UnscentedKalmanFilter.h"
#include "veins/mps/utility/Parameters.h"

Sender::
Sender()
{}



Sender::
Sender(
    BasicSafetyMessage bsm,
    const int    nStates,
    const int    nControls,
    const int    nMeasurements,
    const int    id,
    const Coord  firstPos,
    double       lastBeaconTime,
    u_int        window)
: nStates_                 (nStates),
  nControls_               (nControls),
  nMeasurements_           (nMeasurements),
  nMeasurementsNoise_      (nMeasurements),
  id_                      (id),
  bsm_(bsm),
  nextPosEstimate_(Coord()),
  firstPos_                (firstPos),
  firstBeaconTime_         (lastBeaconTime), // since this is updated only when the Sender is created, lastBeaconTime here is the time when the first beacon was received
  lastBeaconTime_          (lastBeaconTime),
  window_                  (window),
  posTrace_                (CoordVector_t(window_)),
  distanceTrace_           (DoubleVector_t(window_)),
  isDiscarded_             (false),
  isFirstBeacon_           (true),
  isFirstAttack_           (true),
  range95_(XY<Range>{Range{0,0}}),
  range99_(XY<Range>{Range{0,0}}),
  totalDisplacement_(Coord()),
  totalDistanceVelocity_(Coord()),
  fbrTrace_(0)
{}



Sender::
~Sender() {}



const Coord& Sender::
getFirstPos() const {
  return firstPos_;
}




void Sender::
setFirstPos(const Coord& firstPos) {
  firstPos_ = firstPos;
}



int Sender::
getId() const {
  return id_;
}



void Sender::
setId(int id) {
  id_ = id;
}



bool Sender::
isDiscarded() const {
  return isDiscarded_;
}



void Sender::
setIsDiscarded(bool isDiscarded) {
  isDiscarded_ = isDiscarded;
}



bool Sender::
isFirstBeacon() const {
  return isFirstBeacon_;
}



void Sender::
setIsFirstBeacon(bool isFirstBeacon) {
  isFirstBeacon_ = isFirstBeacon;
}



const double Sender::
getLastBeaconTime() const {
  return lastBeaconTime_;
}



void Sender::
setLastBeaconTime(const double lastBeaconTime) {
  lastBeaconTime_ = lastBeaconTime;
}



const DoubleVector_t& Sender::
getDistanceTrace() const {
  return distanceTrace_;
}



void Sender::
setDistanceTrace(const DoubleVector_t& distanceTrace) {
  distanceTrace_ = distanceTrace;
}



void Sender::
addToDistanceTrace(const double senderDistance) {
  distanceTrace_.at(window_ - 1) = senderDistance;
}



const Coord& Sender::
getPosStdDev() const {
  return posStdDev_;
}



void Sender::
setPosStdDev(const Coord& posStdDev) {
  posStdDev_ = posStdDev;
}



const CoordVector_t& Sender::
getPosTrace() const {
  return posTrace_;
}



void Sender::
setPosTrace(const CoordVector_t& posTrace) {
  posTrace_ = posTrace;
}


void Sender::
addToPosTrace(const Coord& senderPos) {
  posTrace_.at(window_ - 1) = senderPos;
}



const EigenVectorXdVector_t& Sender::
getPredictions() const {
  return predictions_;
}



void Sender::
setPredictions(const EigenVectorXdVector_t& predictions) {
  predictions_ = predictions;
}



const BaseKalmanFilter* Sender::
getTracker() const {
  return tracker_;
}



void Sender::
setTracker(BaseKalmanFilter* tracker) {
  tracker_ = tracker;
}



u_int Sender::
getWindow() const {
  return window_;
}



void Sender::
setWindow(u_int window) {
  window_ = window;
}



void Sender::
initializeTracker(const double predictionTimeStep, const int filterType) {
  auto params = Parameters(bsm_.getSenderPos(), bsm_.getSenderSpeed(), bsm_.getSenderAcceleration());

  switch (filterType) {
  case KF:
    setTracker(new KalmanFilter(predictionTimeStep, params, nStates_, nControls_, nMeasurements_));
    break;
  case EKF:
    setTracker(new ExtendedKalmanFilter(predictionTimeStep, params, nStates_, nControls_, nMeasurements_));
    break;
  case UKF:
    setTracker(new UnscentedKalmanFilter(predictionTimeStep, params, nStates_, nControls_, nMeasurements_));
    break;
  default:
    throw std::runtime_error("ERROR: Unknown type of filter chosen!");
  }
}



void Sender::
makePredictions(const int nPredictions) {
  // Update filter with new measurements
  auto measures      = Eigen::VectorXd(nMeasurements_);
  auto position = bsm_.getSenderPos();
  auto speed = bsm_.getSenderSpeed();
  measures           << position.x, position.y, speed.x, speed.y;

  auto measuresNoise = Eigen::VectorXd(nMeasurementsNoise_);
  measuresNoise      << 0.1, 0.1, 0.1, 0.1;

  auto accel = bsm_.getSenderAcceleration();
  auto acceleration  = Eigen::VectorXd(nControls_);
  acceleration       << accel.x, accel.y;

  tracker_->step(acceleration, measures, measuresNoise);

  // get next predicted state through getState()
  predictions_.push_back(tracker_->getState()); // Store all predictions

  // Get future predictions_
  for (auto i = 1; i < nPredictions; ++i) {
    tracker_->predict(acceleration.setZero()); // get update using last state of kalman filter
    predictions_.push_back(tracker_->getState());
  }
}



void Sender::
updatePosTrace() {
  // discard oldest position, insert newest position
  std::rotate(posTrace_.begin(), posTrace_.begin() + 1, posTrace_.end());
  *(posTrace_.rbegin()) = bsm_.getSenderPos();
}



void Sender::
updateDistanceTrace(const double senderDistance) {
  // discard oldest distance, insert newest distance
  std::rotate(distanceTrace_.begin(), distanceTrace_.begin() + 1, distanceTrace_.end());
  *(distanceTrace_.rbegin()) = senderDistance;
}



void Sender::
calcCi(XY<Range> accelCi95, XY<Range> accelCi99) {
  const auto pos = bsm_.getSenderPos();
  const auto spd = bsm_.getSenderSpeed();

  const auto time = simTime().dbl() - lastBeaconTime_;

  range95_.x.low = pos.x + time * spd.x + time * time * accelCi95.x.low;
  range95_.x.high = pos.x + time * spd.x + time * time * accelCi95.x.high;
  range95_.y.low = pos.y + time * spd.y + time * time * accelCi95.y.low;
  range95_.y.high = pos.y + time * spd.y + time * time * accelCi95.y.high;

  range99_.x.low = pos.x + time * spd.x + time * time * accelCi99.x.low;
  range99_.x.high = pos.x + time * spd.x + time * time * accelCi99.x.high;
  range99_.y.low = pos.y + time * spd.y + time * time * accelCi99.y.low;
  range99_.y.high = pos.y + time * spd.y + time * time * accelCi99.y.high;
}



const std::string& Sender::getAttackType() const
{
  return attackType_;
}

void Sender::setAttackType(const std::string& attackType)
{
  attackType_ = attackType;
}

bool Sender::getIsAttack() const
{
  return isAttack_;
}

void Sender::setIsAttack(bool isAttack)
{
  isAttack_ = isAttack;
}

Rating& Sender::getEwmaRating()
{
  return ewmaRating_;
}

void Sender::setEwmaRating(const Rating& ewmaRating)
{
  ewmaRating_ = ewmaRating;
}

Rating& Sender::getInstantRating()
{
  return instantRating_;
}

void Sender::setInstantRating(const Rating& instantRating)
{
  instantRating_ = instantRating;
}

V2xAppFlags& Sender::getV2xAppFlags()
{
  return v2xAppFlags_;
}

void Sender::setV2xAppFlags(const V2xAppFlags& v2xAppFlags)
{
  v2xAppFlags_ = v2xAppFlags;
}

bool Sender::getIsFirstAttack() const
{
  return isFirstAttack_;
}

void Sender::setIsFirstAttack(bool isFirstAttack)
{
  isFirstAttack_ = isFirstAttack;
}

Latency& Sender::getEwmaLatency()
{
  return ewmaLatency_;
}

void Sender::setEwmaLatency(const Latency& ewmaLatency)
{
  ewmaLatency_ = ewmaLatency;
}

double Sender::getRssi() const {
    return rssi_;
}

void Sender::setRssi(double rssi) {
    rssi_ = rssi;
}

int Sender::getMlInstantPrediction() const
{
  return mlInstantPrediction_;
}

void Sender::setMlInstantPrediction(int mlInstantPrediction)
{
  mlInstantPrediction_ = mlInstantPrediction;
}

int Sender::getMlEwmaPrediction() const
{
  return mlEwmaPrediction_;
}

void Sender::setMlEwmaPrediction(int mlEwmaPrediction)
{
  mlEwmaPrediction_ = mlEwmaPrediction;
}

const BasicSafetyMessage& Sender::getBsm() const
{
  return bsm_;
}

void Sender::setBsm(const BasicSafetyMessage& bsm)
{
  bsm_ = bsm;
}

const Coord& Sender::getNextPosEstimate() const
{
  return nextPosEstimate_;
}

void Sender::setNextPosEstimate(const Coord& nextPosEstimate)
{
  nextPosEstimate_ = nextPosEstimate;
}

const Coord& Sender::getNextSpdEstimate() const
{
  return nextSpdEstimate_;
}

void Sender::setNextSpdEstimate(const Coord& nextSpdEstimate)
{
  nextSpdEstimate_ = nextSpdEstimate;
}

const Coord& Sender::getNextPos2Estimate() const
{
  return nextPos2Estimate_;
}

void Sender::setNextPos2Estimate(const Coord& nextPos2Estimate)
{
  nextPos2Estimate_ = nextPos2Estimate;
}

const XY<Range>& Sender::getRange95() const
{
  return range95_;
}

const XY<Range>& Sender::getRange99() const
{
  return range99_;
}

const double Sender::getFirstBeaconTime() const
{
  return firstBeaconTime_;
}

void Sender::setFirstBeaconTime(const double firstBeaconTime)
{
  firstBeaconTime_ = firstBeaconTime;
}

const Coord& Sender::getTotalDisplacement() const
{
  return totalDisplacement_;
}

void Sender::setTotalDisplacement(const Coord& totalDisplacement)
{
  totalDisplacement_ += totalDisplacement;
}

const Coord& Sender::getTotalDistanceVelocity() const
{
  return totalDistanceVelocity_;
}

void Sender::setTotalDistanceVelocity(const Coord& totalDistanceVelocity)
{
  totalDistanceVelocity_ = totalDistanceVelocity;
}

int Sender::getFbrTrace() const
{
  return fbrTrace_;
}

void Sender::setFbrTrace(int fbrTrace)
{
  fbrTrace_ += fbrTrace;
  fbrTrace_ <<= 1;
}
