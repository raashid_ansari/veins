/*
 * Sender.h
 *
 *  Created on: Jun 29, 2018
 *      Author: veins
 */

#ifndef MODULES_UTILITY_SENDER_H_
#define MODULES_UTILITY_SENDER_H_

#include <veins/mps/utility/TypeDefs.h>
#include <veins/mps/basic_checks/predictors/BaseKalmanFilter.h>
#include <veins/mps/utility/Rating.h>
#include <veins/mps/utility/V2xAppFlags.h>
#include <veins/mps/utility/Latency.h>
#include <veins/modules/messages/BasicSafetyMessage_m.h>


class Sender
{
public:
  Sender();
  virtual ~Sender();

  Sender(
      BasicSafetyMessage bsm,
      const int    nStates,
      const int    nControls,
      const int    nMeasurements,
      const int    id,
      const Coord  firstPos,
      double       lastBeaconTime,
      u_int        window);

  void initializeTracker(
    const double predictionTimeStep,
    const int    filterType);

  void makePredictions(const int nPredictions);
  void calcCi(XY<Range> accelCi95, XY<Range> accelCi99);

  int getId() const;
  void setId(int id);

  bool isDiscarded() const;
  void setIsDiscarded(bool isDiscarded);

  const Coord& getFirstPos() const;
  void setFirstPos(const Coord& firstPos);

  bool isFirstBeacon() const;
  void setIsFirstBeacon(bool isFirstBeacon);

  const double getLastBeaconTime() const;
  void setLastBeaconTime(const double lastBeaconTime);

  const DoubleVector_t& getDistanceTrace() const;
  void setDistanceTrace(const DoubleVector_t& distanceTrace);
  void addToDistanceTrace(const double senderDistance);
  void updateDistanceTrace(const double senderDistance);

  const Coord& getPosStdDev() const;
  void setPosStdDev(const Coord& posStdDev);

  const CoordVector_t& getPosTrace() const;
  void setPosTrace(const CoordVector_t& posTrace);
  void addToPosTrace(const Coord& senderPos);
  void updatePosTrace();

  const EigenVectorXdVector_t& getPredictions() const;
  void setPredictions(const EigenVectorXdVector_t& predictions);

  const BaseKalmanFilter* getTracker() const;
  void setTracker(BaseKalmanFilter* tracker);

  u_int getWindow() const;
  void setWindow(u_int window);

  const std::string& getAttackType() const;
  void setAttackType(const std::string& attackType);
  bool getIsAttack() const;
  void setIsAttack(bool isAttack);

  Rating& getEwmaRating();
  void setEwmaRating(const Rating& ewmaRating);
  Rating& getInstantRating();
  void setInstantRating(const Rating& instantRating);
  V2xAppFlags& getV2xAppFlags();
  void setV2xAppFlags(const V2xAppFlags& v2xAppFlags);
  bool getIsFirstAttack() const;
  void setIsFirstAttack(bool isFirstAttack);
  Latency& getEwmaLatency();
  void setEwmaLatency(const Latency& ewmaLatency);
  double getRssi() const;
  void setRssi(double rssi);
  int getMlInstantPrediction() const;
  void setMlInstantPrediction(int mlInstantPrediction);
  int getMlEwmaPrediction() const;
  void setMlEwmaPrediction(int mlEwmaPrediction);
  const BasicSafetyMessage& getBsm() const;
  void setBsm(const BasicSafetyMessage& bsm);
  const Coord& getNextPosEstimate() const;
  void setNextPosEstimate(const Coord& nextPosEstimate);
  const Coord& getNextSpdEstimate() const;
  void setNextSpdEstimate(const Coord& nextSpdEstimate);
  const Coord& getNextPos2Estimate() const;
  void setNextPos2Estimate(const Coord& nextPos2Estimate);
  const XY<Range>& getRange95() const;
  const XY<Range>& getRange99() const;
  const double getFirstBeaconTime() const;
  void setFirstBeaconTime(const double firstBeaconTime);
  const Coord& getTotalDisplacement() const;
  void setTotalDisplacement(const Coord& totalDisplacement);
  const Coord& getTotalDistanceVelocity() const;
  void setTotalDistanceVelocity(const Coord& totalDistanceVelocity);
  int getFbrTrace() const;
  void setFbrTrace(int fbrTrace);

protected:
  int nStates_;
  int nControls_;
  int nMeasurements_;
  int nMeasurementsNoise_;

  enum Predictor {
    KF,
    EKF,
    UKF
  };

private:
  int    id_;
  BasicSafetyMessage bsm_;
  Coord  nextPosEstimate_;
  Coord  nextPos2Estimate_;
  Coord  nextSpdEstimate_;
  Coord  firstPos_;
  double  firstBeaconTime_;

  Rating instantRating_;
  Rating ewmaRating_;

  double lastBeaconTime_;
  u_int  window_;
  CoordVector_t  posTrace_;
  DoubleVector_t distanceTrace_;
  bool   isDiscarded_;
  bool   isFirstBeacon_;

  Coord  posStdDev_;

  BaseKalmanFilter*     tracker_;
  EigenVectorXdVector_t predictions_;

  bool isAttack_;
  bool isFirstAttack_;
  std::string attackType_;
  int mlInstantPrediction_;
  int mlEwmaPrediction_;

  V2xAppFlags v2xAppFlags_;

  Latency ewmaLatency_;
  double rssi_;

  XY<Range> range95_;
  XY<Range> range99_;

  Coord totalDisplacement_;
  Coord totalDistanceVelocity_;
  int fbrTrace_;
};

#endif /* MODULES_UTILITY_SENDER_H_ */
