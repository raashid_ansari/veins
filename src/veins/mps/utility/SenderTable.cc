// This class has been written to initialize the maximum
// number of senders that can be tracked by a receiver.

#include <veins/mps/utility/SenderTable.h>

SenderTable::
SenderTable()
{}



SenderTable::
~SenderTable() {}



void SenderTable::
add(Sender sender) {
  senders_.push_back(sender);
}



void SenderTable::
remove(const int senderId) {
  for (auto& sender : senders_) {
    if (sender.getId() == senderId) {
      sender.setIsDiscarded(true);
      break;
    }
  }
}



Sender* SenderTable::
find(const int senderId) {
  for (auto& sender : senders_)
    if (sender.getId() == senderId)
      return &sender;

  return nullptr;
}


bool SenderTable::
isNew(const int senderId) {
  return find(senderId) == nullptr;
}


SenderVector_t& SenderTable::
getSenders() {
  return senders_;
}
