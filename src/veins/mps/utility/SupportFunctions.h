/*
 * SupportFunctions.h
 *
 *  Created on: Jul 5, 2018
 *      Author: veins
 */

#ifndef COMMON_SUPPORTFUNCTIONS_H_
#define COMMON_SUPPORTFUNCTIONS_H_

#include <veins/mps/utility/TypeDefs.h>
#include "veins/base/utils/Coord.h"

class SupportFunctions
{
public:
  SupportFunctions();
  virtual ~SupportFunctions();

  /* @brief Find if car is ahead or behind */
  const bool   isBehind                  (const Coord& currPos, const Coord& senderPos, const Coord& currDirection) const;
  const double getSafetyDistance         (const Coord& speed) const;
//  const cars::Car& findCarInSimulation();
  const Coord  getMovingStandardDeviation(const CoordVector_t& attribute);
  const double getMovingStandardDeviation(const DoubleVector_t& attribute);
  const Coord  getPosOffset              (const Coord& attackerPos, const Coord& targetPos, const double distance);
  Rectangle getVertices(const Coord& point, const double length, const double width) const;
};

#endif /* COMMON_SUPPORTFUNCTIONS_H_ */
