//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef VEINS_MPS_MODULES_UTILITY_SENDERTABLE_H_
#define VEINS_MPS_MODULES_UTILITY_SENDERTABLE_H_

#include <vector>

#include "veins/mps/utility/SenderTable.h"
#include "Sender.h"

typedef std::vector<Sender> SenderVector_t;

class SenderTable
{
public:
  SenderTable();
  ~SenderTable();

  void add(Sender sender);
  Sender* find(const int senderId);
  void remove(const int senderId);
  bool isNew(const int senderId);
  SenderVector_t& getSenders();
private:
  SenderVector_t senders_;
};

#endif /* VEINS_MPS_MODULES_UTILITY_SENDERTABLE_H_ */
