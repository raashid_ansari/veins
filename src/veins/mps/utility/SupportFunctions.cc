/*
 * SupportFunctions.cc
 *
 *  Created on: Jul 5, 2018
 *      Author: veins
 */

//#include <algorithm>
#include <numeric>
#include "veins/mps/utility/SupportFunctions.h"

SupportFunctions::SupportFunctions()
{
  // TODO Auto-generated constructor stub

}

SupportFunctions::~SupportFunctions()
{
  // TODO Auto-generated destructor stub
}

inline double toPositiveAngle(double angle) {
   angle = fmod(angle, 360);
   while (angle < 0) angle += 360.0;
   return angle;
}

//const cars::Car& SupportFunctions::
//findCarInSimulation() {
//  thisCar_ = *(carVec_.rbegin());
//  thisCar_->setSimulationId(findHost()->getId());
//  thisCar_->setSimulationModule(findHost());
//  thisCar_->getSimulationModule()->setName(thisCar_->getGivenId().c_str());
//}

const bool SupportFunctions::
isBehind(
    const Coord& position,
    const Coord& senderPos,
    const Coord& direction) const { // TODO: Probably use omnet2traci class for this or traci2omnet class
  auto angle = toPositiveAngle(
      std::atan2(direction.y, direction.x) * 180 / M_PI
      );
//  std::cout << "My Direction: " << direction << "\t\t"
//            << "Direction (degrees): "  << angle << "\t"
//            << std::endl;

//======================================================================================================================
//  Given a directed line from point p0(x0, y0) to p1(x1, y1),
//  the following condition tells whether a point p2(x2, y2) is
//  on the left of the line, on the right, or on the same line:
//
//  value = (x1 - x0)*(y2 - y0) - (y1 - y0)*(x2 - x0)
//
//  if value > 0, p2 is on the left side of the line.
//  if value = 0, p2 is on the same line.
//  if value < 0, p2 is on the right side of the line.
//  https://math.stackexchange.com/questions/175896/finding-a-point-along-a-line-a-certain-distance-away-from-another-point
//======================================================================================================================

  if (angle > 0 && angle < 180) {
//    std::cout << "Going Downwards or Rightwards" << std::endl;
    // choose point on positive y axis
    Coord p1 = Coord(0, position.y, position.z);
    return (p1.x - position.x) * (senderPos.y - position.y) - (p1.y - position.y) * (senderPos.x - position.x) > 0;
  }
  else if (angle > 180 && angle < 360) {
//    std::cout << "Going Upwards or Leftwards" << std::endl;
    // choose point on negative y axis
    Coord p1 = Coord(0, position.y, position.z);
    return (p1.x - position.x) * (senderPos.y - position.y) - (p1.y - position.y) * (senderPos.x - position.x) < 0;
  }
  else if (angle == 180) {
//    std::cout << "On 180 line" << std::endl;
    // choose point perpendicularly above x axis
    Coord p1 = Coord(position.x, position.y+5, position.z);
    return (p1.x - position.x) * (senderPos.y - position.y) - (p1.y - position.y) * (senderPos.x - position.x) < 0;
  }
  else if (angle == 0 || angle == 360) {
//    std::cout << "On 0 line" << std::endl;
    // choose point perpendicularly below x axis
    Coord p1 = Coord(position.x, position.y+5, position.z);
    return (p1.x - position.x) * (senderPos.y - position.y) - (p1.y - position.y) * (senderPos.x - position.x) > 0;
  }
  else {
    throw std::runtime_error("Invalid angle given to isBehind function");
  }
  return false;
}
const Coord SupportFunctions::
getPosOffset(const Coord& attackerPos, const Coord& targetPos, const double distance) {
  auto offset = targetPos - attackerPos;
  auto u = offset / std::sqrt(offset.squareLength());

  offset.x = targetPos.x + distance * u.x;
  offset.y = targetPos.y + distance * u.y;
  offset.z = targetPos.z + distance * u.z;

  return offset; // equivalent to return attacker_pos + (distance + attacker_pos.distance(target_pos)) * u;
}



const double SupportFunctions::
getSafetyDistance(const Coord& speed) const {
  const auto MIN_SAFETY_DIST = 5.0;
  auto rmsSpeed = std::sqrt(speed.x * speed.x + speed.y * speed.y);

  // return calculated safety distance formula based on the 3-second rule as explained in the Wikipedia article at:
  // https://en.wikipedia.org/wiki/Assured_Clear_Distance_Ahead#Seconds_of_distance_to_stop_rule_2
  // safetyDist = (speed^2/20) + speed
  auto calcSafetyDist = ((rmsSpeed * rmsSpeed) / 20) + rmsSpeed;

  return calcSafetyDist < MIN_SAFETY_DIST ? MIN_SAFETY_DIST : calcSafetyDist;
}



const Coord SupportFunctions::
getMovingStandardDeviation(const CoordVector_t& attribute) {

  // Calculate mean
  auto sum = Coord();
  for (auto& a : attribute)
    sum += a;

  auto mean = Coord();
  mean = sum / attribute.size();

  // Calculate difference from mean and add it all up
  auto diffFromMean = Coord();
  for (auto& a : attribute) {
    diffFromMean.x += std::pow(a.x - mean.x, 2);
    diffFromMean.y += std::pow(a.y - mean.y, 2);
    diffFromMean.z += std::pow(a.z - mean.z, 2);
  }

  // Calculate variance
  auto variance = Coord();
  variance.x = diffFromMean.x / attribute.size();
  variance.y = diffFromMean.y / attribute.size();
  variance.z = diffFromMean.z / attribute.size();

  // Calculate standard deviation
  auto stdDev = Coord();
  stdDev.x = std::sqrt(variance.x);
  stdDev.y = std::sqrt(variance.y);
  stdDev.z = std::sqrt(variance.z);

  return stdDev;
}



const double SupportFunctions::
getMovingStandardDeviation(const DoubleVector_t& attribute) {
  // Calculate mean
  auto sum = std::accumulate(attribute.begin(), attribute.end(), 0.0);
  auto mean = sum/attribute.size();

  // Calculate difference from mean and add it all up
  auto diffFromMean = 0.0;
  for (auto& a : attribute)
    diffFromMean += std::pow(a - mean, 2);

  // Calculate variance
  auto variance = diffFromMean/attribute.size();

  // return standard deviation
  return std::sqrt(variance);
}


Rectangle SupportFunctions::
getVertices(const Coord& point, const double length, const double width) const {
  Coord lt;
  lt.x = point.x - 0.5*width;
  lt.y = point.y + 0.5*length;

  Coord lb;
  lb.x = point.x - 0.5*width;
  lb.y = point.y - 0.5*length;

  Coord rt;
  rt.x = point.x + 0.5*width;
  rt.y = point.y + 0.5*length;

  Coord rb;
  rb.x = point.x + 0.5*width;
  rb.y = point.y - 0.5*length;

  return Rectangle{lb, rb, rt, lt};
}
