//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef MODULES_UTILITY_PARAMETERS_H_
#define MODULES_UTILITY_PARAMETERS_H_

#include "veins/base/utils/Coord.h"

class Parameters
{
public:
    Parameters                  ();
    Parameters                  (Coord position, Coord speed, Coord acceleration);
    virtual ~Parameters         ();

    const Coord& getPosition    () const;
    void         setPosition    (const Coord& position);

    const Coord& getSpeed       () const;
    void         setSpeed       (const Coord& speed);

    const Coord& getAcceleration() const;
    void         setAcceleration(const Coord& acceleration);

protected:
    Coord position_;
    Coord speed_;
    Coord acceleration_;
};

#endif /* MODULES_UTILITY_PARAMETERS_H_ */
