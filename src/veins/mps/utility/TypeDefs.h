/*
 * Definitions.h
 *
 *  Created on: Jul 3, 2018
 *      Author: veins
 */

#ifndef COMMON_TYPEDEFSANDENUMS_H_
#define COMMON_TYPEDEFSANDENUMS_H_

#include <chrono>
#include <Eigen/Dense>

#include "veins/base/utils/Coord.h"
#include "veins/modules/messages/BasicSafetyMessage_m.h"

#define LOG(X) X.log()

typedef std::vector<Coord>           CoordVector_t;
typedef std::vector<double>          DoubleVector_t;
typedef std::vector<Eigen::VectorXd> EigenVectorXdVector_t;
typedef std::set<int>                IntSet_t;
typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::time_point<Clock> TimePoint;

enum logType {
  LOGTYPE_ALL,
  LOGTYPE_CSV,
  LOGTYPE_JSON
};

struct Rectangle {
  Coord a, b, c, d;
};

struct Triangle {
  Coord a, b, c;
};

enum PdResult {
  PD_UNAVAILABLE = -1,
  PD_SUCCESS,
  PD_FAIL
};

enum BrakeStatus {
 MASK        = 0x1F, // 0001 1111
 UNAVAILABLE = 0x10, // 0001 0000
 LEFT_FRONT  = 0x08, // 0000 1000
 LEFT_REAR   = 0x04, // 0000 0100
 RIGHT_FRONT = 0x02, // 0000 0010
 RIGHT_REAR  = 0x01  // 0000 0001
};

struct Range {
  double low, high;
};

template <typename T>
struct XY {
  T x, y;
};

#endif /* COMMON_TYPEDEFS_H_ */
