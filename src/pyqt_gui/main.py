#! /usr/bin/env python3

###############################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: GUI for MPS
###############################################

import re
import os
import sys
import glob
import pathlib
import shutil
import subprocess
import signal
from datetime import datetime
from contextlib import contextmanager

from PyQt4 import QtGui, QtCore, QtTest
from PyQt4.QtCore import pyqtSlot
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

from gui import gui
import core
from plots.mps import MpsPlot
from plots.attack import AttackPlot
from plots.latency import LatencyPlot
from plots.executiontime import ExecTimePlot

class TimeOutError(Exception):
    pass

class CustomMainWindow(QtGui.QMainWindow):

    def __init__(self):

        super(CustomMainWindow, self).__init__()

        self.mainUi = gui.Ui_MainWindow()
        self.mainUi.setupUi(self)
        self.mainUi.stopButton.setDisabled(True)

        self.base_dir = "{}/src/veins/examples/mps".format(os.environ['HOME'])
        self.mainUi.outputPathTextEdit.setText("{}/results".format(self.base_dir))

        self.getIniFiles()
        self.addAttacks()

        self.core = core.Core()
        self.mps = MpsPlot()
        self.attack = AttackPlot()
        self.latencyPlot = LatencyPlot()
        self.execTimePlot = ExecTimePlot()
        self.addPlot(self.mps, self.mainUi.mpsPerformanceTab)
        self.addPlot(self.attack, self.mainUi.eeblAttackPerformanceTab)
        self.addPlot(self.latencyPlot, self.mainUi.latencyPlotTab)
        self.addPlot(self.execTimePlot, self.mainUi.execTimeTab)

        self._omnet_process = QtCore.QProcess(self)
        self._ml_server_process = QtCore.QProcess(self)

        self._timer = QtCore.QTimer(self)
        self._checks_update_timer = QtCore.QTimer(self)
        self._populate_host_vehicles_timer = QtCore.QTimer(self)

        self.connectEvents()
        self.show()

    def getIniFiles(self):
        ini_file_path = "{}/*.ini".format(self.base_dir)
        configs = [config.split("/")[-1] for config in glob.glob(ini_file_path) if not "omnetpp" in config]
        configs.sort()
        self.mainUi.configFileComboBox.clear()
        self.mainUi.configFileComboBox.addItems(configs)

    def closeEvent(self, event):
        if self._timer.isActive():
            self._timer.stop()
            self._omnet_process.close()
            self._ml_server_process.close()

        if self._checks_update_timer.isActive():
            self._checks_update_timer.stop()

        if self._populate_host_vehicles_timer.isActive():
            self._populate_host_vehicles_timer.stop()

        QtGui.QWidget.closeEvent(self, event)

    def readProcessOutput(self):
        output = str(self._omnet_process.readAll()).split("\\r")
        # print(output)
        for line in output:
            if "Step" and "TOT" and "ACT" and "TraCI" and "BUF" and not "~=" in line.strip():
                tokens = re.findall(r"[\w\d\.#]+", line)
                try:
                    self.mainUi.messageLabel.setText(
                            tokens[0] + ": " + tokens[1]
                            + " Vehicles:: Active: " + tokens[11]
                            + " Departed: " + str(int(tokens[9]) - int(tokens[11]))
                            + " Total: " + tokens[9])
                except IndexError:
                    pass
                except ValueError:
                    pass

    def connectEvents(self):
        # buttons
        self.mainUi.startButton.clicked.connect(self.run)
        self.mainUi.stopButton.clicked.connect(self.stopOmnetCommand)
        self.mainUi.selectOutputPathButton.clicked.connect(self.getFolderPath)

        # timers
        self._checks_update_timer.timeout.connect(self.createRemoteVehicleWidgets)
        self._populate_host_vehicles_timer.timeout.connect(self.populateHostVehicles)
        self._timer.timeout.connect(self._writeSystemResourceUsage)

        # combo-boxes
        self.mainUi.hostVehicleIdComboBox.currentIndexChanged.connect(self.runLocalMps)
        self.mainUi.selectAttackComboBox.currentIndexChanged.connect(self.updateLogPath)
        self.mainUi.configFileComboBox.currentIndexChanged.connect(self.addAttacks)

        # pathtext edits
        self.mainUi.outputPathTextEdit.textChanged.connect(self.updateLogPath)

        # checkboxes
        self.mainUi.animateCheckBox.stateChanged.connect(self.animatePlots)

        # slots
        self._omnet_process.readyRead.connect(self.readProcessOutput)
        self._omnet_process.finished.connect(self.stopOmnetCommand)
        self.mps.attack_data_ready.connect(self.updateAttackStatus)
        self.attack.attack_data_ready.connect(self.updateAttackStatus)

    def _writeSystemResourceUsage(self):
        sru = core.sysuse.SystemResourceUsage()
        try:
            with open(self.log_path + "/ramUsage.csv", "a") as ramUsage_fh:
                ramUsage = sru.getRamUsage(self._omnet_process.pid())
                cpuUsage = sru.getCpuUsage(self._omnet_process.pid())
                ramUsage_fh.write('{0:.3f},{1:.3f}'.format(ramUsage, cpuUsage) + "\n")
        except FileNotFoundError:
            pass

    @pyqtSlot(int, int, int)
    def updateAttackStatus(self, nAttacks, mpsOff, mpsOn):
        status = "Total: {0:d} MPS off: {1:d} MPS on: {2:d} Reduction: {3:.2f}%".format(
                nAttacks, mpsOff, mpsOn, ((mpsOff - mpsOn)*100)/mpsOff)
        self.mainUi.attackStatsLabel.setText(status)

    @pyqtSlot(int)
    def animatePlots(self, checkBoxState):
        if checkBoxState == 2: # checked
            self.mps.animatePlot()
            self.attack.animatePlot()
            self.latencyPlot.animatePlot()
            self.execTimePlot.animatePlot()
        elif checkBoxState == 0: # unchecked
            self.mps._animate_timer.stop()
            self.attack._animate_timer.stop()
            self.latencyPlot._animate_timer.stop()
            self.execTimePlot._animate_timer.stop()

    def updateLogPath(self):
        self.log_path = "{}/{}".format(self.mainUi.outputPathTextEdit.toPlainText(), self.mainUi.selectAttackComboBox.currentText())
        self.mps.log_path = self.log_path
        self.attack.log_path = self.log_path
        self.latencyPlot.log_path = self.log_path
        self.execTimePlot.log_path = self.log_path

        # Check if trace files available
        if not glob.glob("{}/*trace.csv".format(self.log_path)):
            return

        self.core.extract_data(self.log_path)
        self.mps.core = self.core
        self.attack.core = self.core
        self.latencyPlot.core = self.core
        self.execTimePlot.core = self.core
        self.populateHostVehicles()


    def runLocalMps(self):
        if not self.log_path:
            error = "No log path entered!"
            self.mainUi.errorLabel.setText(error)
            raise ValueError(error)
        else:
            self.mainUi.errorLabel.clear()
        # self.createRemoteVehicleWidgets()
        self._checks_update_timer.start(100)
        self._populate_host_vehicles_timer.start(1000)

    def addAttacks(self):
        configs = []
        try:
            with open("{}/{}".format(self.base_dir, self.mainUi.configFileComboBox.currentText()), "r") as omnetpp_fh:
                configs = omnetpp_fh.readlines()
                self.mainUi.errorLabel.clear()
        except FileNotFoundError:
            self.mainUi.errorLabel.setText("config file not found")

        attacks = [re.findall(r"[\w']+", config)[-1] for config in configs if "Config" in config and not "Gui" in config]
        attack = self.mainUi.selectAttackComboBox.currentText()
        self.mainUi.selectAttackComboBox.clear()
        self.mainUi.selectAttackComboBox.addItems(attacks)
        attack_idx = self.mainUi.selectAttackComboBox.findText(attack)
        self.mainUi.selectAttackComboBox.setCurrentIndex(attack_idx)

    def addPlot(self, plotter, tabName):
        toolbar = NavigationToolbar(plotter, self)
        self.plotButton = QtGui.QPushButton("Plot")
        self.animateButton = QtGui.QPushButton("Animate")

        self.artCheckBox = QtGui.QCheckBox()
        self.artCheckBox.setText("ART")
        self.mbfCheckBox = QtGui.QCheckBox()
        self.mbfCheckBox.setText("MBF")
        self.sawCheckBox = QtGui.QCheckBox()
        self.sawCheckBox.setText("SAW")
        self.mvpCheckBox = QtGui.QCheckBox()
        self.mvpCheckBox.setText("MVP")
        self.mvnCheckBox = QtGui.QCheckBox()
        self.mvnCheckBox.setText("MVN")
        self.mapCheckBox = QtGui.QCheckBox()
        self.mapCheckBox.setText("MAP")
        self.manCheckBox = QtGui.QCheckBox()
        self.manCheckBox.setText("MAN")
        self.mdmCheckBox = QtGui.QCheckBox()
        self.mdmCheckBox.setText("MDM")
        self.kalmanCheckBox = QtGui.QCheckBox()
        self.kalmanCheckBox.setText("Kalman")
        self.suspicionCheckBox = QtGui.QCheckBox()
        self.suspicionCheckBox.setText("Suspicion")

        self.plotButton.clicked.connect(plotter.plot)
        self.animateButton.clicked.connect(plotter.animatePlot)

        layout = QtGui.QVBoxLayout(tabName)
        layout.addWidget(plotter)
        hLayout = QtGui.QHBoxLayout()
        hLayout.addWidget(toolbar)

        hLayout.addWidget(self.artCheckBox)
        hLayout.addWidget(self.mbfCheckBox)
        hLayout.addWidget(self.sawCheckBox)
        hLayout.addWidget(self.mvpCheckBox)
        hLayout.addWidget(self.mvnCheckBox)
        hLayout.addWidget(self.mapCheckBox)
        hLayout.addWidget(self.manCheckBox)
        hLayout.addWidget(self.mdmCheckBox)
        hLayout.addWidget(self.kalmanCheckBox)
        hLayout.addWidget(self.suspicionCheckBox)

        hLayout.addWidget(self.plotButton)
        hLayout.addWidget(self.animateButton)
        layout.addLayout(hLayout)
        tabName.setLayout(layout)

    def getFolderPath(self):
        folder_name = QtGui.QFileDialog.getExistingDirectory(None, 'Select a folder:', os.environ['HOME'], QtGui.QFileDialog.ShowDirsOnly)
        if folder_name:
            self.mainUi.outputPathTextEdit.setText(folder_name)

    def buildOppRunArgs(self):
        run_args = "opp_run -m -n .:../veins:../../src/veins --image-path=../../images -l ../../src/veins -c".split()
        run_args.append(self.mainUi.selectAttackComboBox.currentText())

        # Handle GUI choice
        run_args.append("-u")
        if "NoGui" in self.mainUi.configFileComboBox.currentText():
            run_args.append("Cmdenv")
        else:
            run_args.append("Qtenv")
        run_args.append(self.mainUi.configFileComboBox.currentText())
        return run_args


    def executeOmnetCommand(self):
        self.mainUi.startButton.setDisabled(True)
        self.mainUi.stopButton.setEnabled(True)
        self.mainUi.configFileComboBox.setDisabled(True)
        self.mainUi.outputPathTextEdit.setDisabled(True)
        self.mainUi.selectOutputPathButton.setDisabled(True)
        self.mainUi.selectAttackComboBox.setDisabled(True)
        self._timer.start(50)
        try:
            shutil.rmtree(self.log_path)
            self.populateHostVehicles()
        except FileNotFoundError:
            error = "Nothing to delete at: " + self.log_path
            print(error)
            self.mainUi.errorLabel.setText(error)


        run_args = self.buildOppRunArgs()
        self._omnet_process.start(run_args[0], run_args[1:], QtCore.QIODevice.ReadOnly)
        with cd("{}/src/veins/src/veins/mps/detection_algorithms".format(os.environ['HOME'])):
            self._ml_server_process.start("gunicorn", "-w 1 -b 127.0.0.1:8000 MLServer:app".split(), QtCore.QIODevice.ReadOnly)


    def waitForTraceFiles(self):
        # wait for trace files to be generated
        # initialize timeout timer
        signal.signal(signal.SIGALRM, self._sig_alarm)
        if "NoGui" in self.mainUi.configFileComboBox.currentText():
            signal.alarm(60)
        else:
            signal.alarm(120)
        try:
            count = 1
            while(not glob.glob("{}/*_trace.csv".format(self.log_path))):
                QtTest.QTest.qWait(2000)
                wait_list = ["Waiting for trace files"]
                wait_list = wait_list + ['.' for i in range(count)]
                count = count + 1
                wait_string = ""
                for item in wait_list:
                    wait_string = wait_string + item
                self.mainUi.messageLabel.setText(wait_string)
            self.populateHostVehicles()
        except TimeoutError:
            error = "Timeout reached for trace file lookup"
            print(error)
            self.mainUi.errorLabel.setText(error)
            self.mainUi.messageLabel.clear()
        # cancel timeout timer
        signal.alarm(0)

    def stopOmnetCommand(self):
        if self._timer.isActive():
            self._timer.stop()
            self._omnet_process.close()
            self._ml_server_process.close()
            self.mainUi.startButton.setEnabled(True)
            self.mainUi.stopButton.setDisabled(True)
            self.mainUi.configFileComboBox.setEnabled(True)
            self.mainUi.outputPathTextEdit.setEnabled(True)
            self.mainUi.selectOutputPathButton.setEnabled(True)
            self.mainUi.selectAttackComboBox.setEnabled(True)
            self.mainUi.errorLabel.clear()
            self.mainUi.messageLabel.clear()

        if self._checks_update_timer.isActive():
            self._checks_update_timer.stop()

        if self._populate_host_vehicles_timer.isActive():
            self._populate_host_vehicles_timer.stop()

        if self.mps._animate_timer.isActive():
            self.mps._animate_timer.stop()

        if self.attack._animate_timer.isActive():
            self.attack._animate_timer.stop()

        if self.latencyPlot._animate_timer.isActive():
            self.latencyPlot._animate_timer.stop()

        if self.execTimePlot._animate_timer.isActive():
            self.execTimePlot._animate_timer.stop()

    def populateHostVehicles(self):
        # Get Host vehicles
        host_vehicles = self.core.get_host_vehicles()
        # populate host vehicles in drop-down
        if host_vehicles.size == 0:
            self.mainUi.hostVehicleIdComboBox.clear()
            error = "No Host Vehicles found!"
            if not self._timer.isActive():
                error = error + " No simulation is runnning!"
            self.mainUi.errorLabel.setText(error)
        else:
            hv = self.mainUi.hostVehicleIdComboBox.currentText()
            self.mainUi.hostVehicleIdComboBox.clear()
            self.mainUi.hostVehicleIdComboBox.addItems(host_vehicles)
            hv_idx = self.mainUi.hostVehicleIdComboBox.findText(hv)
            self.mainUi.hostVehicleIdComboBox.setCurrentIndex(hv_idx)
            if not self._timer.isActive():
                self.mainUi.errorLabel.clear()

    def run(self):
        error = "No log path entered!"
        try:
            if not self.log_path:
                self.mainUi.errorLabel.setText(error)
                raise ValueError(error)
            else:
                self.mainUi.errorLabel.clear()
        except AttributeError:
            print(error)
            self.mainUi.errorLabel.setText(error)
            return

        # self.mps.log_path = self.log_path
        # self.eebl.log_path = self.log_path

        with cd(self.base_dir):
            self.executeOmnetCommand()

        self.waitForTraceFiles()

        if self.mainUi.animateCheckBox.isChecked():
            self.mps.animatePlot()
            self.attack.animatePlot()
            self.latencyPlot.animatePlot()
            self.execTimePlot.animatePlot()

    def _sig_alarm(self, sig, tb):
        raise TimeoutError("timeout")

    def createRemoteVehicleWidgets(self):
        if self.mainUi.checksStatusWidget.layout():
            QtGui.QWidget().setLayout(self.mainUi.checksStatusWidget.layout())

        if not self.mainUi.hostVehicleIdComboBox.currentText():
            return

        checksGridLayoutWidget = QtGui.QWidget(self.mainUi.checksStatusWidget)

        hLayout = QtGui.QHBoxLayout(checksGridLayoutWidget)
        vLayout = QtGui.QVBoxLayout()
        layout = QtGui.QGridLayout()

        hLayout.addLayout(vLayout)
        vLayout.addLayout(layout)

        hLayout.addStretch(1)
        vLayout.addStretch(1)

        self.mainUi.checksStatusWidget.setLayout(hLayout)

        col = 0
        remoteVehicleIdWidget = QtGui.QWidget(checksGridLayoutWidget)
        remoteVehicleIdWidget.setFixedSize(42, 20)
        remoteVehicleIdLabel = QtGui.QLabel(remoteVehicleIdWidget)
        remoteVehicleIdLabel.setText("RV ID")
        layout.addWidget(remoteVehicleIdWidget, 0, col, 1, 1)

        col = col + 1
        artWidget = QtGui.QWidget(checksGridLayoutWidget)
        artWidget.setFixedSize(42,20)
        artLabel = QtGui.QLabel(artWidget)
        artLabel.setText("ART")
        layout.addWidget(artWidget, 0, col, 1, 1)

        col = col + 1
        sawWidget = QtGui.QWidget(checksGridLayoutWidget)
        sawWidget.setFixedSize(42,20)
        sawLabel = QtGui.QLabel(sawWidget)
        sawLabel.setText("SAW")
        layout.addWidget(sawWidget, 0, col, 1, 1)

        col = col + 1
        mbfWidget = QtGui.QWidget(checksGridLayoutWidget)
        mbfWidget.setFixedSize(42,20)
        mbfLabel = QtGui.QLabel(mbfWidget)
        mbfLabel.setText("MBF")
        layout.addWidget(mbfWidget, 0, col, 1, 1)

        col = col + 1
        mvpWidget = QtGui.QWidget(checksGridLayoutWidget)
        mvpWidget.setFixedSize(42,20)
        mvpLabel = QtGui.QLabel(mvpWidget)
        mvpLabel.setText("MVP")
        layout.addWidget(mvpWidget, 0, col, 1, 1)

        col = col + 1
        mvnWidget = QtGui.QWidget(checksGridLayoutWidget)
        mvnWidget.setFixedSize(42,20)
        mvnLabel = QtGui.QLabel(mvnWidget)
        mvnLabel.setText("MVN")
        layout.addWidget(mvnWidget, 0, col, 1, 1)

        col = col + 1
        mapWidget = QtGui.QWidget(checksGridLayoutWidget)
        mapWidget.setFixedSize(42,20)
        mapLabel = QtGui.QLabel(mapWidget)
        mapLabel.setText("MAP")
        layout.addWidget(mapWidget, 0, col, 1, 1)

        col = col + 1
        manWidget = QtGui.QWidget(checksGridLayoutWidget)
        manWidget.setFixedSize(42,20)
        manLabel = QtGui.QLabel(manWidget)
        manLabel.setText("MAN")
        layout.addWidget(manWidget, 0, col, 1, 1)

        col = col + 1
        mdmWidget = QtGui.QWidget(checksGridLayoutWidget)
        mdmWidget.setFixedSize(42,20)
        mdmLabel = QtGui.QLabel(mdmWidget)
        mdmLabel.setText("MDM")
        layout.addWidget(mdmWidget, 0, col, 1, 1)

        col = col + 1
        kalmanWidget = QtGui.QWidget(checksGridLayoutWidget)
        kalmanWidget.setFixedSize(42,20)
        kalmanLabel = QtGui.QLabel(kalmanWidget)
        kalmanLabel.setText("Kalman")
        layout.addWidget(kalmanWidget, 0, col, 1, 1)

        col = col + 1
        fbrWidget = QtGui.QWidget(checksGridLayoutWidget)
        fbrWidget.setFixedSize(42,20)
        fbrLabel = QtGui.QLabel(fbrWidget)
        fbrLabel.setText("FBR")
        layout.addWidget(fbrWidget, 0, col, 1, 1)

        col = col + 1
        suspicionWidget = QtGui.QWidget(checksGridLayoutWidget)
        suspicionWidget.setFixedSize(55,20)
        suspicionLabel = QtGui.QLabel(suspicionWidget)
        suspicionLabel.setText("Suspicion")
        layout.addWidget(suspicionWidget, 0, col, 1, 1)

        col = col + 1
        mlPredictionWidget = QtGui.QWidget(checksGridLayoutWidget)
        mlPredictionWidget.setFixedSize(55,20)
        mlPredictionLabel = QtGui.QLabel(mlPredictionWidget)
        mlPredictionLabel.setText("ML")
        layout.addWidget(mlPredictionWidget, 0, col, 1, 1)

        col = col + 1
        eeblMpsOffWidget = QtGui.QWidget(checksGridLayoutWidget)
        eeblMpsOffWidget.setFixedSize(85,20)
        eeblMpsOffLabel = QtGui.QLabel(eeblMpsOffWidget)
        eeblMpsOffLabel.setText("EEBL(MPS OFF)")
        layout.addWidget(eeblMpsOffWidget, 0, col, 1, 1)

        col = col + 1
        eeblMpsOnWidget = QtGui.QWidget(checksGridLayoutWidget)
        eeblMpsOnWidget.setFixedSize(85,20)
        eeblMpsOnLabel = QtGui.QLabel(eeblMpsOnWidget)
        eeblMpsOnLabel.setText("EEBL(MPS ON)")
        layout.addWidget(eeblMpsOnWidget, 0, col, 1, 1)

        rvs = self.getRemoteVehicles()
        for row, rv in enumerate(rvs):
            col = 0
            remoteVehicleIdWidget = QtGui.QWidget(checksGridLayoutWidget)
            remoteVehicleIdWidget.setFixedHeight(20)
            if not rv.isGenuine:
                remoteVehicleIdWidget.setStyleSheet("background-color: red;")
            remoteVehicleIdLabel = QtGui.QLabel(remoteVehicleIdWidget)
            remoteVehicleIdLabel.setText(str(rv.id))
            layout.addWidget(remoteVehicleIdWidget, row+1, col, 1, 1)

            col = col + 1
            artWidget = QtGui.QWidget(checksGridLayoutWidget)
            artLabel = QtGui.QLabel(artWidget)
            artLabel.setText(str('{:1.4f}'.format(rv.art.value)))
            artWidget.setStyleSheet("background-color: " + rv.art.color + ";")
            layout.addWidget(artWidget, row+1, col, 1, 1)

            col = col + 1
            sawWidget = QtGui.QWidget(checksGridLayoutWidget)
            sawLabel = QtGui.QLabel(sawWidget)
            sawLabel.setText(str('{:1.4f}'.format(rv.saw.value)))
            sawWidget.setStyleSheet("background-color: " + rv.saw.color + ";")
            layout.addWidget(sawWidget, row+1, col, 1, 1)

            col = col + 1
            mbfWidget = QtGui.QWidget(checksGridLayoutWidget)
            mbfLabel = QtGui.QLabel(mbfWidget)
            mbfLabel.setText(str('{:1.4f}'.format(rv.mbf.value)))
            mbfWidget.setStyleSheet("background-color: " + rv.mbf.color + ";")
            layout.addWidget(mbfWidget, row+1, col, 1, 1)

            col = col + 1
            mvpWidget = QtGui.QWidget(checksGridLayoutWidget)
            mvpLabel = QtGui.QLabel(mvpWidget)
            mvpLabel.setText(str('{:1.4f}'.format(rv.mvp.value)))
            mvpWidget.setStyleSheet("background-color: " + rv.mvp.color + ";")
            layout.addWidget(mvpWidget, row+1, col, 1, 1)

            col = col + 1
            mvnWidget = QtGui.QWidget(checksGridLayoutWidget)
            mvnLabel = QtGui.QLabel(mvnWidget)
            mvnLabel.setText(str('{:1.4f}'.format(rv.mvn.value)))
            mvnWidget.setStyleSheet("background-color: " + rv.mvn.color + ";")
            layout.addWidget(mvnWidget, row+1, col, 1, 1)

            col = col + 1
            mapWidget = QtGui.QWidget(checksGridLayoutWidget)
            mapLabel = QtGui.QLabel(mapWidget)
            mapLabel.setText(str('{:1.4f}'.format(rv.map.value)))
            mapWidget.setStyleSheet("background-color: " + rv.map.color + ";")
            layout.addWidget(mapWidget, row+1, col, 1, 1)

            col = col + 1
            manWidget = QtGui.QWidget(checksGridLayoutWidget)
            manLabel = QtGui.QLabel(manWidget)
            manLabel.setText(str('{:1.4f}'.format(rv.man.value)))
            manWidget.setStyleSheet("background-color: " + rv.man.color + ";")
            layout.addWidget(manWidget, row+1, col, 1, 1)

            col = col + 1
            mdmWidget = QtGui.QWidget(checksGridLayoutWidget)
            mdmLabel = QtGui.QLabel(mdmWidget)
            mdmLabel.setText(str('{:1.4f}'.format(rv.mdm.value)))
            mdmWidget.setStyleSheet("background-color: " + rv.mdm.color + ";")
            layout.addWidget(mdmWidget, row+1, col, 1, 1)

            col = col + 1
            kalmanWidget = QtGui.QWidget(checksGridLayoutWidget)
            kalmanLabel = QtGui.QLabel(kalmanWidget)
            kalmanLabel.setText(str('{:1.4f}'.format(rv.kalman.value)))
            kalmanWidget.setStyleSheet("background-color: " + rv.kalman.color + ";")
            layout.addWidget(kalmanWidget, row+1, col, 1, 1)

            col = col + 1
            fbrWidget = QtGui.QWidget(checksGridLayoutWidget)
            fbrLabel = QtGui.QLabel(fbrWidget)
            fbrLabel.setText(str('{:1.4f}'.format(rv.fbr.value)))
            fbrWidget.setStyleSheet("background-color: " + rv.fbr.color + ";")
            layout.addWidget(fbrWidget, row+1, col, 1, 1)

            col = col + 1
            suspicionWidget = QtGui.QWidget(checksGridLayoutWidget)
            suspicionLabel = QtGui.QLabel(suspicionWidget)
            suspicionLabel.setText(str('{:1.4f}'.format(rv.suspicion.value)))
            suspicionWidget.setStyleSheet("background-color: " + rv.suspicion.color + ";")
            layout.addWidget(suspicionWidget, row+1, col, 1, 1)

            col = col + 1
            mlPredictionWidget = QtGui.QWidget(checksGridLayoutWidget)
            mlPredictionLabel = QtGui.QLabel(mlPredictionWidget)
            if rv.mlPrediction.value == 1:
                mlPredictionLabel.setText("Attack")
                mlPredictionWidget.setStyleSheet("background-color: red;")
            else:
                mlPredictionLabel.setText("Genuine")
            layout.addWidget(mlPredictionWidget, row+1, col, 1, 1)

            col = col + 1
            eeblMpsOffWidget = QtGui.QWidget(checksGridLayoutWidget)
            eeblMpsOffLabel = QtGui.QLabel(eeblMpsOffWidget)
            if rv.eeblMpsOff.value == 1:
                eeblMpsOffWidget.setStyleSheet("background-color: green;")
            layout.addWidget(eeblMpsOffWidget, row+1, col, 1, 1)

            col = col + 1
            eeblMpsOnWidget = QtGui.QWidget(checksGridLayoutWidget)
            eeblMpsOnLabel = QtGui.QLabel(eeblMpsOnWidget)
            if rv.eeblMpsOn.value == 1:
                eeblMpsOnWidget.setStyleSheet("background-color: green;")
            layout.addWidget(eeblMpsOnWidget, row+1, col, 1, 1)

    def getRemoteVehicles(self):
        # get currently selected host vehicle
        hv_id = self.mainUi.hostVehicleIdComboBox.currentText().split('_')[0]

        # check if trace files generated
        if not glob.glob("{}/*trace.csv".format(self.log_path)):
            return

        # extract rows with only selected host vehicle id
        trace_data = self.core.get_remote_vehicles(hv_id)

        # populate remote vehicles with regards to selected host vehicle
        vehicle = core.vehicle.Vehicle(hv_id, "{}/omnetpp.ini".format(self.base_dir))
        return vehicle.populateRvs(trace_data)

        # print("{0:9s}{1:14s}{2:14s}{3:14s}{4:14s}{5:14s}{6:14s}{7:14s}{8:14s}{9:11s}{10:14s}".format(
        #     "ID", "art", "saw", "mbf", "mvp", "mvn", "map", "man", "mdm", "kalman", "suspicion"))
        # for rv in vehicle.rv_list:
        #     print("{0:4d} {1:4.3f} {2:7s} {3:4.3f} {4:7s} {5:4.3f} {6:7s} {7:4.3f} {8:7s} {9:4.3f} {10:7s} {11:4.3f} {12:7s} {13:4.3f} {14:7s} {15:4.3f} {16:7s} {17:4.3f} {18:7s} {19:4.3f} {20:7s}".format(
        #                 rv.vehicle_id,
        #                 rv.art.value, rv.art.color,
        #                 rv.saw.value, rv.saw.color,
        #                 rv.mbf.value, rv.mbf.color,
        #                 rv.mvp.value, rv.mvp.color,
        #                 rv.mvn.value, rv.mvn.color,
        #                 rv.map.value, rv.map.color,
        #                 rv.man.value, rv.man.color,
        #                 rv.mdm.value, rv.mdm.color,
        #                 rv.kalman.value, rv.kalman.color,
        #                 rv.suspicion.value, rv.suspicion.color))
        # return rvs


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    CustomMainWindow()
    sys.exit(app.exec_())
