#####################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: Extracts data using threads so that GUI does not hang
#####################################

from PyQt4.QtCore import QThread, pyqtSignal

from core.data import Data

class VehicleDataThread(QThread):
    vehicle_data_ready = pyqtSignal(PlotData)
    def __init__(self):
        QThread.__init__(self)
        self.data_path = ""

    def setDataPath(self, data_path):
        self.data_path = data_path

    def run(self):
        data = Data()
        data.extract_vehicle_density_data(self.data_path)
        self.get_vehicle_density_data(data)

    def get_vehicle_density_data(self, data):
        p = PlotData()
        p.x = data.vehicle_density_data.simTime.tolist()
        # everything between simTime and repNum
        p.y = data.vehicle_density_data.iloc[:, 1:-1]
        self.vehicle_data_ready.emit(p)


    def __del__(self):
        self.wait()

