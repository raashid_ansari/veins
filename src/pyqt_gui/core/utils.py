############################################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: Calculates performance metrics for evaluation
############################################################

import numpy as np
import pandas as pd

import core.entity as ce

def sanitize_dataframe(df):
    for col in df.columns:
        df.loc[:, col] = df[col].astype(float)
    return df


def calculate_metrics(check, threshold, attack_gt):
    e = ce.Entity()
    df = pd.DataFrame()

    # base confusion
    df["tp"] = ((check > threshold) & (attack_gt == 1)).cumsum()
    df["fp"] = ((check > threshold) & ~(attack_gt == 1)).cumsum()
    df["fn"] = (~(check > threshold) & (attack_gt == 1)).cumsum()
    df["tn"] = (~(check > threshold) & ~(attack_gt == 1)).cumsum()

    # name of the entity
    e.name = check.name

    # get last values of confusion matrix arrays
    e.tp = df.tp.tail(1).item()
    e.fp = df.fp.tail(1).item()
    e.fn = df.fn.tail(1).item()
    e.tn = df.tn.tail(1).item()

    # False positive rate
    e.fpr = divide(df.fp, df.fp + df.tn)

    # True positive rate and recall
    e.tpr = divide(df.tp, df.tp + df.fn)
    e.recall = e.tpr.copy()

    # Precision
    e.precision = divide(df.tp, df.tp + df.fp)

    # F1-score
    e.f1 = divide(2 * e.precision * e.recall, e.precision + e.recall)

    # Accuracy
    e.accuracy = divide(df.tp + df.tn, df.tp + df.tn + df.fp + df.fn)
    return e


def divide(numerator, denominator):
    return np.where(denominator == 0, numerator, numerator / denominator)
