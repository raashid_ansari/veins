###############################################
# Author: Raashid Ansari
# Year: 2018
# Organization: OnBoard Security, Inc.
# Description: Extract RAM usage of a given process
###############################################
import psutil
import time

class SystemResourceUsage:
    def getRamUsage(self, process_id):
        # return the memory usage in MB
        process = psutil.Process(process_id)
        return process.memory_percent()

    def getCpuUsage(self, process_id):
        process = psutil.Process(process_id)
        process.cpu_percent() # have to call here but ignore its result
        time.sleep(0.1)
        return process.cpu_percent()

# if __name__ == '__main__':
    # pid = os.getpid()
    # ru = SystemResourceUsage()
    # for i in range(10):
        # print(psutil.virtual_memory().available, ru.getRamUsage(pid), ru.getCpuUsage(pid))
