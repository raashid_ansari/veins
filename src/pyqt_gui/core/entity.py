###########################################################################
# Author: Raashid Ansari
# Year: 2018
# Organisation: OnBoard Security, Inc.
# Description: Check data structure used in gui
###########################################################################

class Entity:
    def __init__(self):
        self.name = None
        self.tp = None
        self.fp = None
        self.fn = None
        self.tn = None
        self.tpr = []
        self.fpr = []
        self.precision = []
        self.recall = []
        self.f1 = []
        self.accuracy = []
        self.is_enabled = False
        self.value = 0.0


    def update_color(self, orange=0.5, red=1.0):
        if not self.is_enabled:
            self.color = "silver"
            return

        if self.value >= 0.0 and self.value < orange:
            self.color = "white"
        elif self.value >= orange and self.value < red - 0.0002:
            self.color = "orange"
        elif self.value >= red - 0.0002:
            self.color = "red"
