import os

import numpy as np
import pandas as pd

import core.dataextractor as cd
import core.entity as ce
import core.utils as cu

class Core:
    def __init__(self):
        self.df = pd.DataFrame()
        self.attack_gt = pd.Series()

    def extract_data(self, data_path):
        # Extract data
        de = cd.DataExtractor()
        self.df = de.extract_trace_data(data_path)
        self.attack_gt = self.df["groundTruth.attack"]
        self.vdf = de.extract_vehicle_density_data(data_path)

    def get_ewma_checks(self, threshold):
        # create checks with EWMA values object list
        ewma_mps_df = self.df.loc[:, "ratings.ewma.ART" : "ratings.ewma.suspicion"]
        return [cu.calculate_metrics(ewma_mps_df[check], threshold, self.attack_gt)
                for check in ewma_mps_df.columns]

    def get_instant_checks(self, threshold):
        # create checks with Instantaneous values object list
        instant_mps_df = self.df.loc[:, "ratings.instant.ART" : "ratings.instant.suspicion"]
        return [cu.calculate_metrics(instant_mps_df[check], threshold, self.attack_gt) for
                check in instant_mps_df.columns]


    def get_attacks(self, threshold):
        # create attacks object list
        attack_df = self.df.loc[:, "eebl.mpsOff" : "eebl.mpsOn"]
        return [cu.calculate_metrics(attack_df[attack], threshold, self.attack_gt) for
                attack in attack_df.columns]

    def get_latencies(self):
        # create latency list
        latencies = self.df.loc[self.attack_gt == 1]
        return (latencies.simTime, latencies.loc[:, "latency.simTime" : "latency.distance"])

    def get_exec_times(self):
        # create execution time list
        exec_times = self.df.loc[self.df["latency.bsm"].notnull(), :]
        return (exec_times.simTime, exec_times.loc[:, "execTime.ART" : "execTime.EML"])

    def get_host_vehicles(self):
        host_vehicles = (self.df.hvId.astype(str) +\
                "_t" + self.df['groundTruth.threshold'].astype(str) +\
                "_r" + self.df['groundTruth.repNum'].astype(str)).unique()
        host_vehicles.sort()
        return host_vehicles

    def get_remote_vehicles(self, hv_id):
        # extract rows with only selected host vehicle id
        return self.df.loc[self.df.hvId == int(hv_id)]

    def get_time_series(self):
        return self.df.simTime

    def get_vehicle_density(self):
        return (self.vdf.simTime, self.vdf.vehicleDensity)


