##############################################################################
# Author:       Raashid Ansari
# Year:         2018
# Organization: OnBoard Security
# Description:  This is a Flask framework based server application that hosts
#               the Machine Learning Server. The Machine Learning Model is
#               written in MLModel.py file.
# Notes:        This server is deployed on a proxy gunicorn server which is
#               in-turn deployed on the nginx server.
#               While debugging, this server runs on port 8880, gunicorn
#               proxy server runs on 8000, and nginx server listens to the
#               8880 port to forward requests to this server via gunicorn
##############################################################################

import os
import pandas as pd
from pandas.io.json import json_normalize

from flask import Flask
from flask import request
app = Flask(__name__)

df = pd.DataFrame()
first_req_flag = True

@app.route("/data", methods=['PUT'])
def store_incoming_data(host_vehicle_id=None):
    global df
    global first_req_flag
    data = request.get_json()
    if first_req_flag is True:
        first_req_flag = False
        cols = json_normalize(data).columns
        df = pd.DataFrame(columns=cols)
    df.loc[len(df)] = json_normalize(data).values[0]
    return "received data from " + str(df.hvId.item())

@app.route("/")
def index():
    return "Hello World!"

# enable the following to debugging while development
if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port='8882')
